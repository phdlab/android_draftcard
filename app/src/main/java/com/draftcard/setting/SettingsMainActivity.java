package com.draftcard.setting;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.base.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Injecting the Layout to the Activity
 *
 * @Params(integer layout Id )
 */

@EActivity(R.layout.activity_main)
public class SettingsMainActivity extends BaseActivity {

    FChangePassword fChangePassword;
    FEditGeneralInfo fEditGeneralInfo;
    FSettings fSettings;
    FragmentManager fm;

    /**
     * Injecting FrameLayout
     *
     * @params fragment_root
     */
    @ViewById
    FrameLayout fragment_root;

    /**
     * AfterViews annotated method is called when Views are injected
     * Transparent StatusBar method has been called here .
     * Default it will show Setting Screen
     */

    @AfterViews
    public void _AfterViews() {
        transparentStatusBar();
        //statusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        setfSettings();
    }

    /*
     *Initiating ChangePassword screen here
     */

    public void setfChangePassword() {
        fChangePassword = new FChangePassword_.FragmentBuilder_().build();
        ftEnterAdd(fChangePassword);
    }

    /*
     *Initiating GeneralInfo screen here
     */

    public void setfEditGeneralInfo() {
        fEditGeneralInfo = new FEditGeneralInfo_.FragmentBuilder_().build();
        ftEnterAdd(fEditGeneralInfo);
    }

    /*
     *Initiating Settings screen here
     */
    public void setfSettings() {
        fSettings = new FSettings_.FragmentBuilder_().build();
        ftEnterAddNoAnim(fSettings);
    }

    /*
     *Method back Button Pressed
     */

    @Click
    public void btnBack() {
        _GetCurrentFragment();
        if (f instanceof FSettings_) {
            finish();
            overridePendingTransition(R.anim.enter,R.anim.exit);
        } else if (f instanceof FEditGeneralInfo_) {
            if (fEditGeneralInfo.isDialogOpen()) {
                fEditGeneralInfo.closeDialog();
            }else {
                fSettings.initUi();
                ftExitAdd();
                _GetCurrentFragment();
            }
        } else {
            ftExitAdd();
            _GetCurrentFragment();
        }
    }

    /*
     *Overriding back Button Pressed Event
     */
    @Override
    public void onBackPressed() {
        btnBack();
    }


    /*
     *Adding new Fragment to our root Layout
     *@params fragment
     */
    public void ftEnterAdd(Fragment fragment) {
        FragmentTransaction transaction;
        hideKeyBoard();
        // Make sure the current transaction finishes first
        if (fm == null) {
            fm = getSupportFragmentManager();
        }
        String TAG = fragment.getClass().getName().toString();
        // Add it

        transaction = fm.beginTransaction().setCustomAnimations(R.anim.right_left, R.anim.left_right, R.anim.left_right, R.anim.left_right);

        transaction.add(R.id.fragment_root, fragment, TAG).addToBackStack(TAG);
        transaction.commitAllowingStateLoss();
        _SmoothTransaction();

    }

    /*
     *Adding new Fragment to our root Layout without Animation
     *@params fragment
     */
    public void ftEnterAddNoAnim(Fragment fragment) {
        FragmentTransaction transaction;
        hideKeyBoard();
        // Make sure the current transaction finishes first

        fm = getSupportFragmentManager();
        String TAG = fragment.getClass().getName().toString();
        // Add it
        transaction = fm.beginTransaction();

        transaction.add(R.id.fragment_root, fragment, TAG).addToBackStack(TAG);
        transaction.commitAllowingStateLoss();
    }

    /*
     * Pop Back currently added Fragment from the stack
     *
     */

    private void ftExitAdd() {
        _GetCurrentFragment();
        fm.popBackStackImmediate();
        fm.beginTransaction().remove(f).commitAllowingStateLoss();
        _SmoothTransaction();
    }

    /*
     *Smooth transaction for the fragment manager if any pending task exist
     *
     */
    private void _SmoothTransaction() {
        if (fm != null && fm.getBackStackEntryCount() > 0) {
            fm.executePendingTransactions();
        } else {
            fm = getSupportFragmentManager();
            fm.executePendingTransactions();
        }

    }

    public void _ClearAllFragment() {
        clearBackStackInclusive();
        //ft = fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.slide_in_right, R.anim.slide_out_left);
        ft = fm.beginTransaction();
    }

    Fragment f;
    FragmentTransaction ft;

    /*
     *Clear back stack from the fragmentmanager
     *
     */
    public void clearBackStackInclusive() {
        _GetCurrentFragment();
        if (f != null) {
            String tag = f.getClass().getName().toString();
            fm.popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fm.beginTransaction().remove(f).commitAllowingStateLoss();
        }
    }

    /*
     *_Get currently showing fragment Object
     *
     */
    private void _GetCurrentFragment() {
        f = getSupportFragmentManager().findFragmentById(R.id.fragment_root);
    }

    private boolean _GetCurrentFragmentExist(Fragment fragment) {
        if (fragment == null) {
            return false;
        }
        if (fm == null) {
            fm = getSupportFragmentManager();
        }
        String TAG = fragment.getClass().getName().toString();
        // If there is no fragment yet with this tag...
        if (fm.findFragmentByTag(TAG) != null) {
//            isFragmentInitiating = true;
            return true;
        } else {
            return false;
        }
    }

    /*
     *Hide softKeyBoard
     */
    public void hideKeyBoard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }




}
