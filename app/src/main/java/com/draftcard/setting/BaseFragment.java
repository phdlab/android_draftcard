package com.draftcard.setting;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

/**
 * Created by Andriod-176 on 3/3/2017.
 */

public class BaseFragment extends Fragment {
    LinearLayout main;

    /**
     * Set Header View for all extended fragment by Passing the Layout
     * @params main
     */
    public void setToolBar(LinearLayout main) {
        this.main = main;
        if (Build.VERSION.SDK_INT >= 21) {
            this.main.setPadding(0, getStatusBarHeight(), 0, 0);
        }
    }


    /**
     * get StatusBar height and set padding accordingly
     */
    protected int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    /**
     * Common Method for snackBar showing
     */
    public void showSnackbar( String msg) {
        Snackbar.make(main, msg, Snackbar.LENGTH_LONG).show();
    }

    protected void closeKeyBoard(Activity context) {
        View view =  context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
