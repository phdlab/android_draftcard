package com.draftcard.setting;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.dashboard.view.DashboardActivity_;
import com.draftcard.prelogin.view.LoginActivity;
import com.draftcard.prelogin.view.LoginActivity_;
import com.draftcard.setting.model.Rq_Update_Profile;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.facebook.FacebookSdkNotInitializedException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Injecting the Layout to the Fragment
 *
 * @Params(integer layout Id )
 */

@EFragment(R.layout.fragment_setting)
public class FSettings extends BaseFragment {

    /**
     * All the Views are injected here by view Type(ImageView) and Id name(imgProfile)
     */


    Rq_Update_Profile rq_update_profile = new Rq_Update_Profile();
    @ViewById
    ImageButton btnBack;
    @ViewById
    TextView txtTitle;
    @ViewById
    RelativeLayout rlTitle;
    @ViewById
    ImageView imgProfile;
    @ViewById
    RelativeLayout rlPic;
    @ViewById
    TextView txtName;
    @ViewById
    TextView txtSurName;
    @ViewById
    ImageButton imgGrade;
    @ViewById
    TextView txtGradeYear;
    @ViewById
    TextView txtEdit;
    @ViewById
    ImageButton imgGpa;
    @ViewById
    TextView txtGpa;
    @ViewById
    ImageButton imgEmail;
    @ViewById
    TextView txtEmail;
    @ViewById
    ImageButton imgPassword;
    @ViewById
    TextView txtPassword;
    @ViewById
    TextView txtChange;
    @ViewById
    TextView txtLogout;
    @ViewById
    LinearLayout main;
    @ViewById
    LinearLayout llChangePwd;

    /**
     * AfterViews annotated method is called when Views are injected
     * initiating the views are done here in this method.
     */
    @AfterViews
    public void _Settings() {
        txtTitle.setText(getString(R.string.title_settings));
        setToolBar(main);
        initUi();
    }

    /**
     * initiating the views are done here in this method.
     */
    public void initUi() {
        rq_update_profile = AppUtilz.getUserData();
        AppUtilz.showImageNet(getActivity(), rq_update_profile.profile_pic, imgProfile);
        txtName.setText(rq_update_profile.first_name);
        txtSurName.setText(rq_update_profile.last_name);
        txtGpa.setText(rq_update_profile.gpa);
        txtGradeYear.setText(rq_update_profile.grad_year);
        txtEmail.setText(rq_update_profile.email);
        android.util.Log.d(FSettings.class.getSimpleName(), "FSetting::");
        //txtPassword.setText(rq_update_profile.password);
        if (!TextUtils.isEmpty(rq_update_profile.social_id)) {
            llChangePwd.setVisibility(View.GONE);
        }
    }

    /**
     * Click Method for edit info
     */
    @Click
    public void txtEdit() {
        ((SettingsMainActivity) getActivity()).setfEditGeneralInfo();
    }

    /**
     * Click Method for change password
     */
    @Click
    public void txtChange() {
        ((SettingsMainActivity) getActivity()).setfChangePassword();
    }

    /**
     * Click Method for back Button
     */
    @Click
    public void btnBack() {
        ((SettingsMainActivity) getActivity()).btnBack();
    }

    @Click
    public void txtLogout() {
        SharedPreferenceUtil.putValue(AppUtilz.IS_LOGIN, false);
        SharedPreferenceUtil.save();
        getActivity().finish();
    }

}
