package com.draftcard.setting;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.setting.model.Rq_Reset_Pwd;
import com.draftcard.utilz.AppUtilz;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Injecting the Layout to the Fragment
 *
 * @Params(integer layout Id )
 */
@EFragment(R.layout.fragment_reset_password)
public class FResetPassword extends BaseFragment {
    private static final String TAG = "";

    /**
     * All the Views are injected here by view Type(ImageView) and Id name(imgProfile)
     */

    @ViewById
    ImageButton btnBack;
    @ViewById
    TextView txtTitle;
    @ViewById
    RelativeLayout rlTitle;
    @ViewById
    EditText edtNewPwd;
    @ViewById
    TextInputLayout iptNewPwd;
    @ViewById
    EditText edtCnfrmPwd;
    @ViewById
    TextInputLayout iptCnfrmPwd;
    @ViewById
    TextView txtResetPwd;
    @ViewById
    LinearLayout main;

    /**
     * AfterViews annotated method is called when Views are injected
     * initiating the views are done here in this method.
     */
    @AfterViews
    public void _Settings(){
        txtTitle.setText(getString(R.string.change_password));
        setToolBar(main);
    }


    /**
     *Click Method For Back Button
     *
     */
    @Click
    public  void  btnBack(){
        ((SettingsMainActivity)getActivity()).btnBack();
    }


    /**
     *Click Method for Reset Password
     *
     */
    @Click
    public void txtResetPwd() {
        Rq_Reset_Pwd rq_resetPwd = new Rq_Reset_Pwd();
        rq_resetPwd.token = "";
        rq_resetPwd.password = edtNewPwd.getText().toString().trim();
        rq_resetPwd.password_confirmation = edtCnfrmPwd.getText().toString().trim();
        if (isValidate(rq_resetPwd)) {
            HashMap<String, Object> authData = new HashMap<>();
            authData.put("token", rq_resetPwd.token);
            authData.put("password", rq_resetPwd.password);
            authData.put("password_confirmation", rq_resetPwd.password_confirmation);
            callChangePwd(authData);
        } else {
            showSnackbar(alertMsg);
        }
    }
    /**
     *Api Calling For ChangePassword Method
     *
     */
    private void callChangePwd(HashMap<String, Object> resetpwd) {
        if (AppUtilz.isNetworkAvailable(getActivity())) {
            AppUtilz.showProgressDialog(getContext());
            final Observable<CommonResponse> responseUserObservable = NetWorkUtils.getApiService().resetPwd(resetpwd);
            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<CommonResponse>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            showSnackbar(getString(R.string.unexpected_error));
                        }

                        @Override
                        public void onNext(CommonResponse loginResponse) {
                            Log.e(TAG, "onNext: " + loginResponse.getMessage());
                            if (loginResponse.isSuccess()) {
                                ((SettingsMainActivity) getActivity()).btnBack();
                            } else {
                                showSnackbar(loginResponse.getMessage());
                            }
                        }
                    });
        } else {
            showSnackbar(getString(R.string.no_internet));
        }
    }

    String alertMsg = "";
      /*
       *  Validating Data
       * @Params rq_resetPwd
       */

    public boolean isValidate(Rq_Reset_Pwd rq_resetPwd) {
        if (TextUtils.isEmpty(rq_resetPwd.password)) {
            alertMsg = getString(R.string.alrt_new_pwd);
            return false;
        } else if (TextUtils.isEmpty(rq_resetPwd.password_confirmation)) {
            alertMsg = getString(R.string.alrt_cn_pwd);
            return false;
        } else if (!rq_resetPwd.password.equalsIgnoreCase(rq_resetPwd.password_confirmation)) {
            alertMsg = getString(R.string.alrt_same_cn_pwd);
            return false;
        }
        return true;
    }
}
