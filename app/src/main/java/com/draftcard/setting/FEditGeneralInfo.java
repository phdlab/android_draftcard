package com.draftcard.setting;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.transition.Explode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.draftcard.R;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.setting.model.Rq_Update_Profile;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.Constants;
import com.draftcard.utilz.FileUtils;
import com.draftcard.utilz.InputFilterMax;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.draftcard.utilz.camera.BitmapHelper;
import com.draftcard.utilz.camera.CameraIntentHelper;
import com.draftcard.utilz.camera.CameraIntentHelperCallback;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import response.EditProfileResponse;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

/**
 * Injecting the Layout to the Fragment
 *
 * @Params(integer layout Id )
 */
@EFragment(R.layout.fragment_general_info)
public class FEditGeneralInfo extends BaseFragment {

    /**
     * All the Views are injected here by view Type(ImageView) and Id name(imgProfile)
     */

    @ViewById
    ImageView imgProfile;
    @ViewById
    TextView txtChangePic;
    @ViewById
    EditText edtName;
    @ViewById
    TextInputLayout iptName;
    @ViewById
    EditText edtLName;
    @ViewById
    TextInputLayout iptLName;
    @ViewById
    EditText edtYear;
    @ViewById
    TextInputLayout iptYear;
    @ViewById
    EditText edtGpa;
    @ViewById
    TextInputLayout iptGpa;
    @ViewById
    TextView txtSave;

    @ViewById
    ImageButton btnBack;
    @ViewById
    TextView txtTitle;
    @ViewById
    RelativeLayout rlTitle;

    @ViewById
    LinearLayout main;
    protected int FINISH_TIME = 400;
    protected int ANIM_TIME = 300;
    @ViewById
    LinearLayout lin_bottom_sheet;


    Rq_Update_Profile rq_update_profile = new Rq_Update_Profile();


    /**
     * AfterViews annotated method is called when Views are injected
     * initiating the views are done here in this method.
     */
    @AfterViews
    public void _Settings() {
        txtTitle.setText(getString(R.string.edt_general_info));
        setToolBar(main);
        initBottomSheet();
        edtGpa.setFilters(new InputFilter[]{new InputFilterMax(AppUtilz.MAX_GPA, InputFilterMax.FilterType.FLOAT)});
        setData();
    }

    /**
     * Setting data to the rest of the views from sharedpreference
     */

    private void setData() {
        rq_update_profile = AppUtilz.getUserData();
        AppUtilz.showImageNet(getActivity(), rq_update_profile.profile_pic, imgProfile);
        edtGpa.setText(rq_update_profile.gpa);
        edtYear.setText(rq_update_profile.grad_year);
        edtName.setText(rq_update_profile.first_name);
        edtLName.setText(rq_update_profile.last_name);
    }

    /**
     * Initialising the bottom sheet
     */
    private void initBottomSheet() {
        behavior = BottomSheetBehavior.from(lin_bottom_sheet);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
    }


    /*
     * Click Method of ProfilePicImageView
     * Add News Image into the ProfileImageView
     */

    @Click
    public void imgProfileAdd() {
        checkPermission();
    }

    @Click
    public void imgProfile() {
        checkPermission();
    }

    /*
     * Click Method of Open Camera
     *
     */
    @Click
    public void rl_camera() {
        openCamera();
    }

    /*
     * Click Method of Open Gallery
     *
     */
    @Click
    public void rl_gallery() {
        openGallery();
    }

    /*
     * Click Method of EditGeneral Info
     * saving data and varifying it.
     */

    @Click
    public void tvSignUp() {
        rq_update_profile.first_name = edtName.getText().toString().trim();
        rq_update_profile.last_name = edtLName.getText().toString().trim();
        rq_update_profile.grad_year = edtYear.getText().toString().trim();

        if(!TextUtils.isEmpty(edtGpa.getText().toString()) && '.' == edtGpa.getText().toString().charAt(edtGpa.getText().toString().length() - 1)) {
            edtGpa.setText(edtGpa.getText().toString().substring(0, edtGpa.getText().toString().length() - 1));
        }

        rq_update_profile.gpa = edtGpa.getText().toString().trim();
        rq_update_profile.profile_pic = mImagePath;
        if (isValidate(rq_update_profile)) {
            HashMap<String, Object> authData = new HashMap<>();
            authData.put("user_id", rq_update_profile.user_id);
            authData.put("first_name", rq_update_profile.first_name);
            authData.put("last_name", rq_update_profile.last_name);
            authData.put("email", rq_update_profile.email);
            authData.put("dob", rq_update_profile.dob);
            authData.put("school_name", rq_update_profile.school_name);
            authData.put("school_city", rq_update_profile.school_city);
            authData.put("school_mascot", rq_update_profile.school_mascot);
            authData.put("gender", rq_update_profile.gender);
            authData.put("grad_year", rq_update_profile.grad_year);
//            authData.put("token",rq_update_profile.token);
//            File imageProfileFile = new File(rq_update_profile.profile_pic);
//            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageProfileFile);
//            MultipartBody.Part bodyImage = MultipartBody.Part.createFormData("profile_pic", imageProfileFile.getName(), requestFile);
//            authData.put("profile_pic", rq_update_profile.profile_pic);

            MultipartBody.Part profilePicImage = null;

            Log.e(FEditGeneralInfo.class.getSimpleName(), "rq_update_profile.profile_pic::: " + rq_update_profile.profile_pic);

            if (!TextUtils.isEmpty(rq_update_profile.profile_pic)) {
                File profilePicFile = new File(rq_update_profile.profile_pic);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), profilePicFile);
                profilePicImage = MultipartBody.Part.createFormData(AppUtilz.PROFILE_PIC, profilePicFile.getName(), requestFile);
            } else {
//                RequestBody requestFile = RequestBody.create(MediaType.parse("text/plain"), "");
//                profilePicImage = MultipartBody.Part.createFormData("profile_pic", "", requestFile);
            }

            authData.put("gpa", rq_update_profile.gpa);

            callChangePwd(authData, profilePicImage == null ? null : profilePicImage);

        } else {
            showSnackbar(alertMsg);
        }
    }

    /*
     * Calling Api for updateProfile
     * @Params updateProfilepic
     */

    private void callChangePwd(HashMap<String, Object> updateProfile, MultipartBody.Part bodyImage) {

        if (AppUtilz.isNetworkAvailable(getActivity())) {
            AppUtilz.showProgressDialog(getContext());

            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.USER_ID).toString());
            RequestBody firstName = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.FIRST_NAME).toString());
            RequestBody lastName = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.LAST_NAME).toString());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.EMAIL).toString());
            RequestBody dob = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.DOB).toString());
            RequestBody gpa = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.GPA).toString());
            RequestBody schoolName = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.SCHOOL_NAME).toString());
            RequestBody schoolCity = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.SCHOOL_CITY).toString());
            RequestBody schoolMascot = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.SCHOOL_MASCOT).toString());
            RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.GENDER).toString());
            RequestBody gradYear = RequestBody.create(MediaType.parse("text/plain"), updateProfile.get(AppUtilz.GRAD_YEAR).toString());

            Observable<Response<ResponseBody>> responseUserObservable = null;


                Log.e(TAG, "02 Image");
                responseUserObservable = NetWorkUtils.getApiService().editProfile(SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""), userId,
                        firstName, lastName, email, dob, gpa, schoolName, schoolCity, schoolMascot, gender, gradYear, bodyImage);


//            final Observable<CommonResponse> responseUserObservable = NetWorkUtils.getApiService().updateProfile(SharedPreferenceUtil.getString(AppUtilz.TOKEN,""),updateProfile, bodyImage);

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            showSnackbar(getString(R.string.unexpected_error));
                        }


                        @Override
                        public void onNext(Response<ResponseBody> loginResponse) {
//                            if (loginResponse.isSuccess()) {
//                                try {
//                                    EditProfileResponse response = loginResponse;
//                                    if (response.isSuccess()) {
//                                        SharedPreferenceUtil.putValue(AppUtilz.ID, String.valueOf(response.getData().getId()));
//                                        SharedPreferenceUtil.putValue(AppUtilz.EMAIL, response.getData().getEmail());
//                                        SharedPreferenceUtil.putValue(AppUtilz.USER_NAME, response.getData().getFirstName());
//                                        SharedPreferenceUtil.putValue(AppUtilz.FIRST_NAME, response.getData().getFirstName());
//                                        SharedPreferenceUtil.putValue(AppUtilz.LAST_NAME, response.getData().getLastName());
//                                        SharedPreferenceUtil.putValue(AppUtilz.GENDER, response.getData().getGender());
//                                        SharedPreferenceUtil.putValue(AppUtilz.DOB, response.getData().getDob());
//                                        SharedPreferenceUtil.putValue(AppUtilz.ADDRESS, response.getData().getSchoolCity());
//                                        SharedPreferenceUtil.putValue(AppUtilz.AVTAR, response.getData().getProfilePic());
//                                        SharedPreferenceUtil.putValue(AppUtilz.IS_ACTIVE, response.getData().getIsActive());
//                                        SharedPreferenceUtil.putValue(AppUtilz.SOCIAL_ID, response.getData().getSocialId());
//                                        SharedPreferenceUtil.putValue(AppUtilz.GRAD_YEAR, response.getData().getGradYear());
//                                        SharedPreferenceUtil.putValue(AppUtilz.GPA, response.getData().getGpa());
//                                        SharedPreferenceUtil.putValue(AppUtilz.TOKEN, response.getData().getToken());
//                                        SharedPreferenceUtil.putValue(AppUtilz.IS_LOGIN, true);
//                                        SharedPreferenceUtil.save();
//                                        ((SettingsMainActivity) getActivity()).btnBack();
//                                    } else {
//                                        showSnackbar(response.getMessage());
//                                    }
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                showSnackbar("Failure!");
//                            }



                            try {
                                byte[] byteArray = loginResponse.body().bytes();
                                String responseInString = new String(byteArray);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(responseInString);
                                    Gson gson = new Gson();
                                    EditProfileResponse response = gson.fromJson(jsonObject.toString(), EditProfileResponse.class);
                                    if (jsonObject.getBoolean("success")) {
                                        /*convert data object into commonresponse class*/
                                        SharedPreferenceUtil.putValue(AppUtilz.ID, String.valueOf(response.getData().getId()));
                                        SharedPreferenceUtil.putValue(AppUtilz.EMAIL, response.getData().getEmail());
                                        SharedPreferenceUtil.putValue(AppUtilz.USER_NAME, response.getData().getFirstName());
                                        SharedPreferenceUtil.putValue(AppUtilz.FIRST_NAME, response.getData().getFirstName());
                                        SharedPreferenceUtil.putValue(AppUtilz.LAST_NAME, response.getData().getLastName());
                                        SharedPreferenceUtil.putValue(AppUtilz.GENDER, response.getData().getGender());
                                        SharedPreferenceUtil.putValue(AppUtilz.DOB, response.getData().getDob());
                                        SharedPreferenceUtil.putValue(AppUtilz.ADDRESS, response.getData().getSchoolCity());
                                        SharedPreferenceUtil.putValue(AppUtilz.AVTAR, response.getData().getProfilePic());
                                        SharedPreferenceUtil.putValue(AppUtilz.IS_ACTIVE, response.getData().getIsActive());
                                        SharedPreferenceUtil.putValue(AppUtilz.SOCIAL_ID, response.getData().getSocialId());
                                        SharedPreferenceUtil.putValue(AppUtilz.GRAD_YEAR, response.getData().getGradYear());
                                        SharedPreferenceUtil.putValue(AppUtilz.GPA, response.getData().getGpa());
                                        SharedPreferenceUtil.putValue(AppUtilz.TOKEN, response.getData().getToken());
                                        SharedPreferenceUtil.putValue(AppUtilz.IS_LOGIN, true);
                                        SharedPreferenceUtil.save();
                                        ((SettingsMainActivity) getActivity()).btnBack();
                                    } else {
                                        showSnackbar(response.getMessage());
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }

                    });
        } else {
            showSnackbar(getString(R.string.no_internet));
        }
    }

    String alertMsg = "";

      /*
       *  Validating Data
       * @Params rq_update_profile
       */

    public boolean isValidate(Rq_Update_Profile rq_update_profile) {
        if (TextUtils.isEmpty(rq_update_profile.first_name)) {
            alertMsg = getString(R.string.alrt_name);
            return false;
        }
        return true;
    }

      /*
       *  Validating Dialouge is open or not
       *
       */

    public boolean isDialogOpen() {
        return (!(behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED));
    }

    /*
     *  CloseDialog Method
     *
     */
    public void closeDialog() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
    }

     /*
      *  Click method for back Button
      *
      */

    @Click
    public void btnBack() {
        if (isDialogOpen()) {
            closeDialog();
        } else {
            ((SettingsMainActivity) getActivity()).btnBack();
        }
    }


    /*
     * Checking MarshMallow Permission
     *
     */
    public void checkPermission() {
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext) &&
                checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, mContext) && checkPermission(Manifest.permission.CAMERA, mContext)) {
            openBottomSheet();
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constants.REQUEST_PERMISSION_WRITE_STORAGE);
        }
    }

    protected boolean checkPermission(String strPermission, Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int result = ContextCompat.checkSelfPermission(context, strPermission);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    boolean isPhoto = false;


    /*
     *Resume method of Fragment
     *
     */
    @Override
    public void onResume() {
        super.onResume();
        mContext = getActivity();
        if (isPhoto) {
            isPhoto = false;
            openBottomSheet();
        }
    }


    /*
     *onRequestPermissionsResult method when request comes
     *  @Params int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_PERMISSION_WRITE_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                isPhoto = true;
            }
        }
    }

    private BottomSheetBehavior behavior;
    private CameraIntentHelper mCameraIntentHelper;
    private String mImagePath = "";
    private static final String TAG = "AddCardPhotoActivity";
    private Context mContext;

    /*
     * BottomSheetOpenig Method
     */
    private void openBottomSheet() {
        setupCameraIntentHelper();
        if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setPeekHeight(0);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    /*
     * Gallery Open Method
     */
    void openGallery() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Constants.REQUEST_OPEN_GALLERY);
    }


    /*
     * Camera Open Method
     */
    void openCamera() {
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext) &&
                checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, mContext) && checkPermission(Manifest.permission.CAMERA, mContext)) {

            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            behavior.setPeekHeight(0);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                File photoFile = FileUtils.createImageFile();
                mImagePath = photoFile.getAbsolutePath();

                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mCameraIntentHelper != null) {
                mCameraIntentHelper.startCameraIntent();
            }
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constants.REQUEST_PERMISSION_WRITE_STORAGE);
            //showSnackbar(binding.getRoot(), Constants.ErrorStoragePermission);
        }
    }

    /**
     * @param savedInstanceState Start Camera Intent handler
     */

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (mCameraIntentHelper != null) {
            mCameraIntentHelper.onSaveInstanceState(savedInstanceState);
        }
    }

    /**
     * @param savedInstanceState restore instancestate of  Camera Intent handler
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mCameraIntentHelper != null) {
            mCameraIntentHelper.onRestoreInstanceState(savedInstanceState);
        }
    }

    /**
     * SetUp Camera IntentHelper
     */
    private void setupCameraIntentHelper() {
        mCameraIntentHelper = new CameraIntentHelper(getActivity(), new CameraIntentHelperCallback() {
            @Override
            public void onPhotoUriFound(Date dateCameraIntentStarted, Uri photoUri, int rotateXDegrees) {
                if (photoUri != null) {
                    mImagePath = FileUtils.getPath(mContext, photoUri);
                    Log.e(TAG, "Camera: " + mImagePath);
                    displayImage(photoUri);
                }
            }

            @Override
            public void deletePhotoWithUri(Uri photoUri) {
                BitmapHelper.deleteImageWithUriIfExists(photoUri, mContext);
            }

            @Override
            public void onSdCardNotMounted() {
            }

            @Override
            public void onCanceled() {
            }

            @Override
            public void onCouldNotTakePhoto() {
            }

            @Override
            public void onPhotoUriNotFound() {
            }

            @Override
            public void logException(Exception e) {
                Log.d(getClass().getName(), e.getMessage());
            }

            @Override
            public void onActivityResult(Intent intent, int requestCode) {
                startActivityForResult(intent, requestCode);
            }
        });
    }

    /**
     * OnActivityResult Method
     *
     * @param requestCode resultCode data
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_OPEN_CAMERA) {
                mCameraIntentHelper.onActivityResult(requestCode, resultCode, data);
            } else if (requestCode == Constants.REQUEST_OPEN_GALLERY) {
                getGalleryImageUri(data, mContext);
            }
        }

    }

    /**
     * Fetching GalleryImageUri
     *
     * @param data content
     */
    public void getGalleryImageUri(Intent data, Context context) {
        Uri uri = null;
        String selectedImagePath = "";
        try {
            Uri imageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            Cursor cursor = context.getContentResolver().query(imageUri, projection, null, null,
                    null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            selectedImagePath = cursor.getString(column_index);
            uri = Uri.fromFile(new File(selectedImagePath));
            Log.e(TAG, "Image Gallery" + selectedImagePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        onGalleryUrl(uri, selectedImagePath);
    }

    /**
     * DisplayImage passing Uri
     *
     * @param photoUri
     */

    private void displayImage(Uri photoUri) {
        imgProfile.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(photoUri).asBitmap().into(new BitmapImageViewTarget(imgProfile) {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                super.onResourceReady(resource, glideAnimation);
                mBitmap = resource;
            }
        });
    }

    Bitmap mBitmap = null;
    ;

    /**
     * Getting Uri and image path
     *
     * @param uri ,  selectedImagePath
     */

    public void onGalleryUrl(Uri uri, String selectedImagePath) {
        mImagePath = selectedImagePath;
        displayImage(uri);
    }


}
