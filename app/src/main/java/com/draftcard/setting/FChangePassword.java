package com.draftcard.setting;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.setting.model.Rq_Change_Pwd;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Injecting the Layout to the Fragment
 *
 * @Params(integer layout Id )
 */

@EFragment(R.layout.fragment_change_password)
public class FChangePassword extends BaseFragment {
    private static final String TAG = "";

    /**
     * All the Views are injected here by view Type(ImageView) and Id name(imgProfile)
     */

    @ViewById
    ImageButton btnBack;
    @ViewById
    TextView txtTitle;
    @ViewById
    RelativeLayout rlTitle;
    @ViewById
    EditText edtCurrentPwd;
    @ViewById
    TextInputLayout iptCurrentPwd;
    @ViewById
    EditText edtNewPwd;
    @ViewById
    TextInputLayout iptNewPwd;
    @ViewById
    EditText edtCnfrmPwd;
    @ViewById
    TextInputLayout iptCnfrmPwd;
    @ViewById
    TextView txtChangePwd;
    @ViewById
    LinearLayout main;

    /**
     * AfterViews annotated method is called when Views are injected
     * initiating the views are done here in this method.
     */
    @AfterViews
    public void _Settings() {
        txtTitle.setText(getString(R.string.change_password));
        setToolBar(main);
    }

    /**
     * Click Method for backButton
     */
    @Click
    public void btnBack() {
        closeKeyBoard(getActivity());
        ((SettingsMainActivity) getActivity()).btnBack();
    }

    /**
     * Click Method for ChangePassword Button
     */
    @Click
    public void txtChangePwd() {
        Rq_Change_Pwd rq_change_pwd = new Rq_Change_Pwd();
        rq_change_pwd.old_password = edtCurrentPwd.getText().toString().trim();
        rq_change_pwd.password = edtNewPwd.getText().toString().trim();
        rq_change_pwd.password_confirmation = edtCnfrmPwd.getText().toString().trim();
        if (isValidate(rq_change_pwd)) {
            HashMap<String, Object> authData = new HashMap<>();
            authData.put("old_password", rq_change_pwd.old_password);
            authData.put("password", rq_change_pwd.password);
            authData.put("password_confirmation", rq_change_pwd.password_confirmation);
//            authData.put("token", SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""));
            callChangePwd(authData);
        } else {
            showSnackbar(alertMsg);
        }
    }

    /**
     * Api Calling For ChangePassword Method
     */
    private void callChangePwd(HashMap<String, Object> resetpwd) {
        if (AppUtilz.isNetworkAvailable(getActivity())) {
            AppUtilz.showProgressDialog(getContext());
            final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().changePwd(SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""), resetpwd);
            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            showSnackbar(getString(R.string.unexpected_error));
                        }

                        @Override
                        public void onNext(Response<ResponseBody> loginResponse) {
//                            Log.e(TAG, "onNext: " + loginResponse.getMessage());

                            try {
                                byte[] byteArray = loginResponse.body().bytes();
                                String responseInString = new String(byteArray);
                                JSONObject jsonObject = null;
                                jsonObject = new JSONObject(responseInString);
                                if (jsonObject.getBoolean("success")) {
                                    SharedPreferenceUtil.putValue(AppUtilz.PASSWORD, edtNewPwd.getText().toString().trim());
                                    SharedPreferenceUtil.save();
                                    ((SettingsMainActivity) getActivity()).btnBack();
                                } else {
                                    showSnackbar(jsonObject.getString("message"));
                                }
                            } catch (Exception e) {

                            }
                        }
                    });
        } else {
            showSnackbar(getString(R.string.no_internet));
        }
    }

    String alertMsg = "";

    /*
     *  Validating Data
     * @Params rq_change_pwd
     */
    public boolean isValidate(Rq_Change_Pwd rq_change_pwd) {
        if (TextUtils.isEmpty(rq_change_pwd.old_password)) {
            alertMsg = getString(R.string.alrt_curr_pwd);
            return false;
        } else if (!rq_change_pwd.old_password.equalsIgnoreCase(SharedPreferenceUtil.getString(AppUtilz.PASSWORD, ""))) {
            alertMsg = getString(R.string.alrt_corr_curr_pwd);
            return false;
        } else if (TextUtils.isEmpty(rq_change_pwd.password)) {
            alertMsg = getString(R.string.alrt_new_pwd);
            return false;
        } else if (rq_change_pwd.password.length() < 6) {
            alertMsg = getString(R.string.six_character_pwd);
            return false;
        } else if (TextUtils.isEmpty(rq_change_pwd.password_confirmation)) {
            alertMsg = getString(R.string.alrt_cn_pwd);
            return false;
        } else if (!rq_change_pwd.password.equalsIgnoreCase(rq_change_pwd.password_confirmation)) {
            alertMsg = getString(R.string.alrt_same_cn_pwd);
            return false;
        }
        return true;
    }


}
