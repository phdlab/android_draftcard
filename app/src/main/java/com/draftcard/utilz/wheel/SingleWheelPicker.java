package com.draftcard.utilz.wheel;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.draftcard.R;

import java.util.Arrays;
import java.util.List;

public class SingleWheelPicker extends LinearLayout {

    public static final boolean IS_CURVED_DEFAULT = false;

    private static final int VISIBLE_ITEM_COUNT_DEFAULT = 7;

    private WheelPickerAdapter wheelPickerAdapter;

    private Listener listener;

    private int textColor;
    private int selectedTextColor;
    private int textSize;
    private boolean isCurved;
    private int visibleItemCount;

    public SingleWheelPicker(Context context) {
        this(context, null);
    }

    public SingleWheelPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SingleWheelPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);

        //inflate(context, R.layout.single_day_picker, this);
        setGravity(Gravity.CENTER);
        wheelPickerAdapter = new WheelPickerAdapter(context);
        wheelPickerAdapter.setItemAlign(Gravity.CENTER);
        wheelPickerAdapter.setAtmospheric(true);

        addView(wheelPickerAdapter);

        wheelPickerAdapter.setOnValueSelectedListener(new WheelPickerAdapter.OnValueSelectedListener() {

            @Override
            public void onHourSelected(WheelPickerAdapter picker, int position, String value) {
                updateListener();
            }

            @Override
            public void onHourCurrentScrolled(WheelPickerAdapter picker, int position, String value) {

            }

            @Override
            public void onHourCurrentNewDay(WheelPickerAdapter picker) {

            }
        });

        updatePicker();
    }

    public void setData(List<String> data) {
        wheelPickerAdapter.setData(data);
    }

    private void updatePicker() {
        if (wheelPickerAdapter != null) {
            for (WheelPicker wheelPicker : Arrays.asList(wheelPickerAdapter)) {
                wheelPicker.setItemTextColor(textColor);
                wheelPicker.setSelectedItemTextColor(selectedTextColor);
                wheelPicker.setItemTextSize(textSize);
                wheelPicker.setVisibleItemCount(visibleItemCount);
                wheelPicker.setCurved(isCurved);
            }
        }


    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public String getValue() {
        String hour = wheelPickerAdapter.getCurrentValue();

        return hour;
    }

    private void updateListener() {
        final String date = getValue();
        if (listener != null) {
            listener.onDateChanged("displayed", date);
        }
    }

    public void setSelectedItemPosition(int position){
        wheelPickerAdapter.setSelectedItemPosition(position);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SingleWheelPicker);

        final Resources resources = getResources();
        textColor = a.getColor(R.styleable.SingleWheelPicker_picker_textColor,
                resources.getColor(R.color.picker_default_text_color));
        selectedTextColor = a.getColor(R.styleable.SingleWheelPicker_picker_selectedTextColor,
                resources.getColor(R.color.picker_default_selected_text_color));
        textSize = a.getDimensionPixelSize(R.styleable.SingleWheelPicker_picker_textSize,
                resources.getDimensionPixelSize(R.dimen.WheelItemTextSize));
        isCurved = a.getBoolean(R.styleable.SingleWheelPicker_picker_curved, IS_CURVED_DEFAULT);
        visibleItemCount = a.getInt(R.styleable.SingleWheelPicker_picker_visibleItemCount, VISIBLE_ITEM_COUNT_DEFAULT);

        a.recycle();
    }

    public interface Listener {
        void onDateChanged(String displayed, String value);
    }
}