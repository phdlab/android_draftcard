package com.draftcard.utilz.wheel;

import android.content.Context;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.List;

public class WheelPickerAdapter extends WheelPicker {

    public static final int STEP_DEFAULT = 1;

    private OnValueSelectedListener valueSelectedListener;

    private int minValue = 0;

    private int lastScrollPosition;

    private WheelPicker.Adapter adapter;

    private List<String> data=new ArrayList<>();

    public WheelPickerAdapter(Context context) {
        this(context, null);
    }


    public WheelPickerAdapter(Context context, AttributeSet attrs) {
        super(context, attrs);
        //initAdapter();
    }

    public void setData(List<String> data) {
        this.data = data;
        initAdapter();
    }

    private void initAdapter() {
        final List<String> values = new ArrayList<>();

        for (int hour = minValue; hour <= data.size()-1; hour += STEP_DEFAULT) {
            values.add(data.get(hour));
        }

        adapter = new Adapter(values);
        setAdapter(adapter);

    }

    @Override
    protected void onItemSelected(int position, Object item) {
        if (valueSelectedListener != null) {
            valueSelectedListener.onHourSelected(this, position, (String) item);
        }
    }

    @Override
    protected void onItemCurrentScroll(int position, Object item) {
        if (valueSelectedListener != null) {
            valueSelectedListener.onHourCurrentScrolled(this, position, (String) item);
        }

        if (lastScrollPosition != position) {
            valueSelectedListener.onHourCurrentScrolled(this, position, (String) item);
            if (lastScrollPosition == data.size()-1 && position == 0)
                if (valueSelectedListener != null) {
                    valueSelectedListener.onHourCurrentNewDay(this);
                }
            lastScrollPosition = position;
        }
    }

    @Override
    public int findIndexOfDate(String value) {
        return super.findIndexOfDate(value);
    }

    public void setOnValueSelectedListener(OnValueSelectedListener valueSelectedListener) {
        this.valueSelectedListener = valueSelectedListener;
    }

    public String getCurrentValue() {
        return (String) adapter.getItem(getCurrentItemPosition());
    }

    public interface OnValueSelectedListener {
        void onHourSelected(WheelPickerAdapter picker, int position, String value);

        void onHourCurrentScrolled(WheelPickerAdapter picker, int position, String value);

        void onHourCurrentNewDay(WheelPickerAdapter picker);
    }
}