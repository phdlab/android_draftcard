package com.draftcard.utilz;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by SYS087 on 08-Mar-17.
 */

public class InputFilterMax implements InputFilter {

    public enum FilterType {

        INTEGER,
        FLOAT
    }

    private FilterType mFilterType = FilterType.INTEGER;

    private int max;
    private float maxFloat;

    public InputFilterMax(int max) {
        this.max = max;
    }


    public InputFilterMax(String max, FilterType filterType) {
        switch (filterType) {
            case INTEGER:
                this.max = Integer.parseInt(max);
                break;
            case FLOAT:
                this.maxFloat = Float.parseFloat(max);
                break;
        }

        this.mFilterType = filterType;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            String replacement = source.subSequence(start, end).toString();

            String newVal = dest.toString().substring(0, dstart) + replacement +dest.toString().substring(dend, dest.toString().length());


            switch (mFilterType) {
                case INTEGER:
                    int input = Integer.parseInt(newVal);
                    if (input<=max)
                        return null;
                    break;
                case FLOAT:
                    float inputFloat = Float.parseFloat(newVal);
                    if (inputFloat<=maxFloat)
                        return null;
                    break;
            }




        } catch (NumberFormatException nfe) { }
        //Maybe notify user that the value is not good
        return "";
    }
}
