package com.draftcard.utilz;

/**
 * Created by AND001 on 2/23/2017.
 */

public class Constants {


    public static final String POSITION = "pos";
    public static final int REQUEST_OPEN_GALLERY = 101;
    public static final int REQUEST_PERMISSION_WRITE_STORAGE = 102;
    public static final String EXTRA_IS_FROM_SCHOOL = "is_from";
    public static int REQUEST_OPEN_CAMERA = 1000;
}
