package com.draftcard.utilz;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.draftcard.R;
import com.draftcard.dashboard.model.SportsModel;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.setting.model.Rq_Update_Profile;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by AND001 on 2/23/2017.
 */

public class AppUtilz {

    public static final String PASSWORD = "password";
    public static final String DEVICE_TYPE = "device_type";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String GPA = "gpa";
    public static final String GRAD_YEAR = "grad_year";
    public static final String SCHOOL_NAME = "school_name";
    public static final String SCHOOL_CITY = "school_city";
    public static final String SCHOOL_MASCOT = "school_mascot";
    public static final String SECURITY_NUMBER = "security_number";
    public static final String SOCIAL_TYPE = "social_type";
    public static final String IS_LOGIN = "is_login";
    public static final String PASSWORD_CONFIRMATION = "password_confirmation";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String SPORTS_DATA = "sports_data";
    public static final int MAX_AGE_LIMIT = 24;
    public static final int MIN_AGE_LIMIT = 6;
    public static final int MIN_GRADE_LIMIT = 6;
    public static final int MAX_GRADE_LIMIT = 12;
    public static final String MAX_WEIGHT = "500";
    public static final String MAX_GPA = "5";
    public static final String TEMP_CARD_BG_PATH = "temp_card_bg";
    public static final String CARD_BG = "card_bg";
    public static final String CARD_TYPE = "card_type";
    public static final String NAME = "name";
    public static final String LOCATION = "location";
    public static final String TEAM = "team";
    public static final String SPORT_ID = "sport_id";
    public static final String GRADE = "grade";
    public static final String LEVEL = "level";
    public static final String POSITION = "position";
    public static final String NUMBER = "number";
    public static final String AGE = "age";
    public static final String VIDEO_THUMB = "temp_video_thumb";
    public static final String IS_CARD_ADD_SUCCESS = "is_card_add_success";
    public static final String HEIGHT = "height";
    public static final String WEIGHT = "weight";
    public static final String RELOAD_CARDS = "reload_cards";
    public static final String CARD_ID = "card_id";


    public static String deviceType = "Android";
    private static Dialog progressDial;

    public static final String ID = "id";
    public static final String USER_ID = "user_id";
    public static final String IS_REGISTERED = "is_registered";
    public static final String EMAIL = "email";
    public static final String USER_NAME = "user_name";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String GENDER = "gender";
    public static final String DOB = "dob";
    public static final String ADDRESS = "address";
    public static final String AVTAR = "avtar";
    public static final String GOAL = "goal";
    public static final String MEASURE_IN = "measure_in";
    public static final String HEIGHTS = "heights";
    public static final String WEIGHTS = "weights";
    public static final String IS_ACTIVE = "is_active";
    public static final String IS_PAID = "is_paid";
    public static final String SOCIAL_ID = "social_id";
    public static final String TOKEN = "token";


    /**
     * Get Data From the Shared Preference and fill the Model.
     */
    public static Rq_Update_Profile getUserData() {
        Rq_Update_Profile rq_update_profile = new Rq_Update_Profile();
        rq_update_profile.gpa = SharedPreferenceUtil.getString(AppUtilz.GPA, "");
        rq_update_profile.profile_pic = SharedPreferenceUtil.getString(AppUtilz.AVTAR, "");
        rq_update_profile.dob = SharedPreferenceUtil.getString(AppUtilz.DOB, "");
        rq_update_profile.grad_year = SharedPreferenceUtil.getString(AppUtilz.GRAD_YEAR, "");
        rq_update_profile.first_name = SharedPreferenceUtil.getString(AppUtilz.FIRST_NAME, "");
        rq_update_profile.password = SharedPreferenceUtil.getString(AppUtilz.PASSWORD, "");
        rq_update_profile.last_name = SharedPreferenceUtil.getString(AppUtilz.LAST_NAME, "");
        rq_update_profile.school_city = SharedPreferenceUtil.getString(AppUtilz.SCHOOL_CITY, "");
        rq_update_profile.school_mascot = SharedPreferenceUtil.getString(AppUtilz.SCHOOL_MASCOT, "");
        rq_update_profile.school_name = SharedPreferenceUtil.getString(AppUtilz.SCHOOL_NAME, "");
        rq_update_profile.gender = SharedPreferenceUtil.getString(AppUtilz.GENDER, "");
        rq_update_profile.user_id = SharedPreferenceUtil.getString(AppUtilz.ID, "");
        rq_update_profile.email = SharedPreferenceUtil.getString(AppUtilz.EMAIL, "");
        rq_update_profile.token = SharedPreferenceUtil.getString(AppUtilz.TOKEN, "");
        return rq_update_profile;
    }

    /**
     * Get sports data for card details. User will select sport first and then selects position
     * as per the selected sport.
     *
     * @return SportsModel
     */
    public static SportsModel getSportsData() {
        SportsModel sportsModel = new SportsModel();
        String sportsData = SharedPreferenceUtil.getString(AppUtilz.SPORTS_DATA, "");
        if (!TextUtils.isEmpty(sportsData)) {
            try {
                byte[] byteArray = sportsData.getBytes();
                String responseInString = new String(byteArray);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(responseInString);
                    if (jsonObject.getBoolean("success")) {
                        /*convert data object into sports class*/
                        Gson gson = new Gson();
                        sportsModel = gson.fromJson(jsonObject.toString(), SportsModel.class);
                        return sportsModel;
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }


    /**
     * Progress Dialog for network call
     *
     * @param context
     */
//    public static void showProgressDialog(Context context) {
//        try {
//            if (progressDial == null) {
//                stopProgressDialog();
//                progressDial = new ProgressDialog(context);
//                progressDial.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                progressDial.show();
//                progressDial.setCancelable(false);
//                progressDial.setContentView(R.layout.loading_box);
//                ProgressBar progressBar = (ProgressBar) progressDial.findViewById(R.id.progressBar1);
//                progressBar.getIndeterminateDrawable().setColorFilter(0xFF087CAE, android.graphics.PorterDuff.Mode.MULTIPLY);
//            } else {
//                if (!progressDial.isShowing())
//                    progressDial.show();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    /**
     * Progress Dialog for network call
     *
     * @param context
     */
    public static void showProgressDialog(Context context) {
        try {
            if (progressDial != null) {
                if (progressDial.isShowing())
                    progressDial.dismiss();
                progressDial = null;
            }
            progressDial = new Dialog(context);
            progressDial.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDial.setCancelable(true);
            progressDial.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDial.setContentView(R.layout.progress_loader);
            progressDial.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Progress Dialog for network call
     *
     * @param context
     */
    public static void showProgressDialog(Context context, String msg) {
        try {
            if (progressDial != null) {
                if (progressDial.isShowing())
                    progressDial.dismiss();
                progressDial = null;
            }
            progressDial = new Dialog(context);
            progressDial.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDial.setCancelable(true);
            progressDial.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDial.setContentView(R.layout.progress_loader);
            progressDial.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stop Progress dialog
     */
    public static void stopProgressDialog() {
        try {
            if (progressDial != null && progressDial.isShowing()) {
                progressDial.dismiss();
                progressDial = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Close KeyBoard programmatically when needed
     *
     * @param context
     * @param view
     */
    public static void closeKeyBoard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Check Network availability and return true or false
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    /**
     * Show Time Into Given Format
     *
     * @param date , currentStringFormat , desireTimeFormat
     * @return string
     */
    public static String getTimeInGivenFormat(String date, String currentStringFormat, String desireTimeFormat) {
        if (date == null || currentStringFormat == null || desireTimeFormat == null) {
            return null;
        }
        String strCurrentDate = date;
        SimpleDateFormat format = new SimpleDateFormat(currentStringFormat, Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat(desireTimeFormat, Locale.getDefault());
        String desireDate = format.format(newDate);

        return desireDate;
    }

    /**
     * Validating the email string
     *
     * @param target
     * @return boolean
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    /**
     * Show Image with use of Glide into Image View  by passing url
     *
     * @param activity , profile_pic , imgProfile
     * @return
     */
    public static void showImageNet(Context activity, String profile_pic, ImageView imgProfile) {
        Glide.with(activity)
                .load(profile_pic)
                .centerCrop()
                .crossFade()
                .into(imgProfile);
    }




}
