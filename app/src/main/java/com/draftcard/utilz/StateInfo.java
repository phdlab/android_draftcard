package com.draftcard.utilz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SYS087 on 16-Mar-17.
 */

public class StateInfo {

    public static HashMap<String, String> statelist = new HashMap<>();
    static {
        statelist.put("AL", "Alabama");
        statelist.put("AK", "Alaska");
        statelist.put("AZ", "Arizona");
        statelist.put("AR", "Arkansas");
        statelist.put("CA", "California");
        statelist.put("CO", "Colorado");
        statelist.put("CT", "Connecticut");
        statelist.put("DE", "Delaware");
        statelist.put("DC", "District of Columbia");
        statelist.put("FL", "Florida");
        statelist.put("GA", "Georgia");
        statelist.put("HI", "Hawaii");
        statelist.put("ID", "Idaho");
        statelist.put("IL", "Illinois");
        statelist.put("IN", "Indiana");
        statelist.put("IA", "Iowa");
        statelist.put("KS", "Kansas");
        statelist.put("KY", "Kentucky");
        statelist.put("LA", "Louisiana");
        statelist.put("ME", "Maine");
        statelist.put("MD", "Maryland");
        statelist.put("MA", "Massachusetts");
        statelist.put("MI", "Michigan");
        statelist.put("MN", "Minnesota");
        statelist.put("MS", "Mississippi");
        statelist.put("MO", "Missouri");
        statelist.put("MT", "Montana");
        statelist.put("NE", "Nebraska");
        statelist.put("NV", "Nevada");
        statelist.put("NH", "New Hampshire");
        statelist.put("NJ", "New Jersey");
        statelist.put("NM", "New Mexico");
        statelist.put("NY", "New York");
        statelist.put("NC", "North Carolina");
        statelist.put("ND", "North Dakota");
        statelist.put("OH", "Ohio");
        statelist.put("OK", "Oklahoma");
        statelist.put("OR", "Oregon");
        statelist.put("PA", "Pennsylvania");
        statelist.put("RI", "Rhode Island");
        statelist.put("SC", "South Carolina");
        statelist.put("SD", "South Dakota");
        statelist.put("TN", "Tennessee");
        statelist.put("TX", "Texas");
        statelist.put("UT", "Utah");
        statelist.put("VT", "Vermont");
        statelist.put("VA", "Virginia");
        statelist.put("WA", "Washington");
        statelist.put("WV", "West Virginia");
        statelist.put("WI", "Wisconsin");
        statelist.put("WY", "Wyoming");
    }

    public static List<String> getStateNameLists() {
        List<String> stateList = new ArrayList<>();
        for(Map.Entry<String, String> entry: statelist.entrySet()) {
            stateList.add(entry.getValue());
        }
        return stateList;
    }

    public static String getStateName(String stateKey) {
        return statelist.get(stateKey);
    }

    public static String getStateKey(String stateName) {
        for(Map.Entry<String, String> entry: statelist.entrySet()) {
            if(entry.getValue().equals(stateName)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static List<String> getStateKeyNameLists() {
        List<String> stateList = new ArrayList<>();
        for(Map.Entry<String, String> entry: statelist.entrySet()) {
            stateList.add(entry.getKey() + ": " + entry.getValue());
        }
        return stateList;
    }

    public static String getStateKey(int position) {
        int i = 0;
        for(Map.Entry<String, String> entry: statelist.entrySet()) {
            if(i == position) {
                return entry.getKey();
            }
            i++;
        }
        return null;
    }


    public static String getStateName(int position) {
        int i = 0;
        for(Map.Entry<String, String> entry: statelist.entrySet()) {
            if(i == position) {
                return entry.getValue();
            }
            i++;
        }
        return null;
    }
}
