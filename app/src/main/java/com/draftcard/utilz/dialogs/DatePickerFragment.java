package com.draftcard.utilz.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by SYS087 on 06-Mar-17.
 */
public class DatePickerFragment extends DialogFragment {

    DatePickerDialog.OnDateSetListener listener;
    boolean mBirthDayCal;
    private boolean disablePastDates;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of TimePickerDialog and return it
        DatePickerDialog datePickerDialog =  new DatePickerDialog(getActivity(), listener, year, month, day);
        if(mBirthDayCal)
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        if(disablePastDates) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }
        return datePickerDialog;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener, boolean isBirthDayCal){
        this.listener = listener;
        mBirthDayCal = isBirthDayCal;
    }

    public void setDisablePastDates(boolean disablePastDates) {
        this.disablePastDates = disablePastDates;
    }
}