package com.draftcard.prelogin.view;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.draftcard.R;
import com.draftcard.dashboard.view.DashboardActivity_;
import com.draftcard.prelogin.model.SignupRequestModel;
import com.draftcard.prelogin.viewhandler.LoginHandler;
import com.draftcard.prelogin.viewhandler.SignupHandler;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.Constants;
import com.draftcard.utilz.FileUtils;
import com.draftcard.utilz.InputFilterMax;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.draftcard.utilz.camera.BitmapHelper;
import com.draftcard.utilz.camera.CameraIntentHelper;
import com.draftcard.utilz.camera.CameraIntentHelperCallback;
import com.draftcard.utilz.dialogs.DatePickerFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

@EFragment(R.layout.fragment_sign_up)
public class SignUpGeneralInfoFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    @ViewById
    ImageButton btnBack;
    @ViewById
    TextView txtTitle;
    @ViewById
    RelativeLayout rlTitle;
    @ViewById
    ImageView ivProfilePic;
    @ViewById
    EditText edtFirstName;
    @ViewById
    EditText edtLastName;
    @ViewById
    EditText edtEmail;
    @ViewById
    EditText edtPassword;
    @ViewById
    LinearLayout llPhaseOne;
    @ViewById
    EditText edtDob;
    @ViewById
    Spinner edtGender;
    @ViewById
    Spinner edtGraduation;
    @ViewById
    EditText edtGPA;
    @ViewById
    LinearLayout llPhaseTwo;
    @ViewById
    TextView tvNext;
    @ViewById
    TextView txtSignUp;
    @ViewById
    LinearLayout main;
    @ViewById
    RelativeLayout rlCamera;
    @ViewById
    RelativeLayout rlGallery;
    @ViewById
    LinearLayout linBottomSheet;
    private LoginHandler mLoginHandler;
    private SignupHandler mSignupHandler;
    private SignupRequestModel genralRequestModel;

    private String dateOfBirthVal;
    private boolean isEditProfileStyle = false;
    private String profilePic = "";

//
//    private BottomSheetBehavior behavior;
//    private String imagePath = "";
//    private File imagePatientPic = null;

    public interface SignUpCallBack {
        void onResponse(boolean isSucess, String message);
    }

    @AfterViews
    public void _AfterViews() {
        mLoginHandler.showToolBar(false);
        txtTitle.setText(getString(R.string.general_info));
        edtGPA.setFilters(new InputFilter[]{new InputFilterMax(AppUtilz.MAX_GPA, InputFilterMax.FilterType.FLOAT)});
    }

    @AfterViews
    void test() {
        renderGenderView();
        renderGraduationYear();
        renderSocialFBView();
        initBottomSheet();
    }

    private void renderSocialFBView() {
        if (isEditProfileStyle) {
            if (!TextUtils.isEmpty(SharedPreferenceUtil.getString(AppUtilz.PROFILE_PIC, ""))) {
                Log.e(SignUpGeneralInfoFragment.class.getSimpleName(), "02");
                ivProfilePic.setVisibility(View.VISIBLE);
                Glide.with(getActivity()).load(SharedPreferenceUtil.getString(AppUtilz.PROFILE_PIC, ""))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(ivProfilePic);
            }

            edtFirstName.setText(SharedPreferenceUtil.getString(AppUtilz.FIRST_NAME, ""));
            edtLastName.setText(SharedPreferenceUtil.getString(AppUtilz.LAST_NAME, ""));
            edtEmail.setText(SharedPreferenceUtil.getString(AppUtilz.EMAIL, ""));
            edtEmail.setEnabled(false);
            edtPassword.setVisibility(View.GONE);

            if (genralRequestModel == null) {
                genralRequestModel = new SignupRequestModel();
            }
            genralRequestModel.setProfile_pic(SharedPreferenceUtil.getString(AppUtilz.PROFILE_PIC, ""));
            genralRequestModel.setUser_id(SharedPreferenceUtil.getString(AppUtilz.ID, ""));
            genralRequestModel.setFirst_name(edtFirstName.getText().toString());
            genralRequestModel.setLast_name(edtLastName.getText().toString());
            genralRequestModel.setEmail(edtEmail.getText().toString());

        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mLoginHandler = (LoginActivity) context;
            mSignupHandler = (SignupHandler) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Click
    public void tvNext() {
        if (llPhaseOne.getVisibility() == View.VISIBLE) {

            /*check validation*/
            if (genralRequestModel == null) {
                genralRequestModel = new SignupRequestModel();
            }

            genralRequestModel.setFirst_name(edtFirstName.getText().toString());
            genralRequestModel.setLast_name(edtLastName.getText().toString());
            genralRequestModel.setEmail(edtEmail.getText().toString());
            genralRequestModel.setPassword(edtPassword.getText().toString());
            genralRequestModel.setProfile_pic(mImagePath);
            boolean isValidationDone;

            if (isEditProfileStyle) {
                isValidationDone = mSignupHandler.validateGeneralInfo(genralRequestModel, true);
            } else {
                isValidationDone = mSignupHandler.validateGeneralInfo(genralRequestModel, false);
            }


            if (isValidationDone) {

                if (!isEditProfileStyle) {
                    /*check email address exist or not.*/
                    mSignupHandler.checkEmailAddress(genralRequestModel.getEmail(), new SignUpCallBack() {
                        @Override
                        public void onResponse(boolean isSuccess, String message) {
                            if (isSuccess) {
                                Log.e(SignUpGeneralInfoFragment.class.getSimpleName(), "onCardAdded()");
                                _CallPhaseTwo();
                            } else {
                            /*Show alert dialog....or show it in presenter*/
                            }

                        }

                    });
                } else {
                    _CallPhaseTwo();
                }

            }

        }
    }

      /*
      *  Click method for back Button
      *
      */

    @Click
    public void btnBack() {
        if (isDialogOpen()) {
            closeDialog();
        } else if (isPhaseTwo()) {
            _CallPhaseOne();
        } else {
            ((LoginActivity) getActivity()).ivBack();
        }
    }

    public void _CallPhaseTwo() {
        llPhaseOne.setVisibility(View.GONE);
        llPhaseTwo.setVisibility(View.VISIBLE);
        txtTitle.setText(R.string.athelte_profile);
        tvNext.setVisibility(View.GONE);
        txtSignUp.setVisibility(View.VISIBLE);
    }

    public void _CallPhaseOne() {
        llPhaseTwo.setVisibility(View.GONE);
        llPhaseOne.setVisibility(View.VISIBLE);
        txtTitle.setText(getString(R.string.general_info));
        tvNext.setVisibility(View.VISIBLE);
        txtSignUp.setVisibility(View.GONE);

    }

    public boolean isPhaseTwo() {
        return llPhaseTwo.getVisibility() == View.VISIBLE ? true : false;
    }

    @Click
    public void edtDob() {
        openDatePickerDialog();
    }

    @Click
    public void txtSignUp() {
        Log.e("SignUpCalled-->", "ClickCalled");

        if (llPhaseTwo.getVisibility() == View.VISIBLE) {

            if (genralRequestModel == null) {
                return;
            }

            genralRequestModel.setDob(dateOfBirthVal);
            genralRequestModel.setGender(edtGender.getSelectedItem().toString());
            genralRequestModel.setGrad_year(edtGraduation.getSelectedItem().toString());

            if(!TextUtils.isEmpty(edtGPA.getText().toString()) && '.' == edtGPA.getText().toString().charAt(edtGPA.getText().toString().length() - 1)) {
                edtGPA.setText(edtGPA.getText().toString().substring(0, edtGPA.getText().toString().length() - 1));
            }

            genralRequestModel.setGpa(edtGPA.getText().toString());

            boolean isValidAthleticProfile = mSignupHandler.validateAthleticProfile(genralRequestModel);

            if (isValidAthleticProfile) {

                if (!isEditProfileStyle) {

                    mSignupHandler.onSignUpCall(genralRequestModel, new SignUpCallBack() {
                        @Override
                        public void onResponse(boolean isSucess, String message) {
                            if (isSucess) {
                                // NAVIGATION
                                DashboardActivity_.intent(getActivity()).start();
                                getActivity().finish();
                            }
                        }
                    });
                } else {
                    Log.e(SignUpGeneralInfoFragment.class.getSimpleName(), "Edit profile call need to be done...");
                    Log.e(SignUpGeneralInfoFragment.class.getSimpleName(), "User id: " + genralRequestModel.getUser_id());
                    mSignupHandler.onEditProfileCall(genralRequestModel, new SignUpCallBack() {
                        @Override
                        public void onResponse(boolean isSucess, String message) {
                            if (isSucess) {
                                // NAVIGATION
                                DashboardActivity_.intent(getActivity()).start();
                                getActivity().finish();
                            } else {
                                Toast.makeText(getActivity(), "Social sign up failed...", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        }
    }


    private void openDatePickerDialog() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setListener(this, true);
        newFragment.show(getChildFragmentManager(), "DatePicker");
    }

    private void renderGenderView() {
        ArrayList<String> genderArrayList = new ArrayList<>();
        genderArrayList.add(getString(R.string.gender));
        genderArrayList.add(getString(R.string.male));
        genderArrayList.add(getString(R.string.female));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, genderArrayList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtGender.setAdapter(dataAdapter);
    }

    private void renderGraduationYear() {
        ArrayList<String> graduationArrayList = new ArrayList<>();
        graduationArrayList.add(getString(R.string.graduation_year));
        /*calculating graduation year as per Iphone*/
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int maxYear = year + 10;
        int minYear = maxYear - 19;
        for (int i = maxYear; i >= minYear; i--) {
            graduationArrayList.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, graduationArrayList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        edtGraduation.setAdapter(dataAdapter);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        dateOfBirthVal = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        String dateOfBirth = AppUtilz.getTimeInGivenFormat(dateOfBirthVal, "yyyy-MM-dd", "dd MMM yyyy");
        edtDob.setText(dateOfBirth);
    }

    public void isEditProfileStyle(boolean _isEditProfileStyle) {
        this.isEditProfileStyle = _isEditProfileStyle;
    }

    @Click
    public void ivProfilePic() {
        /*open bottom sheet to select photo from gallary or take new photo*/
        checkPermission();
    }

    /*
        * Click Method of Open Camera
        *
        */
    @Click
    public void rl_camera() {
        openCamera();
    }

    /*
     * Click Method of Open Gallery
     *
     */
    @Click
    public void rl_gallery() {
        openGallery();
    }

    /*
   * Checking MarshMallow Permission
   *
   */
    public void checkPermission() {
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext) &&
                checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, mContext) && checkPermission(Manifest.permission.CAMERA, mContext)) {
            openBottomSheet();
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constants.REQUEST_PERMISSION_WRITE_STORAGE);
        }
    }

    protected boolean checkPermission(String strPermission, Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int result = ContextCompat.checkSelfPermission(context, strPermission);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    boolean isPhoto = false;


    /*
     *Resume method of Fragment
     *
     */
    @Override
    public void onResume() {
        super.onResume();
        mContext = getActivity();
        if (isPhoto) {
            isPhoto = false;
            openBottomSheet();
        }
    }

    /*
     *onRequestPermissionsResult method when request comes
     *  @Params int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_PERMISSION_WRITE_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                isPhoto = true;
            }
        }
    }

    private BottomSheetBehavior behavior;
    private CameraIntentHelper mCameraIntentHelper;
    private String mImagePath = "";
    private static final String TAG = "AddCardPhotoActivity";
    private Context mContext;

    /*
     * BottomSheetOpenig Method
     */
    private void openBottomSheet() {
        setupCameraIntentHelper();
        if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setPeekHeight(0);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    /**
     * Initialising the bottom sheet
     */
    private void initBottomSheet() {
        behavior = BottomSheetBehavior.from(linBottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
    }
      /*
       *  Validating Dialouge is open or not
       *
       */

    public boolean isDialogOpen() {
        return (!(behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED));
    }

    /*
     *  CloseDialog Method
     *
     */
    public void closeDialog() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
    }

    /*
     * Gallery Open Method
     */
    void openGallery() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Constants.REQUEST_OPEN_GALLERY);
    }


    /*
     * Camera Open Method
     */
    void openCamera() {
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext) &&
                checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, mContext) && checkPermission(Manifest.permission.CAMERA, mContext)) {

            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            behavior.setPeekHeight(0);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                File photoFile = FileUtils.createImageFile();
                mImagePath = photoFile.getAbsolutePath();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mCameraIntentHelper != null) {
                mCameraIntentHelper.startCameraIntent();
            }
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constants.REQUEST_PERMISSION_WRITE_STORAGE);
            //showSnackbar(binding.getRoot(), Constants.ErrorStoragePermission);
        }
    }

    /**
     * @param savedInstanceState Start Camera Intent handler
     */

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (mCameraIntentHelper != null) {
            mCameraIntentHelper.onSaveInstanceState(savedInstanceState);
        }
    }

    /**
     * @param savedInstanceState restore instancestate of  Camera Intent handler
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mCameraIntentHelper != null) {
            mCameraIntentHelper.onRestoreInstanceState(savedInstanceState);
        }
    }

    /**
     * SetUp Camera IntentHelper
     */
    private void setupCameraIntentHelper() {
        mCameraIntentHelper = new CameraIntentHelper(getActivity(), new CameraIntentHelperCallback() {
            @Override
            public void onPhotoUriFound(Date dateCameraIntentStarted, Uri photoUri, int rotateXDegrees) {
                if (photoUri != null) {
                    mImagePath = FileUtils.getPath(mContext, photoUri);
                    Log.e(TAG, "Camera: " + mImagePath);
                    displayImage(photoUri);
                }
            }

            @Override
            public void deletePhotoWithUri(Uri photoUri) {
                BitmapHelper.deleteImageWithUriIfExists(photoUri, mContext);
            }

            @Override
            public void onSdCardNotMounted() {
            }

            @Override
            public void onCanceled() {
            }

            @Override
            public void onCouldNotTakePhoto() {
            }

            @Override
            public void onPhotoUriNotFound() {
            }

            @Override
            public void logException(Exception e) {
                Log.d(getClass().getName(), e.getMessage());
            }

            @Override
            public void onActivityResult(Intent intent, int requestCode) {
                startActivityForResult(intent, requestCode);
            }
        });
    }

    /**
     * OnActivityResult Method
     *
     * @param requestCode resultCode data
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_OPEN_CAMERA) {
                mCameraIntentHelper.onActivityResult(requestCode, resultCode, data);
            } else if (requestCode == Constants.REQUEST_OPEN_GALLERY) {
                getGalleryImageUri(data, mContext);
            }
        }

    }

    /**
     * Fetching GalleryImageUri
     *
     * @param data content
     */
    public void getGalleryImageUri(Intent data, Context context) {
        Uri uri = null;
        String selectedImagePath = "";
        try {
            Uri imageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            Cursor cursor = context.getContentResolver().query(imageUri, projection, null, null,
                    null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            selectedImagePath = cursor.getString(column_index);
            uri = Uri.fromFile(new File(selectedImagePath));
            Log.e(TAG, "Image Gallery" + selectedImagePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        onGalleryUrl(uri, selectedImagePath);
    }

    /**
     * DisplayImage passing Uri
     *
     * @param photoUri
     */

    private void displayImage(Uri photoUri) {
        ivProfilePic.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(photoUri).into(ivProfilePic);
    }


    /**
     * Getting Uri and image path
     *
     * @param uri ,  selectedImagePath
     */

    public void onGalleryUrl(Uri uri, String selectedImagePath) {
        mImagePath = selectedImagePath;
        displayImage(uri);
    }
}