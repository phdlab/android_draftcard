package com.draftcard.prelogin.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.draftcard.R;
import com.draftcard.prelogin.viewhandler.ForgotPasswordHandler;
import com.draftcard.prelogin.viewhandler.LoginHandler;
import com.draftcard.prelogin.viewhandler.SignInHandler;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;


/**
 * Created by SYS087 on 07-Mar-17.
 */

@EFragment(R.layout.fragment_forgot_pwd)
public class ForgotPasswordFragment extends Fragment {

    private static final String TAG = ForgotPasswordFragment.class.getSimpleName();

    private LoginHandler mLoginHandler;
    private ForgotPasswordHandler mForgotPasswordHandler;

    @ViewById
    LinearLayout llPhaseOne;

    @ViewById
    LinearLayout llPhaseTwo;

    @ViewById
    LinearLayout rlRetrivePwd;

    @ViewById
    EditText edtEmail;

    @ViewById
    TextView tvForgotPwdErrorMsg;

    @ViewById
    TextView tvSNErrorMsg;

    @ViewById
    EditText edtSecurityNo;

    @ViewById
    TextView tvDoneMsg;

    @ViewById
    TextView tvDone;

    @ViewById
    LinearLayout rlConfirm;

    @ViewById
    TextView tvConfirm;

    @ViewById
    LinearLayout llInvalidSN;

    @ViewById
    TextView tvResend;

    @ViewById
    LinearLayout llPhaseThree;

    @ViewById
    LinearLayout rlConfirmPwd;

    @ViewById
    EditText edtNewPwd;

    @ViewById
    EditText edtCnfPwd;

    @ViewById
    TextView tvRetrivePwd;

    private String token = "";

    public interface ForgotPasswordCallback {
        void onResponse(boolean isSuccess, Object object);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLoginHandler.showToolBar(true);
        mLoginHandler.setToolbarTittle("");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mLoginHandler = (LoginActivity) context;
            mForgotPasswordHandler = (LoginActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Click
    public void rlRetrivePwd() {
        android.util.Log.e(TAG, "rlRetrivePwd()");
        mForgotPasswordHandler.validateEmail(edtEmail.getText().toString(), new ForgotPasswordCallback() {
            @Override
            public void onResponse(boolean isSuccess, Object object) {
                if (isSuccess) {
                    llPhaseOne.setVisibility(View.GONE);
                    rlRetrivePwd.setVisibility(View.GONE);
                    llPhaseTwo.setVisibility(View.VISIBLE);
                    rlConfirm.setVisibility(View.VISIBLE);
                } else {
                    tvForgotPwdErrorMsg.setVisibility(View.VISIBLE);
                    tvForgotPwdErrorMsg.setText((String) object);
                }
            }
        });
    }

    @Click
    public void rlConfirm() {
        mForgotPasswordHandler.validateSecurityNumber(edtSecurityNo.getText().toString(), new ForgotPasswordCallback() {
            @Override
            public void onResponse(boolean isSuccess, Object object) {
                if (isSuccess) {
                    token = (String) object;
                    llPhaseTwo.setVisibility(View.GONE);
                    rlConfirm.setVisibility(View.GONE);
                    rlConfirmPwd.setVisibility(View.VISIBLE);
                    llPhaseThree.setVisibility(View.VISIBLE);
                } else {
                    llInvalidSN.setVisibility(View.VISIBLE);
                    tvSNErrorMsg.setVisibility(View.VISIBLE);
                    tvSNErrorMsg.setText((String) object);
                }
            }
        });
    }

    @Click
    public void tvResend() {
        mForgotPasswordHandler.validateEmail(edtEmail.getText().toString(), new ForgotPasswordCallback() {
            @Override
            public void onResponse(boolean isSuccess, Object object) {
                if (isSuccess) {
                    rlRetrivePwd.setVisibility(View.GONE);
                    tvRetrivePwd.setVisibility(View.GONE);
                    llPhaseOne.setVisibility(View.GONE);
                    llPhaseTwo.setVisibility(View.VISIBLE);
                    rlConfirm.setVisibility(View.VISIBLE);
                    llInvalidSN.setVisibility(View.GONE);
                } else {
                    tvForgotPwdErrorMsg.setVisibility(View.VISIBLE);
                    tvForgotPwdErrorMsg.setText((String) object);
                }
            }
        });
    }

    @Click
    public void rlConfirmPwd() {
        mForgotPasswordHandler.resetPassword(edtNewPwd.getText().toString(), edtCnfPwd.getText().toString(), token,  new ForgotPasswordCallback() {
            @Override
            public void onResponse(boolean isSuccess, Object object) {
                if (isSuccess) {
                    // NAVIGATION
                    mLoginHandler.onBackPressed();
                } else {
//                    tvForgotPwdErrorMsg.setVisibility(View.VISIBLE);
//                    tvForgotPwdErrorMsg.setText((String) object);
                }
            }
        });
    }

}
