package com.draftcard.prelogin.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.draftcard.R;
import com.draftcard.dashboard.view.DashboardActivity_;
import com.draftcard.prelogin.viewhandler.LoginHandler;
import com.draftcard.prelogin.viewhandler.SignInHandler;
import com.draftcard.prelogin.viewhandler.SignupHandler;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by SYS087 on 07-Mar-17.
 */
@EFragment(R.layout.fragment_signin)
public class SignInFragment extends Fragment {

    private static final String TAG = SignInFragment.class.getSimpleName();

    public interface SignInCallback {
        void onResponse(boolean isSuccess, Object response);
    }

    private LoginHandler mLoginHandler;
    private SignInHandler mSignInHandler;

    @ViewById
    EditText edtEmail;

    @ViewById
    EditText edtPassword;

    @ViewById
    LinearLayout rlSignIn;

    @ViewById
    TextView tvSignIn;

    @ViewById
    TextView tvSignUp;

    @ViewById
    TextView tvForgotPwd;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLoginHandler.showToolBar(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mLoginHandler = (LoginActivity) context;
            mSignInHandler = (SignInHandler) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Click
    public void tvSignIn() {
        /*validate credentials*/
        mSignInHandler.validateLogin(edtEmail.getText().toString(), edtPassword.getText().toString(), new SignInCallback() {
            @Override
            public void onResponse(boolean isSuccess, Object response) {

                if(isSuccess) {
                    CommonResponse commonResponse = (CommonResponse) response;
                    if(commonResponse.getData() != null) {
                        /*store data in local*/
//                        Toast.makeText(getActivity(), "Login successful", Toast.LENGTH_SHORT).show();
                        // NAVIGATION
                        SharedPreferenceUtil.putValue(AppUtilz.PASSWORD, edtPassword.getText().toString());
                        SharedPreferenceUtil.save();
                        DashboardActivity_.intent(getActivity()).start();
                        getActivity().finish();
                    }
                }
            }
        });
    }

    @Click
    public void tvSignUp() {
        /*navigate to sign up*/
        mSignInHandler.navigateToSignUp();
    }

    @Click
    public void tvForgotPwd() {
        /*navigate to forgot password*/
        mSignInHandler.navigateToForgotPwd();
    }
}
