package com.draftcard.prelogin.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.draftcard.R;
import com.draftcard.base.BaseActivity;
import com.draftcard.dashboard.view.DashboardActivity_;
import com.draftcard.prelogin.model.SignupRequestModel;
import com.draftcard.prelogin.presenter.LoginPresenter;
import com.draftcard.prelogin.presenter.SignUpPresenter;
import com.draftcard.prelogin.viewhandler.ForgotPasswordHandler;
import com.draftcard.prelogin.viewhandler.LoginHandler;
import com.draftcard.prelogin.viewhandler.SignInHandler;
import com.draftcard.prelogin.viewhandler.SignupHandler;
import com.draftcard.utilz.AppUtilz;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Stack;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity implements LoginHandler, SignupHandler, SignInHandler, ForgotPasswordHandler {

    Stack fragmentStack = new Stack();
    @ViewById
    ImageView ivFbLogin;
    @ViewById
    RelativeLayout rlSignUp;
    @ViewById
    TextView tvSignIn;
    @ViewById
    FrameLayout loginContent;
    @ViewById
    ImageView ivBack;
    @ViewById
    TextView tvTittle;
    @ViewById
    RelativeLayout rlToolBar;

    @ViewById
    LinearLayout llSignIn;

    private CallbackManager callbackManager;
    private LoginPresenter loginPresenter;
    private SignUpPresenter mSignUpPresenter;

    @AfterViews
    void test() {
        setStatusBarTranslucent(true);
        loginPresenter = new LoginPresenter(this);
        mSignUpPresenter = new SignUpPresenter(this, LoginActivity.this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        showToolBar(false);
    }

    /**
     * Make status bar  transparent and
     * It will work only for API level 19 and above.
     *
     * @param makeTranslucent
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    Fragment f;

    public void _GetCurrentFragment() {
        f = getSupportFragmentManager().findFragmentById(getContentView().getId());
    }

    @Click
    public void ivFbLogin() {
        closeKeyBoard(this);
        loginPresenter.onFacebookAuthenticate();
    }

    @Click
    public void rlSignUp() {
        closeKeyBoard(this);
        addFragment(new SignUpGeneralInfoFragment_.FragmentBuilder_().build(), R.string.signup);
    }


    @Click
    public void llSignIn() {
        closeKeyBoard(this);
        addFragment(new SignInFragment_.FragmentBuilder_().build(), R.string.signin);
    }

    @Click
    public void ivBack() {
        onBackPressed();
    }

    @Override
    public FrameLayout getContentView() {
        return loginContent;
    }


    @Override
    public Context getContext() {
        return LoginActivity.this;
    }

    @Override
    public CallbackManager getCallBackManager() {
        return callbackManager;
    }

    @Override
    public void addFragment(Fragment fragment, int tag) {
        fragmentStack.push(tag);
        getSupportFragmentManager().beginTransaction()
                .add(getContentView().getId(), fragment, getString(tag)).addToBackStack(getString(tag)).commit();
    }

    @Override
    public void replaceFragment(Fragment fragment, int tag) {
        fragmentStack.clear();
        fragmentStack.push(tag);
        while (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        getSupportFragmentManager().beginTransaction().replace(getContentView().getId(), fragment, getString(tag)).commit();
    }

    @Override
    public void onSuccessfullyLogin() {
        //showToast("Successfully login");
        DashboardActivity_.intent(LoginActivity.this).start();
        finish();
    }

    @Override
    public void onSuccessfullySocialLogin() {
        closeKeyBoard(this);
        SignUpGeneralInfoFragment signUpGeneralInfoFragment_ = new SignUpGeneralInfoFragment_.FragmentBuilder_().build();
        signUpGeneralInfoFragment_.isEditProfileStyle(true);
        addFragment(signUpGeneralInfoFragment_, R.string.signup);
    }

    @Override
    public void onBackPressed() {
        AppUtilz.closeKeyBoard(getContext(), getContentView());
        _GetCurrentFragment();
        if (f != null && f.getTag().equalsIgnoreCase(getString(R.string.signup)) && ((SignUpGeneralInfoFragment) f).isDialogOpen()) {
            ((SignUpGeneralInfoFragment) f).closeDialog();
        } else if ((f != null && f.getTag().equalsIgnoreCase(getString(R.string.signup)) && ((SignUpGeneralInfoFragment) f).isPhaseTwo())) {
            ((SignUpGeneralInfoFragment) f)._CallPhaseOne();
        } else {
            if (fragmentStack != null & !fragmentStack.isEmpty()) {
                fragmentStack.pop();
            }
            boolean showToolBar = false;
            if(getSupportFragmentManager() != null && getSupportFragmentManager().getFragments() != null) {
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment != null && fragment.getTag() != null &&
                            fragment.getTag().equals(getString(R.string.sign_up))) {
                        showToolBar = true;
                    }
                }

            }

            showToolBar(showToolBar);
            super.onBackPressed();
        }
    }

    @Override
    public void ShowError(String str) {
        showSnackbar(getContentView(), str);
    }

    @Override
    public void ShowError(int str) {
        showSnackbar(getContentView(), getString(str));
    }

    @Override
    public void setToolbarTittle(int str) {
        tvTittle.setText(str);
    }

    @Override
    public void setToolbarTittle(String title) {
        tvTittle.setText(title);
    }

    @Override
    public void showToolBar(boolean isShown) {
        if (isShown)
            rlToolBar.setVisibility(View.VISIBLE);
        else
            rlToolBar.setVisibility(View.GONE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSignUpCall(SignupRequestModel signupRequestModel, SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack) {
        mSignUpPresenter.callSignUpByMail(signupRequestModel, signUpCallBack);
    }

    @Override
    public boolean validateGeneralInfo(SignupRequestModel genralRequestModel, boolean isSocialLogin) {
        return mSignUpPresenter.validateGeneralInfo(genralRequestModel, isSocialLogin);
    }

    @Override
    public boolean validateAthleticProfile(SignupRequestModel genralRequestModel) {
        return mSignUpPresenter.validateAthleticProfile(genralRequestModel);
    }

    @Override
    public void checkEmailAddress(String email, SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack) {
        mSignUpPresenter.checkEmailAddress(email, signUpCallBack);
    }

    @Override
    public View getContentViewForError() {
        return loginContent;
    }

    @Override
    public void onEditProfileCall(SignupRequestModel genralRequestModel, SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack) {
        mSignUpPresenter.editProfileCall(genralRequestModel, signUpCallBack);
    }

    @Override
    public void validateLogin(String username, String password, SignInFragment.SignInCallback signInCallback) {
        loginPresenter.validateLogin(username, password, signInCallback);
    }

    @Override
    public void navigateToSignUp() {
        closeKeyBoard(this);
        onBackPressed();
    }

    @Override
    public void navigateToForgotPwd() {
        closeKeyBoard(this);
        addFragment(new ForgotPasswordFragment_.FragmentBuilder_().build(), R.string.forgot_pwd);
    }

    @Override
    public void validateEmail(String email, ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback) {
        loginPresenter.validateEmail(email, forgotPasswordCallback);
    }

    @Override
    public void validateSecurityNumber(String securityNumber, ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback) {
        loginPresenter.validateSecurityNumber(securityNumber, forgotPasswordCallback);
    }

    @Override
    public void resetPassword(String newPwd, String cnfPwd, String token, ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback) {
        loginPresenter.resetPassword(newPwd, cnfPwd, token, forgotPasswordCallback);
    }
}
