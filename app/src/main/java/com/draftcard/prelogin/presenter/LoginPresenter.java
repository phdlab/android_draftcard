package com.draftcard.prelogin.presenter;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.draftcard.R;
import com.draftcard.prelogin.view.ForgotPasswordFragment;
import com.draftcard.prelogin.view.SignInFragment;
import com.draftcard.prelogin.viewhandler.LoginHandler;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.rest.response.CommonResponseArray;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import org.androidannotations.annotations.App;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by SYS092 on 3/3/2017.
 */

public class LoginPresenter {

    private static final String TAG = "LoginPresenter";
    private LoginHandler mLoginHandler;

    public LoginPresenter(LoginHandler loginHandler) {
        this.mLoginHandler = loginHandler;
    }

    public void onFacebookAuthenticate() {
        try {
            LoginManager.getInstance().logInWithReadPermissions((Activity) mLoginHandler.getContext(), Arrays.asList("email", "public_profile", "user_birthday"));
            LoginManager.getInstance().registerCallback(mLoginHandler.getCallBackManager(),
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(final LoginResult loginResult) {
                            // App code
                            GraphRequest request = GraphRequest.newMeRequest(
                                    loginResult.getAccessToken(),
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(
                                                JSONObject object, GraphResponse response) {
                                            HashMap<String, Object> mapRequest = new HashMap<>();
                                            mapRequest.put("social_id", object.optString("id"));
                                            mapRequest.put("social_type", "facebook");

                                            mapRequest.put("email", object.optString("email"));

                                            mapRequest.put("name", object.optString("name"));

                                            mapRequest.put("gender", object.optString("gender"));

                                            mapRequest.put("profile_pic", object.optJSONObject("picture").optJSONObject("data").optString("url"));

                                            mapRequest.put("device_type", AppUtilz.deviceType);
                                            mapRequest.put("device_token", "123456789098765431234567890");
                                            callSocialMediaLogin(mapRequest);
                                        }
                                    });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,name,email,gender, birthday,picture");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            Log.e("Facebook", "onCancel: ");
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            mLoginHandler.ShowError(exception.toString());
                            Log.e("Facebook", "onError() called with: " + "exception = [" + exception.toString() + "]");
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callSocialMediaLogin(final HashMap<String, Object> mapRequest) {
        if (AppUtilz.isNetworkAvailable(mLoginHandler.getContext())) {

            AppUtilz.showProgressDialog(mLoginHandler.getContext(), "");

            final Observable<CommonResponse> responseUserObservable = NetWorkUtils.getApiService().socialSignUp(mapRequest);
            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<CommonResponse>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            mLoginHandler.ShowError(R.string.unexpected_error);
                        }

                        @Override
                        public void onNext(CommonResponse loginResponse) {
                            Log.e(TAG, "onNext: " + loginResponse.getMessage() + " isSuccess? " + loginResponse.isSuccess());
                            if (loginResponse.isSuccess()) {

                                SharedPreferenceUtil.putValue(AppUtilz.ID, loginResponse.getData().getId());
                                SharedPreferenceUtil.putValue(AppUtilz.EMAIL, loginResponse.getData().getEmail());

                                SharedPreferenceUtil.putValue(AppUtilz.USER_NAME, loginResponse.getData().getFirst_name());
                                SharedPreferenceUtil.putValue(AppUtilz.FIRST_NAME, loginResponse.getData().getFirst_name());
                                SharedPreferenceUtil.putValue(AppUtilz.LAST_NAME, loginResponse.getData().getLast_name());

                                SharedPreferenceUtil.putValue(AppUtilz.GENDER, loginResponse.getData().getGender());
                                SharedPreferenceUtil.putValue(AppUtilz.DOB, loginResponse.getData().getDob());
                                SharedPreferenceUtil.putValue(AppUtilz.ADDRESS, loginResponse.getData().getSchool_city());
                                SharedPreferenceUtil.putValue(AppUtilz.AVTAR, loginResponse.getData().getProfile_pic());
                                SharedPreferenceUtil.putValue(AppUtilz.SCHOOL_CITY, loginResponse.getData().getSchool_city());
                                SharedPreferenceUtil.putValue(AppUtilz.SCHOOL_MASCOT, loginResponse.getData().getSchool_mascot());
                                SharedPreferenceUtil.putValue(AppUtilz.GRAD_YEAR, loginResponse.getData().getGrad_year());
                                SharedPreferenceUtil.putValue(AppUtilz.GPA, loginResponse.getData().getGpa());
                                SharedPreferenceUtil.putValue(AppUtilz.DEVICE_TYPE, loginResponse.getData().getDevice_type());
                                SharedPreferenceUtil.putValue(AppUtilz.DEVICE_TOKEN, loginResponse.getData().getDevice_token());
                                SharedPreferenceUtil.putValue(AppUtilz.SECURITY_NUMBER, loginResponse.getData().getSecurity_number());
                                SharedPreferenceUtil.putValue(AppUtilz.SOCIAL_TYPE, loginResponse.getData().getSocial_type());
                                SharedPreferenceUtil.putValue(AppUtilz.IS_REGISTERED, false);
                                SharedPreferenceUtil.putValue(AppUtilz.IS_ACTIVE, loginResponse.getData().getIs_active());
                                SharedPreferenceUtil.putValue(AppUtilz.SOCIAL_ID, loginResponse.getData().getSocial_id());
                                SharedPreferenceUtil.putValue(AppUtilz.TOKEN, loginResponse.getData().getToken());
                                SharedPreferenceUtil.putValue(AppUtilz.IS_LOGIN, true);
                                Log.e(TAG, "Profile_pic: " + mapRequest.get("profile_pic").toString());
                                SharedPreferenceUtil.putValue(AppUtilz.PROFILE_PIC, mapRequest.get("profile_pic").toString());
                                SharedPreferenceUtil.save();

                                if(!TextUtils.isEmpty(AppUtilz.GPA)) {
                                    mLoginHandler.onSuccessfullyLogin();
                                }else {
                                    mLoginHandler.onSuccessfullySocialLogin();
                                }


                            } else {
                                mLoginHandler.ShowError(loginResponse.getMessage());
                            }
                        }
                    });
        } else {
            Snackbar.make(mLoginHandler.getContentView(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }

    public void validateLogin(final String username, final String password, final SignInFragment.SignInCallback signInCallback) {

        if (username == null || password == null || signInCallback == null) {
            return;
        }

        String alertMsg = "";

        /*check validations for login*/
        if (TextUtils.isEmpty(username)) {
            alertMsg = mLoginHandler.getContext().getString(R.string.enter_email);
            signInCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_email));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        }  else if (!AppUtilz.isValidEmail(username)) {
            alertMsg = mLoginHandler.getContext().getString(R.string.enter_valid_email);
            signInCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_valid_email));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        } else if (TextUtils.isEmpty(password)) {
            alertMsg = mLoginHandler.getContext().getString(R.string.enter_password);
            signInCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_password));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        } else if (password.length() < 6) {
            alertMsg = mLoginHandler.getContext().getString(R.string.six_character_pwd);
            signInCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.six_character_pwd));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        }

        if(TextUtils.isEmpty(alertMsg)) {

             /*call webservice to do login*/
            if (AppUtilz.isNetworkAvailable(mLoginHandler.getContext())) {
                AppUtilz.showProgressDialog(mLoginHandler.getContext(), "");

                HashMap<String, String> singinMap = new HashMap<>();
                singinMap.put(AppUtilz.EMAIL, username);
                singinMap.put(AppUtilz.PASSWORD, password);
                singinMap.put(AppUtilz.DEVICE_TYPE, "ANDROID");
            /*TODO? Replace device token with unique device id in app application*/
                singinMap.put(AppUtilz.DEVICE_TOKEN, "1234567890");

                final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().signIn(singinMap);

                responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<Response<ResponseBody>>() {
                            @Override
                            public void onCompleted() {
                                AppUtilz.stopProgressDialog();
                                Log.e(TAG, "onCompleted: ");
                            }

                            @Override
                            public void onError(Throwable e) {
                                AppUtilz.stopProgressDialog();
                                Log.e(TAG, "onError: " + e.toString());
                                mLoginHandler.ShowError(R.string.unexpected_error);
                            }

                            @Override
                            public void onNext(Response<ResponseBody> loginResponse) {

                                try {
                                    byte[] byteArray = loginResponse.body().bytes();
                                    String responseInString = new String(byteArray);
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = new JSONObject(responseInString);
                                        if (jsonObject.getBoolean("success")) {
                                        /*convert data object into commonresponse class*/
                                            Gson gson = new Gson();
                                            CommonResponse sighUpResponse = gson.fromJson(jsonObject.toString(), CommonResponse.class);

                                            SharedPreferenceUtil.putValue(AppUtilz.ID, sighUpResponse.getData().getId());
                                            SharedPreferenceUtil.putValue(AppUtilz.EMAIL, sighUpResponse.getData().getEmail());
                                            SharedPreferenceUtil.putValue(AppUtilz.USER_NAME, sighUpResponse.getData().getFirst_name());
                                            SharedPreferenceUtil.putValue(AppUtilz.FIRST_NAME, sighUpResponse.getData().getFirst_name());
                                            SharedPreferenceUtil.putValue(AppUtilz.LAST_NAME, sighUpResponse.getData().getLast_name());
                                            SharedPreferenceUtil.putValue(AppUtilz.GENDER, sighUpResponse.getData().getGender());
                                            SharedPreferenceUtil.putValue(AppUtilz.DOB, sighUpResponse.getData().getDob());
                                            SharedPreferenceUtil.putValue(AppUtilz.ADDRESS, sighUpResponse.getData().getSchool_city());
                                            SharedPreferenceUtil.putValue(AppUtilz.AVTAR, sighUpResponse.getData().getProfile_pic());
                                            SharedPreferenceUtil.putValue(AppUtilz.IS_ACTIVE, sighUpResponse.getData().getIs_active());
                                            SharedPreferenceUtil.putValue(AppUtilz.SOCIAL_ID, sighUpResponse.getData().getSocial_id());
                                            SharedPreferenceUtil.putValue(AppUtilz.GRAD_YEAR, sighUpResponse.getData().getGrad_year());
                                            SharedPreferenceUtil.putValue(AppUtilz.GPA, sighUpResponse.getData().getGpa());
                                            SharedPreferenceUtil.putValue(AppUtilz.TOKEN, sighUpResponse.getData().getToken());
                                            SharedPreferenceUtil.putValue(AppUtilz.IS_LOGIN, true);
                                            SharedPreferenceUtil.putValue(AppUtilz.PASSWORD, password);
                                            SharedPreferenceUtil.save();

                                            signInCallback.onResponse(true, sighUpResponse);
                                        } else {
                                            showAlert(mLoginHandler.getContext().getString(R.string.alert), jsonObject.getString("message"));
                                        }
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                        });


            } else {
                Snackbar.make(mLoginHandler.getContentView(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
            }
        }



    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(mLoginHandler.getContext(), R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    public void validateEmail(final String email, final ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback) {
        if (email == null || forgotPasswordCallback == null) {
            return;
        }

        /*check validations for login*/
        if (TextUtils.isEmpty(email)) {
            forgotPasswordCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_email));
            return;
        } else if (!AppUtilz.isValidEmail(email)) {
            forgotPasswordCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_valid_email));
            return;
        }

        /*call webservice to do login*/
        if (AppUtilz.isNetworkAvailable(mLoginHandler.getContext())) {

            AppUtilz.showProgressDialog(mLoginHandler.getContext(), "");

            HashMap<String, String> singinMap = new HashMap<>();
            singinMap.put(AppUtilz.EMAIL, email);
            singinMap.put(AppUtilz.DEVICE_TYPE, "ANDROID");
            /*TODO? Replace device token with unique device id in app application*/
            singinMap.put(AppUtilz.DEVICE_TOKEN, "1234567890");

            final Observable<CommonResponseArray> responseUserObservable = NetWorkUtils.getApiService().recoverPassword(singinMap);

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<CommonResponseArray>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            mLoginHandler.ShowError(R.string.unexpected_error);
                        }

                        @Override
                        public void onNext(CommonResponseArray loginResponse) {
                            Log.e(TAG, "onNext: " + loginResponse.getMessage() + " isSuccess? " + loginResponse.isSuccess());
                            if (loginResponse.isSuccess()) {
                                forgotPasswordCallback.onResponse(true, loginResponse);
                            } else {
                                forgotPasswordCallback.onResponse(false, loginResponse.getMessage());
                            }
                        }
                    });


        } else {
            Snackbar.make(mLoginHandler.getContentView(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }

    public void validateSecurityNumber(final String securityNumber, final ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback) {

        if (securityNumber == null || forgotPasswordCallback == null) {
            return;
        }

        /*check validations for login*/
        if (TextUtils.isEmpty(securityNumber)) {
            forgotPasswordCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_email));
            return;
        }

        /*call webservice to do login*/
        if (AppUtilz.isNetworkAvailable(mLoginHandler.getContext())) {
            AppUtilz.showProgressDialog(mLoginHandler.getContext(), "");

            HashMap<String, String> securityNumberMap = new HashMap<>();
            securityNumberMap.put(AppUtilz.SECURITY_NUMBER, securityNumber);
            securityNumberMap.put(AppUtilz.DEVICE_TYPE, "ANDROID");
            /*TODO? Replace device token with unique device id in app application*/
            securityNumberMap.put(AppUtilz.DEVICE_TOKEN, "1234567890");

            final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().confirmNumber(securityNumberMap);

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            mLoginHandler.ShowError(R.string.unexpected_error);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> response) {


                            try {
                                byte[] byteArray = response.body().bytes();
                                String responseInString = new String(byteArray);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(responseInString);
                                    if (jsonObject.getBoolean("success")) {
                                        forgotPasswordCallback.onResponse(true, jsonObject.getJSONObject("data").get("token"));
                                    } else {
                                        forgotPasswordCallback.onResponse(false, jsonObject.getString("message"));
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }


                    });


        } else {
            Snackbar.make(mLoginHandler.getContentView(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }

    }

    public void resetPassword(final String newPwd, final String cnfPwd, final String token, final ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback) {

        if(newPwd == null || cnfPwd == null || token == null || forgotPasswordCallback == null) {
            return;
        }

        String alertMsg = "";

        if (TextUtils.isEmpty(newPwd)) {
            alertMsg = mLoginHandler.getContext().getString(R.string.enter_password);
            forgotPasswordCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_password));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        } else if (newPwd.length() < 6) {
            alertMsg = mLoginHandler.getContext().getString(R.string.six_character_pwd);
            forgotPasswordCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.six_character_pwd));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        } else if (TextUtils.isEmpty(cnfPwd)) {
            alertMsg = mLoginHandler.getContext().getString(R.string.enter_cnf_pwd);
            forgotPasswordCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.enter_cnf_pwd));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        } else if (!newPwd.equals(cnfPwd)) {
            alertMsg =  mLoginHandler.getContext().getString(R.string.pwd_cnf_pwd_not_match);
            forgotPasswordCallback.onResponse(false, mLoginHandler.getContext().getString(R.string.pwd_cnf_pwd_not_match));
            showAlert(mLoginHandler.getContext().getString(R.string.alert), alertMsg);
            return;
        }


        if(TextUtils.isEmpty(alertMsg)) {

        /*call webservice to do login*/
            if (AppUtilz.isNetworkAvailable(mLoginHandler.getContext())) {

                AppUtilz.showProgressDialog(mLoginHandler.getContext(), "");

                HashMap<String, Object> securityNumberMap = new HashMap<>();

                securityNumberMap.put(AppUtilz.TOKEN, token);
                securityNumberMap.put(AppUtilz.PASSWORD, newPwd);
                securityNumberMap.put(AppUtilz.PASSWORD_CONFIRMATION, cnfPwd);

                final Observable<CommonResponseArray> responseUserObservable = NetWorkUtils.getApiService().resetPassword(securityNumberMap);

                responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<CommonResponseArray>() {
                            @Override
                            public void onCompleted() {
                                AppUtilz.stopProgressDialog();
                            }

                            @Override
                            public void onError(Throwable e) {
                                AppUtilz.stopProgressDialog();
                                mLoginHandler.ShowError(R.string.unexpected_error);
                            }

                            @Override
                            public void onNext(CommonResponseArray response) {

                                if(response.isSuccess()) {
                                    forgotPasswordCallback.onResponse(true, "");
                                } else {
                                    showAlert(mLoginHandler.getContext().getString(R.string.alert), response.getMessage());
                                    forgotPasswordCallback.onResponse(false, response.getMessage());
                                }

                            }


                        });


            } else {
                Snackbar.make(mLoginHandler.getContentView(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
            }
        }


    }
}