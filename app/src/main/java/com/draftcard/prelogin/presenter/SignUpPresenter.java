package com.draftcard.prelogin.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.draftcard.R;
import com.draftcard.prelogin.model.SignupRequestModel;
import com.draftcard.prelogin.view.SignUpGeneralInfoFragment;
import com.draftcard.prelogin.viewhandler.LoginHandler;
import com.draftcard.prelogin.viewhandler.SignupHandler;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.rest.response.CommonResponseArray;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.google.gson.Gson;

import org.androidannotations.annotations.App;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import response.EditProfileResponse;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by SYS087 on 06-Mar-17.
 */

public class SignUpPresenter {

    private static final String TAG = SignUpPresenter.class.getSimpleName();

    private SignupHandler mSignupHandler;
    private LoginHandler mLoginHandler;

    private Context mContext;

    public SignUpPresenter(SignupHandler signUpHandler, Context mContext) {
        this.mSignupHandler = signUpHandler;
        this.mContext = mContext;
    }

    public void callSignUpByMail(final SignupRequestModel signupRequestModel, final SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack) {
        if (AppUtilz.isNetworkAvailable(mContext)) {

            AppUtilz.showProgressDialog(mContext, "");

            HashMap<String, String> signUpMap = new HashMap<>();
            signUpMap.put(AppUtilz.FIRST_NAME, signupRequestModel.getFirst_name());
            signUpMap.put(AppUtilz.LAST_NAME, signupRequestModel.getLast_name());
            signUpMap.put(AppUtilz.EMAIL, signupRequestModel.getEmail());
            signUpMap.put(AppUtilz.PASSWORD, signupRequestModel.getPassword());
            signUpMap.put(AppUtilz.DOB, signupRequestModel.getDob());
            signUpMap.put(AppUtilz.GENDER, signupRequestModel.getGender());
            signUpMap.put(AppUtilz.DEVICE_TYPE, "ANDROID");
            signUpMap.put(AppUtilz.DEVICE_TOKEN, "1234567890");
            signUpMap.put(AppUtilz.GPA, signupRequestModel.getGpa());
            signUpMap.put(AppUtilz.GRAD_YEAR, signupRequestModel.getGrad_year());
            signUpMap.put(AppUtilz.SCHOOL_NAME, signupRequestModel.getSchool_name());
            signUpMap.put(AppUtilz.SCHOOL_CITY, signupRequestModel.getSchool_city());
            signUpMap.put(AppUtilz.SCHOOL_MASCOT, signupRequestModel.getSchool_mascot());


            final Observable<CommonResponse> responseUserObservable = NetWorkUtils.getApiService().signUp(signUpMap);
            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<CommonResponse>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");

                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            signUpCallBack.onResponse(false, "Error");

                        }

                        @Override
                        public void onNext(CommonResponse sighUpResponse) {
                            Log.e(TAG, "onNext: " + sighUpResponse.getMessage() + " isSuccess? " + sighUpResponse.isSuccess());
                            if (sighUpResponse.isSuccess()) {
                                Log.e(TAG, "Signup response success...");
                                SharedPreferenceUtil.putValue(AppUtilz.ID, sighUpResponse.getData().getId());
                                SharedPreferenceUtil.putValue(AppUtilz.EMAIL, sighUpResponse.getData().getEmail());
                                SharedPreferenceUtil.putValue(AppUtilz.USER_NAME, sighUpResponse.getData().getFirst_name());
                                SharedPreferenceUtil.putValue(AppUtilz.FIRST_NAME, sighUpResponse.getData().getFirst_name());
                                SharedPreferenceUtil.putValue(AppUtilz.LAST_NAME, sighUpResponse.getData().getLast_name());
                                SharedPreferenceUtil.putValue(AppUtilz.GENDER, sighUpResponse.getData().getGender());
                                SharedPreferenceUtil.putValue(AppUtilz.DOB, sighUpResponse.getData().getDob());
                                SharedPreferenceUtil.putValue(AppUtilz.ADDRESS, sighUpResponse.getData().getSchool_city());
                                SharedPreferenceUtil.putValue(AppUtilz.AVTAR, sighUpResponse.getData().getProfile_pic());
                                SharedPreferenceUtil.putValue(AppUtilz.IS_ACTIVE, sighUpResponse.getData().getIs_active());
                                SharedPreferenceUtil.putValue(AppUtilz.SOCIAL_ID, sighUpResponse.getData().getSocial_id());
                                SharedPreferenceUtil.putValue(AppUtilz.GRAD_YEAR, sighUpResponse.getData().getGrad_year());
                                SharedPreferenceUtil.putValue(AppUtilz.GPA, sighUpResponse.getData().getGpa());
                                SharedPreferenceUtil.putValue(AppUtilz.TOKEN, sighUpResponse.getData().getToken());
                                SharedPreferenceUtil.putValue(AppUtilz.PASSWORD, signupRequestModel.getPassword());
                                SharedPreferenceUtil.putValue(AppUtilz.IS_LOGIN, true);
                                SharedPreferenceUtil.save();
                                signUpCallBack.onResponse(true, null);
                                mLoginHandler.onSuccessfullyLogin();
                            } else {
                                showAlert(mContext.getString(R.string.alert), sighUpResponse.getMessage());
                                signUpCallBack.onResponse(false, sighUpResponse.getMessage());
                            }
                        }
                    });
        } else {
            Snackbar.make(mSignupHandler.getContentViewForError(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }

    public boolean validateGeneralInfo(SignupRequestModel genralRequestModel, boolean isSocialLogin) {

        if (genralRequestModel == null) {
            return false;
        }

        String alertMsg = "";


        if (TextUtils.isEmpty(genralRequestModel.getFirst_name())) {
            alertMsg = mContext.getString(R.string.enter_firstname);
        } else if (TextUtils.isEmpty(genralRequestModel.getLast_name())) {
            alertMsg = mContext.getString(R.string.enter_lastname);
        } else if (TextUtils.isEmpty(genralRequestModel.getEmail())) {
            alertMsg = mContext.getString(R.string.enter_email);
        } else if (!AppUtilz.isValidEmail(genralRequestModel.getEmail())) {
            alertMsg = mContext.getString(R.string.enter_valid_email);
        }
        if (!TextUtils.isEmpty(alertMsg)) {
            showAlert(mContext.getString(R.string.alert), alertMsg);
            return false;
        } else if (!isSocialLogin) {
            if (TextUtils.isEmpty(genralRequestModel.getPassword())) {
                alertMsg = mContext.getString(R.string.enter_password);
            } else if (genralRequestModel.getPassword().length() < 6) {
                alertMsg = mContext.getString(R.string.six_character_pwd);
            }
        }


        if (!TextUtils.isEmpty(alertMsg)) {
            showAlert(mContext.getString(R.string.alert), alertMsg);
            return false;
        }


        return true;


    }

    public boolean validateAthleticProfile(SignupRequestModel genralRequestModel) {

        if (genralRequestModel == null) {
            return false;
        }

        String alertMsg = "";

        if (TextUtils.isEmpty(genralRequestModel.getDob())) {
            alertMsg = mContext.getString(R.string.enter_dob);
        } else if (TextUtils.isEmpty(genralRequestModel.getGender()) || genralRequestModel.getGender().equals(mContext.getString(R.string.gender))) {
            alertMsg = mContext.getString(R.string.select_gender);
        } else if (TextUtils.isEmpty(genralRequestModel.getGrad_year()) || genralRequestModel.getGrad_year().equals(mContext.getString(R.string.graduation_year))) {
            alertMsg = mContext.getString(R.string.select_graduation_year);
        } else if (TextUtils.isEmpty(genralRequestModel.getGpa())) {
            alertMsg = mContext.getString(R.string.enter_gpa);
        } else {
            String gpa = genralRequestModel.getGpa();
            float gpaval = Float.parseFloat(gpa);
            int gpaFinal = (int) gpaval;

            if(gpaFinal > 5) {
                alertMsg = mContext.getString(R.string.enter_gpa_less_then_five);
            }
        }


        if (!TextUtils.isEmpty(alertMsg)) {
            showAlert(mContext.getString(R.string.alert), alertMsg);
            return false;
        }


        return true;
    }

    public void checkEmailAddress(final String email, final SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack) {
        if (email == null || signUpCallBack == null) {
            return;
        }

        if (AppUtilz.isNetworkAvailable(mContext)) {

            AppUtilz.showProgressDialog(mContext, "");

            HashMap<String, String> emailMap = new HashMap<>();
            emailMap.put("email", email);

            final Observable<CommonResponseArray> responseUserObservable = NetWorkUtils.getApiService().emailVerification(emailMap);
            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<CommonResponseArray>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            signUpCallBack.onResponse(false, "Error");

                        }

                        @Override
                        public void onNext(CommonResponseArray loginResponse) {
                            Log.e(TAG, "onNext: " + loginResponse.getMessage() + " isSuccess? " + loginResponse.isSuccess());
                            if (loginResponse.isSuccess()) {
                                signUpCallBack.onResponse(true, null);
                            } else {
                                showAlert(mContext.getString(R.string.alert), loginResponse.getMessage());
                                signUpCallBack.onResponse(false, loginResponse.getMessage());
                            }
                        }
                    });
        } else {
            Snackbar.make(mSignupHandler.getContentViewForError(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(mContext, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    public void editProfileCall(SignupRequestModel genralRequestModel, final SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack) {
        if (genralRequestModel == null || signUpCallBack == null) {
            return;
        }

        if (AppUtilz.isNetworkAvailable(mContext)) {

            AppUtilz.showProgressDialog(mContext, "");

            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getUser_id());
            RequestBody firstName = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getFirst_name());
            RequestBody lastName = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getLast_name());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getEmail());
            RequestBody dob = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getDob());
            RequestBody gpa = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getGpa());
            RequestBody schoolName = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getSchool_name());
            RequestBody schoolCity = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getSchool_city());
            RequestBody schoolMascot = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getSchool_mascot());
            RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getGender());
            RequestBody gradYear = RequestBody.create(MediaType.parse("text/plain"), genralRequestModel.getGrad_year());


            MultipartBody.Part profilePicImage;

            if (!TextUtils.isEmpty(genralRequestModel.getProfile_pic())) {
                File profilePicFile = new File(genralRequestModel.getProfile_pic());
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), profilePicFile);
                profilePicImage = MultipartBody.Part.createFormData(AppUtilz.PROFILE_PIC, profilePicFile.getName(), requestFile);
            } else {
                RequestBody requestFile = RequestBody.create(MediaType.parse("text/plain"), "");
                profilePicImage = MultipartBody.Part.createFormData("profilepic", "", requestFile);
            }

            final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().editProfile(SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""),
                    userId,
                    firstName, lastName, email, dob, gpa, schoolName, schoolCity, schoolMascot, gender, gradYear, profilePicImage);

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            signUpCallBack.onResponse(false, "Error");

                        }

                        @Override
                        public void onNext(Response<ResponseBody> loginResponse) {


                            try {
                                byte[] byteArray = loginResponse.body().bytes();
                                String responseInString = new String(byteArray);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(responseInString);
                                    if (jsonObject.getBoolean("success")) {
                                        /*convert data object into commonresponse class*/
                                        Gson gson = new Gson();
                                        EditProfileResponse sighUpResponse = gson.fromJson(jsonObject.toString(), EditProfileResponse.class);
                                        SharedPreferenceUtil.putValue(AppUtilz.ID, String.valueOf(sighUpResponse.getData().getId()));
                                        SharedPreferenceUtil.putValue(AppUtilz.EMAIL, sighUpResponse.getData().getEmail());
                                        SharedPreferenceUtil.putValue(AppUtilz.USER_NAME, sighUpResponse.getData().getFirstName());
                                        SharedPreferenceUtil.putValue(AppUtilz.FIRST_NAME, sighUpResponse.getData().getFirstName());
                                        SharedPreferenceUtil.putValue(AppUtilz.LAST_NAME, sighUpResponse.getData().getLastName());
                                        SharedPreferenceUtil.putValue(AppUtilz.GENDER, sighUpResponse.getData().getGender());
                                        SharedPreferenceUtil.putValue(AppUtilz.DOB, sighUpResponse.getData().getDob());
                                        SharedPreferenceUtil.putValue(AppUtilz.ADDRESS, sighUpResponse.getData().getSchoolCity());
                                        SharedPreferenceUtil.putValue(AppUtilz.AVTAR, sighUpResponse.getData().getProfilePic());
                                        SharedPreferenceUtil.putValue(AppUtilz.IS_ACTIVE, sighUpResponse.getData().getIsActive());
                                        SharedPreferenceUtil.putValue(AppUtilz.SOCIAL_ID, sighUpResponse.getData().getSocialId());
                                        SharedPreferenceUtil.putValue(AppUtilz.GRAD_YEAR, sighUpResponse.getData().getGradYear());
                                        SharedPreferenceUtil.putValue(AppUtilz.GPA, sighUpResponse.getData().getGpa());
                                        SharedPreferenceUtil.putValue(AppUtilz.TOKEN, sighUpResponse.getData().getToken());
                                        SharedPreferenceUtil.putValue(AppUtilz.IS_LOGIN, true);
                                        SharedPreferenceUtil.save();
                                        signUpCallBack.onResponse(true, responseInString);
                                    } else {
                                        showAlert(mLoginHandler.getContext().getString(R.string.alert), jsonObject.getString("message"));
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });
        } else {
            Snackbar.make(mSignupHandler.getContentViewForError(), R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }
}
