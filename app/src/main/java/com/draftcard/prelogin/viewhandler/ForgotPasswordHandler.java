package com.draftcard.prelogin.viewhandler;

import com.draftcard.prelogin.view.ForgotPasswordFragment;
import com.draftcard.prelogin.view.SignInFragment;

/**
 * Created by SYS087 on 07-Mar-17.
 */

public interface ForgotPasswordHandler {

    void validateEmail(String email, ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback);
    void validateSecurityNumber(String securityNumber, ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback);
    void resetPassword(String newPwd, String cnfPwd, String token, ForgotPasswordFragment.ForgotPasswordCallback forgotPasswordCallback);
}
