package com.draftcard.prelogin.viewhandler;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import com.facebook.CallbackManager;

/**
 * Created by SYS092 on 3/3/2017.
 */

public interface LoginHandler {

    FrameLayout getContentView();

    Context getContext();

    CallbackManager getCallBackManager();

    void addFragment(Fragment fragment, int tag);

    void replaceFragment(Fragment fragment, int tag);

    void onSuccessfullyLogin();

    void onSuccessfullySocialLogin();

    void onBackPressed();

    void ShowError(String str);

    void ShowError(int str);

    void setToolbarTittle(int str);

    void setToolbarTittle(String title);

    void showToolBar(boolean isShown);

}
