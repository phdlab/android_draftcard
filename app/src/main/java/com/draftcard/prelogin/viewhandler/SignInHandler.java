package com.draftcard.prelogin.viewhandler;

import com.draftcard.prelogin.view.SignInFragment;

/**
 * Created by SYS087 on 07-Mar-17.
 */

public interface SignInHandler {

    void validateLogin(String username, String password, SignInFragment.SignInCallback signInCallback);

    void navigateToSignUp();

    void navigateToForgotPwd();
}
