package com.draftcard.prelogin.viewhandler;

import android.view.View;

import com.draftcard.prelogin.model.SignupRequestModel;
import com.draftcard.prelogin.view.SignUpGeneralInfoFragment;

/**
 * Created by SYS087 on 06-Mar-17.
 */

public interface SignupHandler {

    void onSignUpCall(SignupRequestModel signupRequestModel, SignUpGeneralInfoFragment.SignUpCallBack onSignUpCall);

    boolean validateGeneralInfo(SignupRequestModel genralRequestModel, boolean isSocialLogin);

    boolean validateAthleticProfile(SignupRequestModel genralRequestModel);

    void checkEmailAddress(String email, SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack);

    View getContentViewForError();

    void onEditProfileCall(SignupRequestModel genralRequestModel, SignUpGeneralInfoFragment.SignUpCallBack signUpCallBack);
}

