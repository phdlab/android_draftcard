package com.draftcard.prelogin.model;

/**
 * Created by SYS092 on 3/3/2017.
 */

public class LoginModel {

    String userName;
    String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
