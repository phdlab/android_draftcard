package com.draftcard;

import android.app.Application;

import com.draftcard.utilz.SharedPreferenceUtil;

import org.androidannotations.annotations.EApplication;

/**
 * Created by Andriod-176 on 3/2/2017.
 */

@EApplication
public class DraftCardApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferenceUtil.init(getApplicationContext());
    }

}