package com.draftcard.rest.response;

import java.util.List;

/**
 * Created by SYS087 on 06-Mar-17.
 */

public class CommonResponseArray {
    boolean success;
    String message;
    CommonResponse.Errors errors;
    List<CommonResponse.success> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CommonResponse.Errors getErrors() {
        return errors;
    }

    public void setErrors(CommonResponse.Errors errors) {
        this.errors = errors;
    }

    public List<CommonResponse.success> getData() {
        return data;
    }

    public void setData(List<CommonResponse.success> data) {
        this.data = data;
    }

    public class Errors {

    }

    public class success {

        private String id;
        private String first_name;
        private String last_name;
        private String email;
        private String gender;
        private String dob;
        private String school_city;
        private String school_name;
        private String school_mascot;
        private String profile_pic;
        private String grad_year;
        private String gpa;
        private String is_active;
        private String device_type;
        private String device_token;
        private String social_id;
        private String social_type;
        private String security_number;
        private String token;
        private boolean isRegistered;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getSchool_city() {
            return school_city;
        }

        public void setSchool_city(String school_city) {
            this.school_city = school_city;
        }

        public String getSchool_name() {
            return school_name;
        }

        public void setSchool_name(String school_name) {
            this.school_name = school_name;
        }

        public String getSchool_mascot() {
            return school_mascot;
        }

        public void setSchool_mascot(String school_mascot) {
            this.school_mascot = school_mascot;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getGrad_year() {
            return grad_year;
        }

        public void setGrad_year(String grad_year) {
            this.grad_year = grad_year;
        }

        public String getGpa() {
            return gpa;
        }

        public void setGpa(String gpa) {
            this.gpa = gpa;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getSocial_id() {
            return social_id;
        }

        public void setSocial_id(String social_id) {
            this.social_id = social_id;
        }

        public String getSocial_type() {
            return social_type;
        }

        public void setSocial_type(String social_type) {
            this.social_type = social_type;
        }

        public String getSecurity_number() {
            return security_number;
        }

        public void setSecurity_number(String security_number) {
            this.security_number = security_number;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public boolean isRegistered() {
            return isRegistered;
        }

        public void setRegistered(boolean registered) {
            isRegistered = registered;
        }
    }
}
