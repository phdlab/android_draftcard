package com.draftcard.rest;

import com.draftcard.dashboard.model.SportsModel;
import com.draftcard.dashboard.model.school.SchoolModel;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.rest.response.CommonResponseArray;
import com.draftcard.utilz.AppUtilz;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;
import wrap.code.json.Data2Model;

/**
 * Created by Andriod-176 on 3/2/2017.
 */

public interface RetroFitService {

    @FormUrlEncoded
    @POST(URL.SOCIAL_AUTH)
    Observable<CommonResponse> socialSignUp(@FieldMap HashMap<String, Object> authData);

    @FormUrlEncoded
    @POST(URL.EMAIL_VERIFICATION)
    Observable<CommonResponseArray> emailVerification(@FieldMap HashMap<String, String> emailData);

    @FormUrlEncoded
    @POST(URL.SIGN_UP)
    Observable<CommonResponse> signUp(@FieldMap HashMap<String, String> emailData);

    @FormUrlEncoded
    @POST(URL.SIGN_IN)
    Observable<Response<ResponseBody>> signIn(@FieldMap HashMap<String, String> emailData);

    @FormUrlEncoded
    @POST(URL.FORGOT_PWD)
    Observable<CommonResponseArray> recoverPassword(@FieldMap HashMap<String, String> emailData);

    @FormUrlEncoded
    @POST(URL.CONFIRM_NUMBER)
    Observable<Response<ResponseBody>> confirmNumber(@FieldMap HashMap<String, String> emailData);

    @FormUrlEncoded
    @POST(URL.CHANGE_PWD)
    Observable<Response<ResponseBody>> changePwd(@Query("token") String token, @FieldMap HashMap<String, Object> authData);

    @FormUrlEncoded
    @POST(URL.RESET_PWD)
    Observable<CommonResponse> resetPwd(@FieldMap HashMap<String, Object> authData);

    @FormUrlEncoded
    @POST(URL.RESET_PWD)
    Observable<CommonResponseArray> resetPassword(@FieldMap HashMap<String, Object> authData);

    @FormUrlEncoded
    @POST(URL.UPDATE_PROFILE)
    Observable<CommonResponse> updateProfile(@FieldMap HashMap<String, Object> authData);

    @Multipart
    @POST(URL.EDIT_PROFILE)
    Observable<Response<ResponseBody>> editProfile(@Query("token") String token,
                                                   @Part(AppUtilz.USER_ID) RequestBody userId,
                                                   @Part(AppUtilz.FIRST_NAME) RequestBody firstName,
                                                   @Part(AppUtilz.LAST_NAME) RequestBody lastName,
                                                   @Part(AppUtilz.EMAIL) RequestBody email,
                                                   @Part(AppUtilz.DOB) RequestBody dob,
                                                   @Part(AppUtilz.GPA) RequestBody gpa,
                                                   @Part(AppUtilz.SCHOOL_NAME) RequestBody schoolName,
                                                   @Part(AppUtilz.SCHOOL_CITY) RequestBody schoolCity,
                                                   @Part(AppUtilz.SCHOOL_MASCOT) RequestBody schoolMascot,
                                                   @Part(AppUtilz.GENDER) RequestBody gender,
                                                   @Part(AppUtilz.GRAD_YEAR) RequestBody gradyear,
                                                   @Part MultipartBody.Part file);


    @Data2Model(modelName = "EditProfileResponse", jsonStr = "{\"success\":true,\"message\":\"Profile Updated successfully\",\"errors\":{},\"data\":{\"id\":73,\"first_name\":\"tet\",\"last_name\":\"tet\",\"email\":\"tet@tet.tet\",\"gender\":\"Female\",\"dob\":\"2017-03-08\",\"school_city\":\"\",\"school_name\":\"\",\"school_mascot\":\"\",\"profile_pic\":\"http:\\/\\/dev-build.net\\/draftcard\\/storage\\/app\\/userprofile\\/738343313.jpg\",\"grad_year\":\"2024\",\"gpa\":\"5\",\"is_active\":\"1\",\"device_type\":\"0\",\"device_token\":\"1234567890\",\"social_id\":\"\",\"social_type\":\"0\",\"security_number\":\"\",\"token\":\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjczLCJpc3MiOiJodHRwOlwvXC9kZXYtYnVpbGQubmV0XC9kcmFmdGNhcmRcL2FwaVwvZWRpdF9wcm9maWxlIiwiaWF0IjoxNDg5MDQ3Nzk4LCJleHAiOjE0ODkxMzQxOTgsIm5iZiI6MTQ4OTA0Nzc5OCwianRpIjoiZmQzNDE0MDQ1ZjY4OTdmNzE1NGIyNDcyOTQ0MjQxYzYifQ.mJgLIh3TX9df5qw6O7aIe2AHH9oYOp_dAg2wV_X2NL0\"}}")
    public final String pckName = "response";

//    @Multipart
//    @POST(URL.UPDATE_PROFILE)
//    Observable<CommonResponse> updateProfile(@Query("token") String token, @PartMap HashMap<String, Object> authData, @Part MultipartBody.Part file);

    @GET(URL.SPORTS)
    Observable<Response<ResponseBody>> getSports();

    @Multipart
    @POST(URL.ADD_CARD)
    Observable<Response<ResponseBody>> addCard(@Query(AppUtilz.TOKEN) String token,
                                               @Part(AppUtilz.USER_ID) RequestBody userId,
                                               @Part(AppUtilz.CARD_TYPE) RequestBody cardTyoe,
                                               @Part(AppUtilz.NAME) RequestBody name,
                                               @Part(AppUtilz.LOCATION) RequestBody location,
                                               @Part(AppUtilz.TEAM) RequestBody team,
                                               @Part(AppUtilz.SPORT_ID) RequestBody sportId,
                                               @Part(AppUtilz.GRADE) RequestBody grade,
                                               @Part(AppUtilz.LEVEL) RequestBody level,
                                               @Part(AppUtilz.POSITION) RequestBody position,
                                               @Part(AppUtilz.NUMBER) RequestBody number,
                                               @Part(AppUtilz.AGE) RequestBody age,
                                               @Part(AppUtilz.HEIGHT) RequestBody height,
                                               @Part(AppUtilz.WEIGHT) RequestBody weight,
                                               @Part(AppUtilz.GPA) RequestBody gpa,
                                               @Part MultipartBody.Part cardBg,
                                               @Part MultipartBody.Part videoThumb);

    @FormUrlEncoded
    @POST(URL.GET_CARD)
    Observable<Response<ResponseBody>> getCard(@Query("token") String token, @FieldMap HashMap<String, String> authData);

    @FormUrlEncoded
    @POST(URL.DELETE_CARD)
    Observable<Response<ResponseBody>> deleteCard(@Query("token") String token, @FieldMap HashMap<String, String> authData);

    @GET
    Observable<Response<ResponseBody>> getSchools(@Url String url,
                                                  @Query("token") String token,
                                                  @Query("q") String queryParam,
                                                  @Query("state") String state);
}
