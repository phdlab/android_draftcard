package com.draftcard.rest;

/**
 * Created by SYS092 on 3/3/2017.
 */

public class URL {

    //public static final String BASE_URL = "http://www.devser.net/draftcard/api/";
    public static final String BASE_URL = "http://api.draftcard.com/dev/api/";


    public static final String SOCIAL_AUTH = "auth/social_login";
    public static final String EMAIL_VERIFICATION = "auth/email_verification";
    public static final String SIGN_IN = "auth/login";
    public static final String SIGN_UP = "auth/signup";
    public static final String RESET_PWD= "auth/reset";
    public static final String UPDATE_PROFILE= "auth/edit_profile";
    public static final String CHANGE_PWD = "auth/change_password";
    public static final String FORGOT_PWD = "auth/recovery";
    public static final String CONFIRM_NUMBER = "auth/confirm_number";
    public static final String EDIT_PROFILE = "edit_profile";

    public static final String SPORTS = "getsports";
    public static final String ADD_CARD = "add_card";
    public static final String GET_CARD = "get_card";
    public static final String DELETE_CARD = "delete_card";
}
