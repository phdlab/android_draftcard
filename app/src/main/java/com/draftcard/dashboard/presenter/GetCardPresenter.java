package com.draftcard.dashboard.presenter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.draftcard.R;
import com.draftcard.dashboard.model.getcard.GetCardModel;
import com.draftcard.dashboard.viewhandler.CardDetailHandler;
import com.draftcard.dashboard.viewhandler.ICardDetailFetch;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by SYS087 on 15-Mar-17.
 */

public class GetCardPresenter {

    private Context context;
    private ICardDetailFetch mICardDetailFetch;
    private View mView;
    private static final String TAG = GetCardPresenter.class.getSimpleName();

    public GetCardPresenter(Context _context, ICardDetailFetch _mICardDetailFetch) {
        this.context = _context;
        this.mICardDetailFetch = _mICardDetailFetch;
    }

    public void getCards() {
                     /*call webservice to do login*/
        if (AppUtilz.isNetworkAvailable(context)) {

            AppUtilz.showProgressDialog(context, "");

            HashMap<String, String> getCardMap = new HashMap<>();
            getCardMap.put(AppUtilz.USER_ID, SharedPreferenceUtil.getString(AppUtilz.ID, ""));

            final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().getCard(SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""), getCardMap);

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            mICardDetailFetch.onCardDetailFetched(false, "Error!!!");
                        }

                        @Override
                        public void onNext(Response<ResponseBody> loginResponse) {

                            try {
                                byte[] byteArray = loginResponse.body().bytes();
                                String responseInString = new String(byteArray);
                                Log.e(TAG, "responseString=> " + responseInString);
                                JSONObject jsonObject = null;
                                try {
                                    Log.e(TAG, "responseString=> 1");
                                    jsonObject = new JSONObject(responseInString);
                                    if (jsonObject.getBoolean("success")) {
                                        Log.e(TAG, "responseString=> 2");
                                        /*convert data object into common response class*/
                                        try {
                                            Log.e(TAG, "responseString=> 3");
                                            Gson gson = new Gson();
                                            GetCardModel cardModel = gson.fromJson(jsonObject.toString(), GetCardModel.class);
                                            mICardDetailFetch.onCardDetailFetched(true, responseInString);
                                        } catch (Exception e) {
                                            Log.e(TAG, "responseString=> 4");
                                            e.printStackTrace();
                                            mICardDetailFetch.onCardDetailFetched(false, "Error!!!");
                                        }


                                    } else {
                                        Log.e(TAG, "responseString=> 5");
                                        mICardDetailFetch.onCardDetailFetched(false, "Error!!!");
                                    }
                                } catch (Exception e1) {
                                    Log.e(TAG, "responseString=> 6");
                                    e1.printStackTrace();
                                }
                            } catch (IOException e) {
                                Log.e(TAG, "responseString=> 7");
                                e.printStackTrace();
                            }

                        }
                    });



        } else {
            Snackbar.make(mView, R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }
}
