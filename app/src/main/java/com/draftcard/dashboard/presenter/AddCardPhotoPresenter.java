package com.draftcard.dashboard.presenter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.draftcard.dashboard.viewhandler.AddCardPhotoHandler;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;

import java.io.File;

/**
 * Created by AND001 on 3/3/2017.
 */

public class AddCardPhotoPresenter {

    private static final String TAG = "AddCardPhotoPresenter";

    AddCardPhotoHandler mAddCardPhotoHandler;

    public AddCardPhotoPresenter(AddCardPhotoHandler mAddCardPhotoHandler){
        this.mAddCardPhotoHandler = mAddCardPhotoHandler;
    }

    public void getGalleryImageUri(Intent data, Context context) {
        Uri uri = null;
        String selectedImagePath = "";
        try {
            Uri imageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            Cursor cursor = context.getContentResolver().query(imageUri, projection, null, null,
                    null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            selectedImagePath = cursor.getString(column_index);

            SharedPreferenceUtil.putValue(AppUtilz.TEMP_CARD_BG_PATH, selectedImagePath);
            SharedPreferenceUtil.save();

            uri= Uri.fromFile(new File(selectedImagePath));
            Log.e(TAG, "Image Gallery" + selectedImagePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAddCardPhotoHandler.onGalleryUrl(uri, selectedImagePath);
    }
}
