package com.draftcard.dashboard.presenter;

import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.dashboard.model.Position;
import com.draftcard.dashboard.model.Sport;
import com.draftcard.dashboard.model.SportsModel;
import com.draftcard.dashboard.view.CardDetailActivity;
import com.draftcard.dashboard.viewhandler.CardDetailHandler;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.StateInfo;
import com.draftcard.utilz.wheel.SingleWheelPicker;

import org.androidannotations.annotations.App;

import java.util.ArrayList;
import java.util.List;

import static com.draftcard.R.id.inches;

/**
 *
 */

public class CardDetailPresenter {

    BottomSheetDialog mBottomSheetDialog;
    private CardDetailHandler cardDetailHandler;
    private Context context;
    private String mSport;

    public CardDetailPresenter(Context context, CardDetailHandler cardDetailHandler) {
        this.cardDetailHandler = cardDetailHandler;
        this.context = context;
        mBottomSheetDialog = new BottomSheetDialog(context);
    }

    public void openHeightDialog() {

        View sheetView = LayoutInflater.from(context).inflate(R.layout.custom_multiple_wheel_picker, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        final SingleWheelPicker feetPicker = (SingleWheelPicker) sheetView.findViewById(R.id.feet);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 1; i <= 6; i++) {
            list.add(i + " Ft");
        }
        feetPicker.setData(list);
        feetPicker.setSelectedItemPosition(list.size() / 2);

        final SingleWheelPicker inchesPicker = (SingleWheelPicker) sheetView.findViewById(inches);
        ArrayList<String> listInches = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            listInches.add(i + " Inches");
        }
        inchesPicker.setData(listInches);
        inchesPicker.setSelectedItemPosition(listInches.size() / 2);

        TextView txtOk = (TextView) sheetView.findViewById(R.id.txtOk);

        TextView txtCancel = (TextView) sheetView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ft = feetPicker.getValue().split("Ft")[0].trim();
                String inches = inchesPicker.getValue().split("Inches")[0].trim();
                /**/
                cardDetailHandler.onHeightSelect(Integer.parseInt(ft) * 12 + Integer.parseInt(inches), ft + "'" + inches + "\"");
                mBottomSheetDialog.cancel();
            }
        });
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    public void openSportDialog() {
        View sheetView = LayoutInflater.from(context).inflate(R.layout.custom_single_wheel_picker, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        final SingleWheelPicker singleWheelPicker = (SingleWheelPicker) sheetView.findViewById(R.id.picker);
        ArrayList<String> list = new ArrayList<>();

        SportsModel sportsModel  = AppUtilz.getSportsData();

        if(sportsModel != null) {
            List<Sport> sportList = sportsModel.getData().getSports();
            for(Sport sport: sportList) {
                list.add(sport.getName());
            }
        }


        singleWheelPicker.setData(list);
        singleWheelPicker.setSelectedItemPosition(list.size() / 2);

        TextView txtOk = (TextView) sheetView.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) sheetView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SportsModel sportsModel  = AppUtilz.getSportsData();

                if(sportsModel != null) {
                    List<Sport> sportList = sportsModel.getData().getSports();
                    for(Sport sport: sportList) {
                        if(sport.getName().equals(singleWheelPicker.getValue())) {
                            cardDetailHandler.onSportSelect(singleWheelPicker.getValue(), String.valueOf(sport.getId()));
                            break;
                        }
                    }
                }

                mBottomSheetDialog.cancel();
            }
        });
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    public void openAgeDialog() {
        View sheetView = LayoutInflater.from(context).inflate(R.layout.custom_single_wheel_picker, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        final SingleWheelPicker singleWheelPicker = (SingleWheelPicker) sheetView.findViewById(R.id.picker);
        ArrayList<String> list = new ArrayList<>();
        for (int i = AppUtilz.MIN_AGE_LIMIT; i <= AppUtilz.MAX_AGE_LIMIT; i++) {
            list.add("" + i);
        }

        singleWheelPicker.setData(list);
        //singleWheelPicker.setSelectedItemPosition(list.size() / 2);

        TextView txtOk = (TextView) sheetView.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) sheetView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardDetailHandler.onAgeSelect(singleWheelPicker.getValue());
                mBottomSheetDialog.cancel();
            }
        });
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    public void openPositionDialog() {
        View sheetView = LayoutInflater.from(context).inflate(R.layout.custom_single_wheel_picker, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        final SingleWheelPicker singleWheelPicker = (SingleWheelPicker) sheetView.findViewById(R.id.picker);
        ArrayList<String> list = new ArrayList<>();

        if(!TextUtils.isEmpty(mSport)) {
            SportsModel sportsModel  = AppUtilz.getSportsData();

            if(sportsModel != null) {
                List<Sport> sportList = sportsModel.getData().getSports();
                for(Sport sport: sportList) {
                    if(sport.getName().trim().equals(mSport.trim())) {
                        for(Position position: sport.getPositions()) {
                            list.add(position.getName());
                        }
                    }
                }
            }

        }

        singleWheelPicker.setData(list);
        //singleWheelPicker.setSelectedItemPosition(list.size() / 2);

        TextView txtOk = (TextView) sheetView.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) sheetView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardDetailHandler.onPositionSelect(singleWheelPicker.getValue());
                mBottomSheetDialog.cancel();
            }
        });
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    public void setSport(String sport) {
        mSport = sport;
    }

    public void openGradeDialog() {
        View sheetView = LayoutInflater.from(context).inflate(R.layout.custom_single_wheel_picker, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        final SingleWheelPicker singleWheelPicker = (SingleWheelPicker) sheetView.findViewById(R.id.picker);
        ArrayList<String> list = new ArrayList<>();
        for (int i = AppUtilz.MIN_GRADE_LIMIT; i <= AppUtilz.MAX_GRADE_LIMIT; i++) {
            list.add("" + i);
        }

        singleWheelPicker.setData(list);
        //singleWheelPicker.setSelectedItemPosition(list.size() / 2);

        TextView txtOk = (TextView) sheetView.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) sheetView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardDetailHandler.onGradeSelect(singleWheelPicker.getValue());
                mBottomSheetDialog.cancel();
            }
        });
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    public void openLevelDialog() {

        View sheetView = LayoutInflater.from(context).inflate(R.layout.custom_single_wheel_picker, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        final SingleWheelPicker singleWheelPicker = (SingleWheelPicker) sheetView.findViewById(R.id.picker);
        ArrayList<String> list = new ArrayList<>();
        list.add("Freshman");
        list.add("Sophomore");
        list.add("Junior Varsity");
        list.add("Varsity");


        singleWheelPicker.setData(list);
        //singleWheelPicker.setSelectedItemPosition(list.size() / 2);

        TextView txtOk = (TextView) sheetView.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) sheetView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardDetailHandler.OnLevelSelected(singleWheelPicker.getValue());
                mBottomSheetDialog.cancel();
            }
        });
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();

    }



}
