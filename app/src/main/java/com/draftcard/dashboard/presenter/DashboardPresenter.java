package com.draftcard.dashboard.presenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.draftcard.R;
import com.draftcard.dashboard.viewhandler.OnDashboardChangeListener;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by SYS087 on 16-Mar-17.
 */

public class DashboardPresenter {

    private OnDashboardChangeListener mOnDashboardChangeListener;
    private Context context;
    private static final String TAG = "DashboardPresenter";

    public DashboardPresenter(Context context, OnDashboardChangeListener mOnDashboardChangeListener) {
        this.mOnDashboardChangeListener = mOnDashboardChangeListener;
        this.context = context;
    }

    public void shareImage(RelativeLayout rel_front) {
        Bitmap returnedBitmap = Bitmap.createBitmap(rel_front.getWidth(), rel_front.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable =rel_front.getBackground();
        if (bgDrawable!=null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.TRANSPARENT);
        rel_front.draw(canvas);
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("image/*");
        i.putExtra(Intent.EXTRA_STREAM, getImageUri(returnedBitmap));
        try {
            context.startActivity(Intent.createChooser(i, "Share With"));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    private Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void shareLink(String link) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Link");
            i.putExtra(Intent.EXTRA_TEXT, link);
            context.startActivity(Intent.createChooser(i, "Choose one"));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCard(Long id, View view) {
        if (AppUtilz.isNetworkAvailable(context)) {

            AppUtilz.showProgressDialog(context, "");

            HashMap<String, String> getCardMap = new HashMap<>();
            getCardMap.put(AppUtilz.USER_ID, SharedPreferenceUtil.getString(AppUtilz.ID, ""));
            getCardMap.put(AppUtilz.CARD_ID, ""+id);
            Log.e(TAG, "deleteCard: "+getCardMap.toString());
            final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().deleteCard(SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""), getCardMap);

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            mOnDashboardChangeListener.onCardDeleteListener(false, "Error!!!");
                        }

                        @Override
                        public void onNext(Response<ResponseBody> loginResponse) {
                            try {
                                byte[] byteArray = loginResponse.body().bytes();
                                String responseInString = new String(byteArray);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(responseInString);
                                    if (jsonObject.getBoolean("success")) {
                                        /*convert data object into common response class*/
                                        try {
                                            mOnDashboardChangeListener.onCardDeleteListener(true, jsonObject.getString("message"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            mOnDashboardChangeListener.onCardDeleteListener(false, "Error!!!");
                                        }


                                    } else {
                                        mOnDashboardChangeListener.onCardDeleteListener(false, ""+jsonObject.getString("message"));
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } else {
            Snackbar.make(view, R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }
}
