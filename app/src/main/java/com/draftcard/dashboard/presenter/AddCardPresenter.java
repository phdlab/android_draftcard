package com.draftcard.dashboard.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.draftcard.R;
import com.draftcard.dashboard.model.CardModel;
import com.draftcard.dashboard.viewhandler.CardDetailHandler;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.google.gson.Gson;

import org.androidannotations.annotations.App;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by SYS087 on 15-Mar-17.
 */

public class AddCardPresenter {

    private Context context;
    private CardDetailHandler cardDetailHandler;
    private View view;
    private static final String TAG = AddCardPresenter.class.getSimpleName();

    public AddCardPresenter(Context context, CardDetailHandler cardDetailHandler, View _view) {
        this.cardDetailHandler = cardDetailHandler;
        this.context = context;
        this.view = _view;
    }

    public void addCard(CardModel cardModel) {
        if(cardModel == null) {
            return;
        }

        String alertMsg = "";

        /*Validations for Club card*/
        if(cardModel.getCardType().equals("2")) {
            Log.e(TAG, "Club position: " +  cardModel.getPosition());
            if (TextUtils.isEmpty(cardModel.getLocation())) {
                alertMsg = context.getString(R.string.msg_validation_enter_club_location);
            } else if (TextUtils.isEmpty(cardModel.getName())) {
                alertMsg = context.getString(R.string.msg_validation_enter_club_name);
            } else if (TextUtils.isEmpty(cardModel.getTeam())) {
                alertMsg = context.getString(R.string.msg_validation_enter_team_name);
            } else if (TextUtils.isEmpty(cardModel.getSportId())) {
                alertMsg = context.getString(R.string.msg_validation_select_sport);
            } else if (TextUtils.isEmpty(cardModel.getPosition())) {
                alertMsg = context.getString(R.string.msg_validation_select_position);
            } else if (TextUtils.isEmpty(cardModel.getHeight())) {
                alertMsg = context.getString(R.string.msg_validation_enter_height);
            } else if (TextUtils.isEmpty(cardModel.getWeight())) {
                alertMsg = context.getString(R.string.msg_validation_enter_weight);
            } else if (TextUtils.isEmpty(cardModel.getGPA())) {
                alertMsg = context.getString(R.string.msg_validation_enter_gpa);
            }
        }
        /*Validations for Sport card*/
        else if (cardModel.getCardType().equals("1")) {
            Log.e(TAG, "School position: " +  cardModel.getPosition());
            if (TextUtils.isEmpty(cardModel.getName())) {
                alertMsg = context.getString(R.string.msg_validation_enter_school);
            } else if (TextUtils.isEmpty(cardModel.getTeam())) {
                alertMsg = context.getString(R.string.msg_validation_enter_team_name);
            } else if (TextUtils.isEmpty(cardModel.getSportId())) {
                alertMsg = context.getString(R.string.msg_validation_select_sport);
            } else if (TextUtils.isEmpty(cardModel.getPosition())) {
                alertMsg = context.getString(R.string.msg_validation_select_position);
            } else if (TextUtils.isEmpty(cardModel.getHeight())) {
                alertMsg = context.getString(R.string.msg_validation_enter_height);
            } else if (TextUtils.isEmpty(cardModel.getWeight())) {
                alertMsg = context.getString(R.string.msg_validation_enter_weight);
            } else if (TextUtils.isEmpty(cardModel.getGPA())) {
                alertMsg = context.getString(R.string.msg_validation_enter_gpa);
            }
        }


        if(!TextUtils.isEmpty(alertMsg)) {
            showAlert(context.getString(R.string.alert), alertMsg);
            return;
        }

        /*call webservice to do login*/
        if (AppUtilz.isNetworkAvailable(context)) {
            AppUtilz.showProgressDialog(context, "");


            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), SharedPreferenceUtil.getString(AppUtilz.ID, ""));
            android.util.Log.e(TAG, "userid: " + SharedPreferenceUtil.getString(AppUtilz.ID, ""));
            RequestBody cardType = RequestBody.create(MediaType.parse("text/plain"), cardModel.getCardType());
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), cardModel.getName());
            RequestBody location = RequestBody.create(MediaType.parse("text/plain"), cardModel.getLocation());
            RequestBody team = RequestBody.create(MediaType.parse("text/plain"), cardModel.getTeam());
            RequestBody sportId = RequestBody.create(MediaType.parse("text/plain"), cardModel.getSportId());
            RequestBody grade = RequestBody.create(MediaType.parse("text/plain"), cardModel.getGrade());
            RequestBody level = RequestBody.create(MediaType.parse("text/plain"), cardModel.getLevel());
            RequestBody position = RequestBody.create(MediaType.parse("text/plain"), cardModel.getPosition());
            RequestBody number = RequestBody.create(MediaType.parse("text/plain"), cardModel.getNumber());
            RequestBody age = RequestBody.create(MediaType.parse("text/plain"), cardModel.getAge());

            RequestBody height = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(cardModel.getHeightInInches()));
            RequestBody weight = RequestBody.create(MediaType.parse("text/plain"), cardModel.getWeight());
            RequestBody gpa = RequestBody.create(MediaType.parse("text/plain"), cardModel.getGPA());



            MultipartBody.Part cardBg;
            MultipartBody.Part videoThumb;

            if (!TextUtils.isEmpty(cardModel.getCardBg())) {
                File profilePicFile = new File(cardModel.getCardBg());
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), profilePicFile);
                cardBg = MultipartBody.Part.createFormData(AppUtilz.CARD_BG, profilePicFile.getName(), requestFile);
            } else {
                RequestBody requestFile = RequestBody.create(MediaType.parse("text/plain"), "");
                cardBg = MultipartBody.Part.createFormData(AppUtilz.CARD_BG, "", requestFile);
            }

            if (!TextUtils.isEmpty(cardModel.getVideoThumb())) {
                File profilePicFile = new File(cardModel.getVideoThumb());
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), profilePicFile);
                videoThumb = MultipartBody.Part.createFormData(AppUtilz.VIDEO_THUMB, profilePicFile.getName(), requestFile);
            } else {
                RequestBody requestFile = RequestBody.create(MediaType.parse("text/plain"), "");
                videoThumb = MultipartBody.Part.createFormData(AppUtilz.VIDEO_THUMB, "", requestFile);
            }

            SharedPreferenceUtil.putValue(AppUtilz.TEMP_CARD_BG_PATH, "");
            SharedPreferenceUtil.putValue(AppUtilz.VIDEO_THUMB, "");
            SharedPreferenceUtil.save();

            final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().addCard(SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""), userId, cardType,
                    name, location, team, sportId, grade, level, position, number, age, height, weight, gpa, cardBg, videoThumb
                    );

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            AppUtilz.stopProgressDialog();
                            Log.e(TAG, "onError: " + e.toString());
                            cardDetailHandler.onCardAdded(false, "Error");

                        }

                        @Override
                        public void onNext(Response<ResponseBody> loginResponse) {


                            try {
                                byte[] byteArray = loginResponse.body().bytes();
                                String responseInString = new String(byteArray);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(responseInString);
                                    if (jsonObject.getBoolean("success")) {
                                        /*convert data object into commonresponse class*/
                                        Gson gson = new Gson();
                                        cardDetailHandler.onCardAdded(true, responseInString);
                                    } else {
                                        cardDetailHandler.onCardAdded(false, jsonObject.getString("message"));
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });

        } else {
            Snackbar.make(view, R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }

    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }
}
