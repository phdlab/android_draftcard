package com.draftcard.dashboard.presenter;

import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.dashboard.viewhandler.AddSchoolHandler;
import com.draftcard.dashboard.viewhandler.CardDetailHandler;
import com.draftcard.utilz.StateInfo;
import com.draftcard.utilz.wheel.SingleWheelPicker;

import java.util.ArrayList;

/**
 * Created by SYS087 on 16-Mar-17.
 */

public class SchoolPresenter {

    BottomSheetDialog mBottomSheetDialog;
    private AddSchoolHandler cardDetailHandler;
    private Context context;
    private String mSport;

    public SchoolPresenter(Context context, AddSchoolHandler cardDetailHandler) {
        this.cardDetailHandler = cardDetailHandler;
        this.context = context;
        mBottomSheetDialog = new BottomSheetDialog(context);
    }

    public void openStateView() {

        View sheetView = LayoutInflater.from(context).inflate(R.layout.custom_single_wheel_picker, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        final SingleWheelPicker singleWheelPicker = (SingleWheelPicker) sheetView.findViewById(R.id.picker);
        ArrayList<String> list = new ArrayList<>();

        for(String state: StateInfo.getStateNameLists()) {
            list.add(state);
        }

        singleWheelPicker.setData(list);
        //singleWheelPicker.setSelectedItemPosition(list.size() / 2);

        TextView txtOk = (TextView) sheetView.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) sheetView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardDetailHandler.onStateSelected(singleWheelPicker.getValue());
                mBottomSheetDialog.cancel();
            }
        });
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();

    }
}
