package com.draftcard.dashboard.viewhandler;

/**
 * Created by SYS087 on 17-Mar-17.
 */

public interface ICardDetailFetch {
    void onCardDetailFetched(boolean b, String s);
}
