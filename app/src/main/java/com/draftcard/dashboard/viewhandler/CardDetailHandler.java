package com.draftcard.dashboard.viewhandler;

import android.view.View;

/**
 *
 */

public interface CardDetailHandler {

    public void onGradeClick(View v);

    public void OnLevelClick(View v);
    public void OnLevelSelected(String value);

    public void onSportClick(View v);

    public void onHeightClick(View v);

    public void onPositionClick(View v);

    public void onAgeClick(View v);

    public void onHeightSelect(int inches, String height);

    public void onSportSelect(String value, String sportId);

    public void onAgeSelect(String value);

    public void onGradeSelect(String value);

    public void onPositionSelect(String value);

    void onCardAdded(boolean b, String responseInString);
}
