package com.draftcard.dashboard.viewhandler;

import android.net.Uri;

/**
 * Created by AND001 on 3/3/2017.
 */

public interface AddCardPhotoHandler {

    void onGalleryUrl(Uri uri, String selectedImagePath);
}
