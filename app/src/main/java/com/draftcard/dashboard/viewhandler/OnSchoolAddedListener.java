package com.draftcard.dashboard.viewhandler;

/**
 * Created by SYS087 on 16-Mar-17.
 */

public interface OnSchoolAddedListener {

    void onSchoolAddedListener(String schoolName, String schoolCity, String state);
}
