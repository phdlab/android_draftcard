package com.draftcard.dashboard.viewhandler;

import com.draftcard.dashboard.model.school.School;
import com.draftcard.dashboard.model.school.SchoolModel;

/**
 * Created by AND001 on 3/7/2017.
 */

public interface OnSchoolSelectListener {

    void onSchoolSelect();
    void launchAddSchool();
    void onSchoolSelect(School school);
}
