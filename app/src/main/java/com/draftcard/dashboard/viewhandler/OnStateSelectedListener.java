package com.draftcard.dashboard.viewhandler;

/**
 * Created by SYS087 on 16-Mar-17.
 */

public interface OnStateSelectedListener {
    void onStateSelected(String stateName, String stateKey);
    void onStateSelected(int position);
}
