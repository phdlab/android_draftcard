package com.draftcard.dashboard.view;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.draftcard.R;
import com.draftcard.dashboard.model.school.SchoolModel;
import com.draftcard.dashboard.viewhandler.OnSchoolSelectListener;
import com.draftcard.dashboard.viewhandler.OnStateSelectedListener;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.draftcard.utilz.StateInfo;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;


/**
 * Created by SYS087 on 16-Mar-17.
 */
@EFragment(R.layout.dialog_fragment_state_list)
public class StateDialogFragment extends DialogFragment implements OnStateSelectedListener{

    @ViewById
    RecyclerView rvStateView;

    private OnStateSelectedListener mOnStateSelectedListener;
    private static final String TAG = StateDialogFragment.class.getSimpleName();

    private StateAdapter mStateAdapter;

    @AfterViews
    public void init() {
        initDialog();
    }

    private void initDialog() {
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.ThemeDialogZoomAnimation;
        WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        wmlp.gravity = Gravity.FILL_HORIZONTAL;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRecyclerAdapter();
    }

    public void setListener(OnStateSelectedListener _mOnStateSelectedListener) {
        this.mOnStateSelectedListener = _mOnStateSelectedListener;
    }


    @Click
    public void img_back() {
        dismiss();
    }


    private void setRecyclerAdapter() {
        mStateAdapter = new StateAdapter(getContext(), StateInfo.getStateKeyNameLists());
        mStateAdapter.setOnStateSelectedListener(this);
        rvStateView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvStateView.setLayoutManager(mLayoutManager);
        rvStateView.setItemAnimator(new DefaultItemAnimator());
        rvStateView.setAdapter(mStateAdapter);
    }

    @Override
    public void onStateSelected(String stateName, String stateKey) {

    }

    @Override
    public void onStateSelected(int position) {
        mOnStateSelectedListener.onStateSelected(StateInfo.getStateName(position), StateInfo.getStateKey(position));
    }
}
