package com.draftcard.dashboard.view;

import android.graphics.Color;
import android.os.Build;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.draftcard.R;
import com.draftcard.base.BaseActivity;
import com.draftcard.dashboard.model.CardModel;
import com.draftcard.dashboard.model.school.School;
import com.draftcard.dashboard.presenter.AddCardPresenter;
import com.draftcard.dashboard.presenter.CardDetailPresenter;
import com.draftcard.dashboard.viewhandler.CardDetailHandler;
import com.draftcard.dashboard.viewhandler.OnSchoolAddedListener;
import com.draftcard.dashboard.viewhandler.OnSchoolSelectListener;
import com.draftcard.dashboard.viewhandler.OnStateSelectedListener;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.Constants;
import com.draftcard.utilz.InputFilterMax;
import com.draftcard.utilz.SharedPreferenceUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by AND001 on 3/3/2017.
 */

@EActivity(R.layout.activity_card_detail)
public class CardDetailActivity extends BaseActivity implements OnSchoolSelectListener, CardDetailHandler, OnSchoolAddedListener, OnStateSelectedListener {

    @ViewById
    RelativeLayout rel_header;
    @ViewById
    TextView txt_save;
    @ViewById
    TextView txt_height;
    @ViewById
    TextView txt_sport;
    @ViewById
    TextView txt_position;
    @ViewById
    TextView tv_school_name;
    @ViewById
    TextView tvAge;
    @ViewById
    TextView tvGrade;
    @ViewById
    LinearLayout lin_club_location;
    @ViewById
    LinearLayout lin_club_name;
    @ViewById
    LinearLayout lin_school_name;
    @ViewById
    LinearLayout ddAge;
    @ViewById
    LinearLayout ddGrade;
    CardDetailPresenter cardDetailPresenter;
    AddCardPresenter mAddCardPresenter;

    private boolean mIsFromSchool;

    @ViewById
    EditText edt_weight;


    @ViewById
    EditText edt_gpa;

    @ViewById
    EditText edt_club_location;
    @ViewById
    EditText edt_club_name;
    @ViewById
    EditText edt_team_name;

    @ViewById
    EditText edtNumber;

    @ViewById
    LinearLayout rootView;

    @ViewById
    LinearLayout ddLevel;

    @ViewById
    TextView tvLevel;


    private String sportId = "";
    private String schoolAddress, state, schoolName;
    StateDialogFragment stateDialogFragment;
    int heightInInches;

    @Override
    public void onSchoolAddedListener(String schoolName, String schoolCity, String state) {
        schoolName = schoolName + "\n" + state;
        tv_school_name.setText(schoolName);
    }

    @Override
    public void onStateSelected(String _stateName, String stateKey) {
        if(stateDialogFragment != null) {
            stateDialogFragment.dismiss();
        }
        state = stateKey;
        SchoolListDialogFragment schoolListDialogFragment = new SchoolListDialogFragment_();
        SchoolListDialogFragment.setState(state);
        schoolListDialogFragment.setListener(this);
        schoolListDialogFragment.show(getSupportFragmentManager(), "");
    }

    @Override
    public void onStateSelected(int position) {

    }

    @AfterViews
    public void init() {

        mIsFromSchool = getIntent().getExtras().getBoolean(Constants.EXTRA_IS_FROM_SCHOOL);
        transparentStatusBar();
        if (Build.VERSION.SDK_INT >= 21) {
            rel_header.setPadding(0, getStatusBarHeight(), 0, 0);
        }
        setupSlideWindowAnimations(Gravity.BOTTOM, Gravity.BOTTOM);

        if (!mIsFromSchool) {
            //This for club card
            lin_school_name.setVisibility(View.GONE);
            lin_club_location.setVisibility(View.VISIBLE);
            lin_club_name.setVisibility(View.VISIBLE);
            ddAge.setVisibility(View.VISIBLE);
            ddGrade.setVisibility(View.GONE);
        } else {
            //This for School card
        }

        edt_weight.setFilters(new InputFilter[]{new InputFilterMax(AppUtilz.MAX_WEIGHT, InputFilterMax.FilterType.INTEGER)});
        edt_gpa.setFilters(new InputFilter[]{new InputFilterMax(AppUtilz.MAX_GPA, InputFilterMax.FilterType.FLOAT)});

        cardDetailPresenter = new CardDetailPresenter(this, this);
        mAddCardPresenter = new AddCardPresenter(this, this, rootView);
    }

    @Click
    public void img_back() {
        onBackPressed();
    }

    @Click
    public void lin_school_name() {
        /*Launch state view */
        stateDialogFragment = new StateDialogFragment_();
        stateDialogFragment.setListener(this);
        stateDialogFragment.show(getSupportFragmentManager(), "");
    }


    @Click
    public void txt_save() {
        CardModel cardModel = new CardModel();


        if( !TextUtils.isEmpty(edt_gpa.getText().toString()) && '.' == edt_gpa.getText().toString().charAt(edt_gpa.getText().toString().length() - 1)) {
            edt_gpa.setText(edt_gpa.getText().toString().substring(0, edt_gpa.getText().toString().length() - 1));
        }

        if (mIsFromSchool) {
            cardModel.setCardType("1");
            cardModel.setName(tv_school_name.getText().toString());
            /*Todo? Need to pass school address*/
            if(!TextUtils.isEmpty(schoolAddress)) {
                cardModel.setLocation(schoolAddress);
            } else {
                cardModel.setLocation(state);
            }
            cardModel.setTeam(edt_team_name.getText().toString());
            cardModel.setSportId(sportId);

            if(tvGrade.getText().toString().equals(getString(R.string.card_detail_grade))) {
                cardModel.setGrade("");
            } else {
                cardModel.setGrade(tvGrade.getText().toString());
            }

            cardModel.setHeight(txt_height.getText().toString());
            cardModel.setHeightInInches(heightInInches);
            cardModel.setWeight(edt_weight.getText().toString());
            cardModel.setGPA(edt_gpa.getText().toString());

            if(tvLevel.getText().toString().equals(getString(R.string.level))) {
                cardModel.setLevel("");
            } else {
                cardModel.setLevel(tvLevel.getText().toString());
            }

            if(txt_position.getText().toString().equals(getString(R.string.card_detail_position))) {
                cardModel.setPosition("");
            } else {
                cardModel.setPosition(txt_position.getText().toString());
            }

            cardModel.setNumber(edtNumber.getText().toString());
            cardModel.setCardBg(SharedPreferenceUtil.getString(AppUtilz.TEMP_CARD_BG_PATH, ""));
        } else {
            cardModel.setCardType("2");
            cardModel.setLocation(edt_club_location.getText().toString());
            cardModel.setName(edt_club_name.getText().toString());
            cardModel.setTeam(edt_team_name.getText().toString());
            cardModel.setSportId(sportId);

            if(tvAge.getText().toString().equals(getString(R.string.age))) {
                cardModel.setAge("");
            } else {
                cardModel.setAge(tvAge.getText().toString());
            }


            cardModel.setHeight(txt_height.getText().toString());
            cardModel.setHeightInInches(heightInInches);
            cardModel.setWeight(edt_weight.getText().toString());
            cardModel.setGPA(edt_gpa.getText().toString());

            if(txt_position.getText().toString().equals(getString(R.string.card_detail_position))) {
                cardModel.setPosition("");
            } else {
                cardModel.setPosition(txt_position.getText().toString());
            }

            cardModel.setNumber(edtNumber.getText().toString());
            cardModel.setCardBg(SharedPreferenceUtil.getString(AppUtilz.TEMP_CARD_BG_PATH, ""));
        }

        mAddCardPresenter.addCard(cardModel);
    }

    @Override
    public void onSchoolSelect() {

    }

    @Override
    public void launchAddSchool() {
        AddSchoolFragment schoolListDialogFragment = new AddSchoolFragment_();
        schoolListDialogFragment.setListener(this);
        schoolListDialogFragment.show(getSupportFragmentManager(), "");
    }

    @Override
    public void onSchoolSelect(School school) {
        tv_school_name.setText(school.getName());
        state = school.getState();
        schoolAddress = school.getAddress();
    }

    @Override
    public void onGradeClick(View v) {
        cardDetailPresenter.openGradeDialog();
    }

    @Override
    public void OnLevelClick(View v) {
        cardDetailPresenter.openLevelDialog();
    }

    @Override
    public void OnLevelSelected(String value) {
        tvLevel.setTextColor(Color.BLACK);
        tvLevel.setText(value);
    }

    @Override
    public void onSportClick(View v) {
        cardDetailPresenter.openSportDialog();
    }

    @Override
    public void onHeightClick(View v) {
        cardDetailPresenter.openHeightDialog();
    }

    @Override
    public void onPositionClick(View v) {
        cardDetailPresenter.setSport(txt_sport.getText().toString());
        cardDetailPresenter.openPositionDialog();
    }

    @Override
    public void onAgeClick(View v) {
        cardDetailPresenter.openAgeDialog();
    }

    @Override
    public void onHeightSelect(int inches, String height) {
        heightInInches = inches;
        txt_height.setText(height);
    }

    @Override
    public void onSportSelect(String value, String sportId) {
        this.sportId = sportId;
        txt_sport.setTextColor(Color.BLACK);
        txt_position.setText(getString(R.string.card_detail_position));
        txt_sport.setText(value);
    }

    @Override
    public void onAgeSelect(String value) {
        tvAge.setTextColor(Color.BLACK);
        tvAge.setText(value);
    }

    @Override
    public void onGradeSelect(String value) {
        if (mIsFromSchool) {
            android.util.Log.e(CardDetailActivity.class.getSimpleName(), "Val : " + value);
            if (Integer.parseInt(value) >= 10) {
                ddLevel.setVisibility(View.VISIBLE);
            } else {
                ddLevel.setVisibility(View.GONE);
                tvLevel.setText("Level");
            }
        }
        tvGrade.setTextColor(Color.BLACK);
        tvGrade.setText(value);
    }

    @Override
    public void onPositionSelect(String value) {
        txt_position.setTextColor(Color.BLACK);
        txt_position.setText(value);
    }

    @Override
    public void onCardAdded(boolean b, String responseInString) {
        if (b) {
            //Toast.makeText(CardDetailActivity.this, "Card created successfully.", Toast.LENGTH_SHORT).show();
            SharedPreferenceUtil.putValue(AppUtilz.IS_CARD_ADD_SUCCESS, true);
            SharedPreferenceUtil.save();
            finish();
        } else {
            Toast.makeText(CardDetailActivity.this, responseInString, Toast.LENGTH_SHORT).show();
        }
    }




}
