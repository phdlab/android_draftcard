package com.draftcard.dashboard.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.draftcard.R;
import com.draftcard.base.BaseActivity;
import com.draftcard.dashboard.presenter.AddCardPhotoPresenter;
import com.draftcard.dashboard.viewhandler.AddCardPhotoHandler;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.Constants;
import com.draftcard.utilz.FileUtils;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.draftcard.utilz.camera.BitmapHelper;
import com.draftcard.utilz.camera.CameraIntentHelper;
import com.draftcard.utilz.camera.CameraIntentHelperCallback;
import com.draftcard.utilz.customviews.RoundCornerImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.Date;

/**
 * Created by AND001 on 3/3/2017.
 */

@EActivity(R.layout.activity_add_card_photo)
public class AddCardPhotoActivity extends BaseActivity implements AddCardPhotoHandler{

    @ViewById
    RelativeLayout rel_header;
    @ViewById
    LinearLayout lin_bottom_sheet;
    @ViewById
    RoundCornerImageView img_preview;

    private BottomSheetBehavior behavior;
    private CameraIntentHelper mCameraIntentHelper;
    private String mImagePath = "";
    private static final String TAG = "AddCardPhotoActivity";
    private Context mContext;
    private AddCardPhotoPresenter mAddCardPhotoPresenter;

    @AfterViews
    public void init(){
        mContext = this;
        transparentStatusBar();
        if (Build.VERSION.SDK_INT >= 21) {
            rel_header.setPadding(0, getStatusBarHeight(), 0, 0);
        }
        mAddCardPhotoPresenter = new AddCardPhotoPresenter(this);

        initBottomSheet();

        setupExplodeAnimations();

        checkPermission();

        setupCameraIntentHelper();
    }

    private void initBottomSheet() {
        behavior = BottomSheetBehavior.from(lin_bottom_sheet);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
    }

    @Click
    public void img_back(){
        onBackPressed();
    }

    @Click
    public void txt_next(){
        Intent intent = new Intent(this, CardDetailActivity_.class);
        intent.putExtra(Constants.EXTRA_IS_FROM_SCHOOL, getIntent().getExtras().getBoolean(Constants.EXTRA_IS_FROM_SCHOOL));
        moveActivity(intent, this, false);
    }

    @Click
    public void img_preview(){
        openBottomSheet();
    }

    @Click
    public void img_add_photo(){
        openBottomSheet();
    }

    @Click
    public void rl_camera(){
        openCamera();
    }

    @Click
    public void rl_gallery(){
        openGallery();
    }


    public void checkPermission() {
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext) &&
                checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, mContext) && checkPermission(Manifest.permission.CAMERA, mContext)) {
        } else {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constants.REQUEST_PERMISSION_WRITE_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == Constants.REQUEST_PERMISSION_WRITE_STORAGE){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }


    private void openBottomSheet() {
        if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setPeekHeight(0);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    void openGallery() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setPeekHeight(0);
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Constants.REQUEST_OPEN_GALLERY);
    }

    void openCamera(){
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, mContext) &&
                checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, mContext) && checkPermission(Manifest.permission.CAMERA,mContext)) {

            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            behavior.setPeekHeight(0);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                File photoFile = FileUtils.createImageFile();
                mImagePath = photoFile.getAbsolutePath();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mCameraIntentHelper != null) {
                mCameraIntentHelper.startCameraIntent();
            }
        }else{
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constants.REQUEST_PERMISSION_WRITE_STORAGE);
            //showSnackbar(binding.getRoot(), Constants.ErrorStoragePermission);
        }
    }

    /**
     *
     * @param savedInstanceState Start Camera Intent handler
     */

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        mCameraIntentHelper.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCameraIntentHelper.onRestoreInstanceState(savedInstanceState);
    }

    private void setupCameraIntentHelper() {
        mCameraIntentHelper = new CameraIntentHelper(this, new CameraIntentHelperCallback() {
            @Override
            public void onPhotoUriFound(Date dateCameraIntentStarted, final Uri photoUri, int rotateXDegrees) {
                if (photoUri != null) {
                    mImagePath = FileUtils.getPath(mContext, photoUri);
                    Log.e(TAG, "Camera: "+mImagePath);
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            displayImage(photoUri);
                        }
                    }, 2000);

                }
            }

            @Override
            public void deletePhotoWithUri(Uri photoUri) {
                BitmapHelper.deleteImageWithUriIfExists(photoUri, mContext);
            }

            @Override
            public void onSdCardNotMounted() {
            }

            @Override
            public void onCanceled() {
            }

            @Override
            public void onCouldNotTakePhoto() {
            }

            @Override
            public void onPhotoUriNotFound() {
            }

            @Override
            public void logException(Exception e) {
                Log.d(getClass().getName(), e.getMessage());
            }

            @Override
            public void onActivityResult(Intent intent, int requestCode) {
                startActivityForResult(intent, requestCode);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_OPEN_CAMERA) {
                mCameraIntentHelper.onActivityResult(requestCode, resultCode, data);
            } else if (requestCode == Constants.REQUEST_OPEN_GALLERY) {
                mAddCardPhotoPresenter.getGalleryImageUri(data, mContext);
            }
        }
    }

    /**
     *
     * End  Camera Intent handler
     */

    private void displayImage(Uri photoUri) {
        img_preview.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(photoUri).into(img_preview);
    }

    @Override
    public void onGalleryUrl(Uri uri, String selectedImagePath) {
        mImagePath = selectedImagePath;
        displayImage(uri);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*If card added successfully, finish this activity...*/
        if(SharedPreferenceUtil.getBoolean(AppUtilz.IS_CARD_ADD_SUCCESS, false)) {
            SharedPreferenceUtil.putValue(AppUtilz.RELOAD_CARDS, true);
            SharedPreferenceUtil.putValue(AppUtilz.IS_CARD_ADD_SUCCESS, false);
            SharedPreferenceUtil.save();
            finish();
        }
    }
}
