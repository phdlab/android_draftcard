package com.draftcard.dashboard.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.draftcard.R;
import com.draftcard.dashboard.model.school.School;
import com.draftcard.dashboard.model.school.SchoolModel;
import com.draftcard.dashboard.viewhandler.OnSchoolAddedListener;
import com.draftcard.dashboard.viewhandler.OnSchoolSelectListener;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.rest.response.CommonResponse;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.draftcard.utilz.StateInfo;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.KeyUp;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by AND001 on 3/7/2017.
 */

@EFragment(R.layout.dialog_fragment_school_list)
public class SchoolListDialogFragment extends DialogFragment implements OnSchoolSelectListener {

    @ViewById
    RecyclerView recycler_view;
    @ViewById
    EditText edt_search_school;

    private OnSchoolSelectListener mOnSchoolSelectListner;
    private static final String TAG = "SchoolListDialogFragmen";
    private static String mState = "";

    Subscription responseSubscription;

    @ViewById
    LinearLayout llSchoolNotFound;

    SchoolListAdapter mSchoolListAdapter;

    @AfterViews
    public void init() {
        initDialog();
    }

    private void initDialog() {
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.ThemeDialogZoomAnimation;
        WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        wmlp.gravity = Gravity.FILL_HORIZONTAL;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSchoolList();
    }

    public void setListener(OnSchoolSelectListener mOnSchoolSelectListner) {
        this.mOnSchoolSelectListner = mOnSchoolSelectListner;
    }


    @TextChange
    public void edt_search_school() {
        Log.e(TAG, "Query: " + edt_search_school.getText().toString());
        if(responseSubscription != null) {
            responseSubscription.unsubscribe();
        }
        getSchoolList();

    }


    public static void setState(String state) {
        mState = state;
    }

    @Click
    public void img_back(){
        dismiss();
    }


    private void getSchoolList() {

        /*call webservice to do login*/
        if (AppUtilz.isNetworkAvailable(getActivity())) {


            Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().getSchools("http://app.draftcard.com/great-schools/search", SharedPreferenceUtil.getString(AppUtilz.TOKEN, ""), edt_search_school.getText().toString(), mState);

            responseSubscription = new Subscriber<Response<ResponseBody>>() {
                @Override
                public void onCompleted() {
                    Log.e(TAG, "onCompleted: ");
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(TAG, "onError: " + e.toString());
                }

                @Override
                public void onNext(Response<ResponseBody> loginResponse) {

                    try {
                        byte[] byteArray = loginResponse.body().bytes();
                        String responseInString = new String(byteArray);
                        android.util.Log.e(TAG, "response: " + responseInString);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(responseInString);
                            if (jsonObject.getBoolean("success")) {
                                /*try to convert json to SchoolModel array*/
                                try {
                                    Gson gson = new Gson();
                                    SchoolModel schoolModelResponse = gson.fromJson(jsonObject.toString(), SchoolModel.class);
                                    llSchoolNotFound.setVisibility(View.GONE);
                                    setRecyclerAdapter(schoolModelResponse.getSchools());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    llSchoolNotFound.setVisibility(View.VISIBLE);
                                }

                            } else {
                                        /*Error*/
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            };
            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((Observer<? super Response<ResponseBody>>) responseSubscription);
        } else {

        }
    }

    @Click
    public void llSchoolNotFound() {
        mOnSchoolSelectListner.launchAddSchool();
        dismiss();
    }


    private void setRecyclerAdapter(List<School> schoolList) {
        mSchoolListAdapter = new SchoolListAdapter(getContext(), schoolList);
        mSchoolListAdapter.setOnSchoolSelectedListener(this);
        recycler_view.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(mSchoolListAdapter);
    }

    @Override
    public void onSchoolSelect() {

    }

    @Override
    public void launchAddSchool() {

    }

    @Override
    public void onSchoolSelect(School school) {
        mOnSchoolSelectListner.onSchoolSelect(school);
        dismiss();
    }
}
