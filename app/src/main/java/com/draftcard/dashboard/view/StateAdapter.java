package com.draftcard.dashboard.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.dashboard.viewhandler.OnStateSelectedListener;

import java.util.List;

/**
 * Created by SYS087 on 16-Mar-17.
 */

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.ViewHolder> {

    private List<String> mStateList;
    private Context mContext;
    private Activity activity;

    private OnStateSelectedListener mOnStateSelectedListener;

    public void setOnStateSelectedListener(OnStateSelectedListener onStateSelectedListener) {
        mOnStateSelectedListener = onStateSelectedListener;
    }

    public StateAdapter(Context context, List<String> stateList) {
        this.mStateList = stateList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_state, parent, false);
        StateAdapter.ViewHolder vh = new StateAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.stateName.setText(mStateList.get(position));
        holder.llState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnStateSelectedListener.onStateSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStateList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView stateName;
        LinearLayout llState;

        public ViewHolder(View itemView) {
            super(itemView);
            stateName = (TextView) itemView.findViewById(R.id.stateName);
            llState = (LinearLayout) itemView.findViewById(R.id.llState);
        }
    }
}
