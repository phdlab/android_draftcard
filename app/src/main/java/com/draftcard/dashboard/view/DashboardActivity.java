package com.draftcard.dashboard.view;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.RelativeLayout;

import com.draftcard.R;
import com.draftcard.base.BaseActivity;
import com.draftcard.dashboard.model.CardModel;
import com.draftcard.dashboard.model.getcard.Card;
import com.draftcard.dashboard.model.getcard.GetCardModel;
import com.draftcard.dashboard.presenter.GetCardPresenter;
import com.draftcard.dashboard.viewhandler.ICardDetailFetch;
import com.draftcard.prelogin.view.LoginActivity_;
import com.draftcard.prelogin.view.SplashActivity;
import com.draftcard.rest.NetWorkUtils;
import com.draftcard.setting.SettingsMainActivity;
import com.draftcard.setting.SettingsMainActivity_;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.Constants;
import com.draftcard.utilz.DepthPageTransformer;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.draftcard.utilz.ZoomOutPageTransformer;
import com.draftcard.utilz.customviews.CenterViewPager;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@EActivity(R.layout.activity_dashboard)
public class DashboardActivity extends BaseActivity implements ICardDetailFetch {

    @ViewById
    RelativeLayout rel_header;
    @ViewById
    CenterViewPager view_pager;

    private int mSize = 0;

    private GetCardPresenter mGetCardPresenter;

    private static final String TAG = DashboardActivity.class.getSimpleName();

    @Click
    public void imgBack() {
        SettingsMainActivity_.intent(DashboardActivity.this).start();
        overridePendingTransition(R.anim.back_enter, R.anim.back_exit);
    }

    @AfterViews
    public void init() {
        transparentStatusBar();
        //statusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        if (Build.VERSION.SDK_INT >= 21) {
            rel_header.setPadding(0, getStatusBarHeight(), 0, 0);
        }

        getSportsInformation();

        mGetCardPresenter = new GetCardPresenter(this, this);

        mGetCardPresenter.getCards();

    }

    private void setPagerAdapter(final List<Card> cardList, final String cardBgPath, final String videoThumbPath) {
        final FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mSize + 1;
            }

            @Override
            public android.support.v4.app.Fragment getItem(int position) {
                Fragment fragment;
                if (position == mSize) {
                    fragment = new CreateCardFragment_();
                } else {
                    fragment = new DashboardFragment_();
                    ((DashboardFragment_)fragment).setCardModel(cardList.get(position));
                    ((DashboardFragment_)fragment).setCardBgPath(cardBgPath);
                    ((DashboardFragment_)fragment).setVideoThumbPath(videoThumbPath);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.POSITION, position);
                    fragment.setArguments(bundle);
                }
                return fragment;
            }

            @Override
            public Parcelable saveState() {
                return null;
            }
        };

        view_pager.setAdapter(adapter);
        view_pager.setCurrentItem(0);
        view_pager.setPageTransformer(false, new ZoomOutPageTransformer());
        view_pager.setPageMargin(20);
        view_pager.setOffscreenPageLimit(mSize + 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!SharedPreferenceUtil.getBoolean(AppUtilz.IS_LOGIN, false)) {
            finish();
            LoginActivity_.intent(DashboardActivity.this).start();
        }

        if(SharedPreferenceUtil.getBoolean(AppUtilz.RELOAD_CARDS, false)) {
            SharedPreferenceUtil.putValue(AppUtilz.RELOAD_CARDS, false);
            SharedPreferenceUtil.save();
            mGetCardPresenter.getCards();
        }
    }

    private void getSportsInformation() {
        if (AppUtilz.isNetworkAvailable(DashboardActivity.this)) {

            final Observable<Response<ResponseBody>> responseUserObservable = NetWorkUtils.getApiService().getSports();

            responseUserObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Response<ResponseBody>>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onNext(Response<ResponseBody> loginResponse) {
                            try {
                                byte[] byteArray = new byte[0];
                                byteArray = loginResponse.body().bytes();
                                String responseInString = new String(byteArray);
                                android.util.Log.e(DashboardActivity.class.getSimpleName(), "ResponseInString=> " + responseInString);
                                SharedPreferenceUtil.putValue(AppUtilz.SPORTS_DATA, responseInString);
                                SharedPreferenceUtil.save();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });


        }
    }

    @Override
    public void onCardDetailFetched(boolean success, String s) {
        if(success) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(s);
                Gson gson = new Gson();
                GetCardModel cardModel = gson.fromJson(jsonObject.toString(), GetCardModel.class);
                if(cardModel != null && cardModel.getData() != null) {
                    mSize = cardModel.getData().getCards().size();
                    android.util.Log.e(TAG, "Card size: " + mSize);
                    setPagerAdapter(cardModel.getData().getCards(), cardModel.getData().getCardBgPath(), cardModel.getData().getCardVideoThumbPath());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            android.util.Log.e(TAG, "Failed to fetch card information!!!");
        }
    }


}
