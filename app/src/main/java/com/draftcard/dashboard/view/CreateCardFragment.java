package com.draftcard.dashboard.view;

import android.content.Intent;
import android.widget.LinearLayout;

import com.draftcard.R;
import com.draftcard.base.BaseFragment;
import com.draftcard.utilz.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by AND001 on 3/2/2017.
 */

@EFragment(R.layout.fragment_create_card)
public class CreateCardFragment extends BaseFragment {

    @ViewById
    LinearLayout vg_cover;

    @AfterViews
    public void init(){
        vg_cover.getLayoutParams().width = (int) (getWidth()/1.1);
        vg_cover.requestLayout();
    }

    @Click
    public void txt_school_sport(){
        Intent intent = new Intent(getActivity(), AddCardPhotoActivity_.class);
        intent.putExtra(Constants.EXTRA_IS_FROM_SCHOOL, true);
        moveActivity(intent, getActivity(), false);
        //AddCardPhotoActivity_.intent(getActivity()).start();
    }

    @Click
    public void txt_club_sport(){
        Intent intent = new Intent(getActivity(), AddCardPhotoActivity_.class);
        intent.putExtra(Constants.EXTRA_IS_FROM_SCHOOL, false);
        moveActivity(intent, getActivity(), false);
    }
}
