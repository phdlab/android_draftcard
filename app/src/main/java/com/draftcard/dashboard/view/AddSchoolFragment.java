package com.draftcard.dashboard.view;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.dashboard.presenter.AddCardPhotoPresenter;
import com.draftcard.dashboard.presenter.SchoolPresenter;
import com.draftcard.dashboard.viewhandler.AddSchoolHandler;
import com.draftcard.dashboard.viewhandler.OnSchoolAddedListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by SYS087 on 16-Mar-17.
 */
@EFragment(R.layout.dialog_fragment_add_school)
public class AddSchoolFragment extends DialogFragment implements AddSchoolHandler {

    @ViewById
    TextView tvState;
    @ViewById
    EditText edtSchoolName;
    @ViewById
    EditText edtSchoolCity;
    @ViewById
    TextView txt_save;
    @ViewById
    RelativeLayout rlState;

    private OnSchoolAddedListener mListener;
    SchoolPresenter mSchoolPresenter;

    @AfterViews
    public void init() {
        initDialog();
    }

    private void initDialog() {
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.ThemeDialogZoomAnimation;
        WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        wmlp.gravity = Gravity.FILL_HORIZONTAL;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSchoolPresenter = new SchoolPresenter(getActivity(), this);
    }

    public void setListener(OnSchoolAddedListener listener) {
        mListener = listener;
    }

    @Click
    public void img_back(){
        dismiss();
    }

    @Override
    public void onStateSelected(String state) {
        tvState.setTextColor(Color.BLACK);
        tvState.setText(state);
    }


    @Click
    public void rlState() {
        mSchoolPresenter.openStateView();
    }

    @Click
    public void txt_save() {

        String alertMessage  = "";

        if(TextUtils.isEmpty(edtSchoolName.getText().toString())) {
            alertMessage = getString(R.string.msg_validation_enter_school);
        } else if (TextUtils.isEmpty(edtSchoolCity.getText().toString())) {
            alertMessage = getString(R.string.msg_validation_enter_school_city);
        } else if (tvState.getText().toString().equals(getString(R.string.state))) {
            alertMessage = getString(R.string.msg_validation_enter_state);
        }

        if(!TextUtils.isEmpty(alertMessage)) {
           showAlert(getString(R.string.alert), alertMessage);
            return;
        }

        mListener.onSchoolAddedListener(edtSchoolName.getText().toString(), edtSchoolCity.getText().toString(), tvState.getText().toString());
        dismiss();
    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }
}
