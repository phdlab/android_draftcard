package com.draftcard.dashboard.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.draftcard.R;
import com.draftcard.dashboard.model.school.School;
import com.draftcard.dashboard.viewhandler.OnSchoolSelectListener;
import com.draftcard.dashboard.viewhandler.OnStateSelectedListener;

import java.util.List;

/**
 * Created by SYS087 on 16-Mar-17.
 */

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.ViewHolder> {

    private List<School> mSchoolModelList;
    private Context mContext;
    private Activity activity;

    private OnSchoolSelectListener mOnSchoolSelectedListener;

    public void setOnSchoolSelectedListener(OnSchoolSelectListener onSchoolSelectedListener) {
        this.mOnSchoolSelectedListener = onSchoolSelectedListener;
    }

    public SchoolListAdapter(Context context, List<School> schoolList) {
        this.mSchoolModelList = schoolList;
    }

    @Override
    public SchoolListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_school_name, parent, false);
        SchoolListAdapter.ViewHolder vh = new SchoolListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(SchoolListAdapter.ViewHolder holder, final int position) {

        holder.address.setText(mSchoolModelList.get(position).getAddress());
        holder.state.setText(mSchoolModelList.get(position).getState());
        holder.schoolName.setText(mSchoolModelList.get(position).getName());

        holder.llSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnSchoolSelectedListener.onSchoolSelect(mSchoolModelList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSchoolModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView schoolName;
        TextView state;
        TextView address;

        LinearLayout llSchool;

        public ViewHolder(View itemView) {
            super(itemView);
            schoolName = (TextView) itemView.findViewById(R.id.schoolName);
            state = (TextView) itemView.findViewById(R.id.state);
            address = (TextView) itemView.findViewById(R.id.address);
            llSchool = (LinearLayout) itemView.findViewById(R.id.llSchool);
        }
    }

}
