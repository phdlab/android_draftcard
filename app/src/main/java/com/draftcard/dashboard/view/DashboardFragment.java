package com.draftcard.dashboard.view;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.draftcard.R;
import com.draftcard.base.BaseFragment;
import com.draftcard.dashboard.model.CardModel;
import com.draftcard.dashboard.model.Sport;
import com.draftcard.dashboard.model.SportsModel;
import com.draftcard.dashboard.model.getcard.Card;
import com.draftcard.dashboard.model.getcard.GetCardModel;
import com.draftcard.dashboard.presenter.DashboardPresenter;
import com.draftcard.dashboard.viewhandler.OnDashboardChangeListener;
import com.draftcard.utilz.AppUtilz;
import com.draftcard.utilz.DialogUtils;
import com.draftcard.utilz.FlipAnimListener;
import com.draftcard.utilz.SharedPreferenceUtil;
import com.draftcard.utilz.customviews.RoundCornerImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import static com.draftcard.utilz.AppUtilz.getSportsData;

/**
 * Created by AND001 on 3/2/2017.
 */

@EFragment(R.layout.fragment_dashboard)
public class DashboardFragment extends BaseFragment implements OnDashboardChangeListener{

    @ViewById
    RelativeLayout rel_front;
    @ViewById
    RelativeLayout rel_back;
    @ViewById
    LinearLayout vg_cover;
    @ViewById
    ImageView img_more;

    private ValueAnimator mFlipAnimator;
    private static final String TAG = "DashboardFragment";

    public Card mCardModel;
    public String cardBgPath;
    public String videoThumbPath;
    private DashboardPresenter mDashboardPresenter;

    @ViewById
    TextView txt_team_name;
    @ViewById
    TextView txt_sport;
    @ViewById
    TextView txt_first_name;
    @ViewById
    TextView txt_last_name;
    @ViewById
    TextView txt_club_name;
    @ViewById
    TextView txt_craft;
    @ViewById
    TextView txt_back_craft;
    @ViewById
    TextView txt_position;
    @ViewById
    TextView txt_back_first_name;
    @ViewById
    TextView txt_back_last_name;
    @ViewById
    TextView txt_back_club_name;
    @ViewById
    TextView txt_back_club_location;
    @ViewById
    TextView txt_back_position;
    @ViewById
    TextView txt_height;
    @ViewById
    TextView txt_weight;
    @ViewById
    TextView txt_gpa;
    @ViewById
    RoundCornerImageView img_front;
    @ViewById
    RoundCornerImageView img_back;

    @AfterViews
    public void init() {
        mFlipAnimator = ValueAnimator.ofFloat(0f, 1f);
        mFlipAnimator.addUpdateListener(new FlipAnimListener(rel_front, rel_back));
        vg_cover.getLayoutParams().width = (int) (getWidth()/1.1);
        vg_cover.requestLayout();
        mDashboardPresenter = new DashboardPresenter(getActivity(), this);
        renderCardInformation();
    }

    private void renderCardInformation() {

        if(!TextUtils.isEmpty(mCardModel.getCardBg()) && !TextUtils.isEmpty(cardBgPath)) {
            Glide.with(getActivity()).load(cardBgPath + mCardModel.getCardBg()).into(img_front);
            Glide.with(getActivity()).load(cardBgPath + mCardModel.getCardBg()).into(img_back);
        }

        txt_first_name.setText(SharedPreferenceUtil.getString(AppUtilz.FIRST_NAME, ""));
        txt_back_first_name.setText(SharedPreferenceUtil.getString(AppUtilz.FIRST_NAME, ""));
        txt_last_name.setText(SharedPreferenceUtil.getString(AppUtilz.LAST_NAME, ""));
        txt_back_last_name.setText(SharedPreferenceUtil.getString(AppUtilz.LAST_NAME, ""));

        if(!TextUtils.isEmpty(mCardModel.getTeam())) {
            txt_team_name.setText(mCardModel.getTeam());
        }

        if(!TextUtils.isEmpty(mCardModel.getSportId())) {

            SportsModel sportsModel = getSportsData();

            for(Sport sport: sportsModel.getData().getSports()) {
                if(String.valueOf(sport.getId()).equals(mCardModel.getSportId())) {
                    txt_sport.setText(sport.getName());
                    break;
                }
            }

        }

        if(!TextUtils.isEmpty(mCardModel.getName())) {
            txt_club_name.setText(mCardModel.getName());
        }

        if(!TextUtils.isEmpty(mCardModel.getName())) {
            txt_club_name.setText(mCardModel.getName());
            txt_back_club_name.setText(mCardModel.getName());
        }


        if(!TextUtils.isEmpty(mCardModel.getLocation())) {
            txt_back_club_location.setText(mCardModel.getLocation());
        }

        if(!TextUtils.isEmpty(mCardModel.getPosition())) {
            txt_back_position.setText(mCardModel.getPosition());
            txt_position.setText(mCardModel.getPosition());
        }

        if(!TextUtils.isEmpty(mCardModel.getNumber())) {
            txt_craft.setText(mCardModel.getNumber());
            txt_back_craft.setText(mCardModel.getNumber());
        }

        if(!TextUtils.isEmpty(mCardModel.getWeight())) {
            txt_weight.setText(mCardModel.getWeight());
        }

        if(!TextUtils.isEmpty(mCardModel.getHeight())) {
            try {
                int heightInInches = Integer.parseInt(mCardModel.getHeight());
                int feet = heightInInches/12;
                int inches = heightInInches%12;
                txt_height.setText(String.valueOf(feet) + "'" + String.valueOf(inches) + "\"");
            } catch (Exception e) {
                e.printStackTrace();
                txt_height.setText(mCardModel.getHeight());
            }


        }

        if(!TextUtils.isEmpty(mCardModel.getGPA())) {
            txt_gpa.setText(mCardModel.getGPA());
        }

    }

    @Click
    public void rel_front(){
        toggleFlip();
    }

    @Click
    public void rel_back(){
        toggleFlip();
    }

    @Click
    public void img_more(){
        openViewMore();
    }

    void openViewMore(){
        final Dialog dialog = new DialogUtils(getContext()).setupCustomeDialogFromBottom(R.layout.dialog_view_more);
        TextView mTxtShareImage = (TextView) dialog.findViewById(R.id.txt_share_image);
        TextView mTxtShareLink = (TextView) dialog.findViewById(R.id.txt_share_link);
        TextView mTxtDelete = (TextView) dialog.findViewById(R.id.txt_delete);
        mTxtShareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDashboardPresenter.shareImage(rel_front);
                dialog.dismiss();
            }
        });

        mTxtShareLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDashboardPresenter.shareLink(getString(R.string.share_link_base)+mCardModel.getId());
                dialog.dismiss();
            }
        });

        mTxtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showAlertDialog(new OnDialogClick() {
                    @Override
                    public void onPositiveBtnClick() {
                        mDashboardPresenter.deleteCard(mCardModel.getId(), vg_cover);
                    }

                    @Override
                    public void onNegativeBtnClick() {

                    }
                }, getString(R.string.delete), getString(R.string.sure_want_to_delete), true);
            }
        });
        dialog.show();
    }

    void openMenu(){
        PopupMenu popup = new PopupMenu(getActivity(), img_more);
        popup.getMenuInflater().inflate(R.menu.menu_dashboard, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_share:
                        showToast("Share");
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    private void toggleFlip() {
        if(isFlipped()){
            mFlipAnimator.reverse();
        } else {
            mFlipAnimator.start();
        }
    }

    private boolean isFlipped() {
        return mFlipAnimator.getAnimatedFraction() == 1;
    }

    public void setCardModel(Card cardModel) {
        mCardModel = cardModel;
    }

    public String getCardBgPath() {
        return cardBgPath;
    }

    public void setCardBgPath(String cardBgPath) {
        this.cardBgPath = cardBgPath;
    }

    public String getVideoThumbPath() {
        return videoThumbPath;
    }

    public void setVideoThumbPath(String videoThumbPath) {
        this.videoThumbPath = videoThumbPath;
    }

    @Override
    public void onCardDeleteListener(boolean isSucc, String response) {
        if(isSucc){
            showSnackbar(vg_cover, response);
            ((DashboardActivity)getActivity()).init();
        }else{
            showAlertDialog(new OnDialogClick() {
                @Override
                public void onPositiveBtnClick() {

                }

                @Override
                public void onNegativeBtnClick() {

                }
            }, getString(R.string.alert), response, false);
        }
    }
}
