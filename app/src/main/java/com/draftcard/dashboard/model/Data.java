
package com.draftcard.dashboard.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Data {

    @SerializedName("sports")
    private List<Sport> mSports;

    public List<Sport> getSports() {
        return mSports;
    }

    public void setSports(List<Sport> sports) {
        mSports = sports;
    }

}
