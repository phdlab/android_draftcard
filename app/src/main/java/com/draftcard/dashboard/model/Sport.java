
package com.draftcard.dashboard.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Sport {

    @SerializedName("id")
    private Long mId;
    @SerializedName("is_active")
    private String mIsActive;
    @SerializedName("name")
    private String mName;
    @SerializedName("positions")
    private List<Position> mPositions;
    @SerializedName("stats")
    private List<Stat> mStats;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIsActive() {
        return mIsActive;
    }

    public void setIsActive(String is_active) {
        mIsActive = is_active;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<Position> getPositions() {
        return mPositions;
    }

    public void setPositions(List<Position> positions) {
        mPositions = positions;
    }

    public List<Stat> getStats() {
        return mStats;
    }

    public void setStats(List<Stat> stats) {
        mStats = stats;
    }

}
