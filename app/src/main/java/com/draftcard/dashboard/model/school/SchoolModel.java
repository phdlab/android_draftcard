
package com.draftcard.dashboard.model.school;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class SchoolModel {

    @SerializedName("schools")
    private List<School> mSchools;
    @SerializedName("success")
    private Boolean mSuccess;

    public List<School> getSchools() {
        return mSchools;
    }

    public void setSchools(List<School> schools) {
        mSchools = schools;
    }

    public Boolean getSuccess() {
        return mSuccess;
    }

    public void setSuccess(Boolean success) {
        mSuccess = success;
    }

}
