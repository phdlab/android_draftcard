
package com.draftcard.dashboard.model;

import com.google.gson.annotations.SerializedName;

public class Unit {

    @SerializedName("id")
    private Long mId;
    @SerializedName("is_active")
    private String mIsActive;
    @SerializedName("max")
    private String mMax;
    @SerializedName("min")
    private String mMin;
    @SerializedName("stats_id")
    private String mStatsId;
    @SerializedName("units")
    private String mUnits;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIsActive() {
        return mIsActive;
    }

    public void setIsActive(String is_active) {
        mIsActive = is_active;
    }

    public String getMax() {
        return mMax;
    }

    public void setMax(String max) {
        mMax = max;
    }

    public String getMin() {
        return mMin;
    }

    public void setMin(String min) {
        mMin = min;
    }

    public String getStatsId() {
        return mStatsId;
    }

    public void setStatsId(String stats_id) {
        mStatsId = stats_id;
    }

    public String getUnits() {
        return mUnits;
    }

    public void setUnits(String units) {
        mUnits = units;
    }

}
