
package com.draftcard.dashboard.model.school;

import com.google.gson.annotations.SerializedName;


public class School {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("city")
    private String mCity;
    @SerializedName("district")
    private String mDistrict;
    @SerializedName("districtId")
    private String mDistrictId;
    @SerializedName("districtNCESId")
    private String mDistrictNCESId;
    @SerializedName("enrollment")
    private String mEnrollment;
    @SerializedName("fax")
    private String mFax;
    @SerializedName("gradeRange")
    private String mGradeRange;
    @SerializedName("gsId")
    private String mGsId;
    @SerializedName("gsRating")
    private String mGsRating;
    @SerializedName("lat")
    private String mLat;
    @SerializedName("lon")
    private String mLon;
    @SerializedName("name")
    private String mName;
    @SerializedName("ncesId")
    private String mNcesId;
    @SerializedName("overviewLink")
    private String mOverviewLink;
    @SerializedName("parentRating")
    private String mParentRating;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("ratingsLink")
    private String mRatingsLink;
    @SerializedName("reviewsLink")
    private String mReviewsLink;
    @SerializedName("schoolStatsLink")
    private String mSchoolStatsLink;
    @SerializedName("state")
    private String mState;
    @SerializedName("type")
    private String mType;
    @SerializedName("website")
    private String mWebsite;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getDistrict() {
        return mDistrict;
    }

    public void setDistrict(String district) {
        mDistrict = district;
    }

    public String getDistrictId() {
        return mDistrictId;
    }

    public void setDistrictId(String districtId) {
        mDistrictId = districtId;
    }

    public String getDistrictNCESId() {
        return mDistrictNCESId;
    }

    public void setDistrictNCESId(String districtNCESId) {
        mDistrictNCESId = districtNCESId;
    }

    public String getEnrollment() {
        return mEnrollment;
    }

    public void setEnrollment(String enrollment) {
        mEnrollment = enrollment;
    }

    public String getFax() {
        return mFax;
    }

    public void setFax(String fax) {
        mFax = fax;
    }

    public String getGradeRange() {
        return mGradeRange;
    }

    public void setGradeRange(String gradeRange) {
        mGradeRange = gradeRange;
    }

    public String getGsId() {
        return mGsId;
    }

    public void setGsId(String gsId) {
        mGsId = gsId;
    }

    public String getGsRating() {
        return mGsRating;
    }

    public void setGsRating(String gsRating) {
        mGsRating = gsRating;
    }

    public String getLat() {
        return mLat;
    }

    public void setLat(String lat) {
        mLat = lat;
    }

    public String getLon() {
        return mLon;
    }

    public void setLon(String lon) {
        mLon = lon;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNcesId() {
        return mNcesId;
    }

    public void setNcesId(String ncesId) {
        mNcesId = ncesId;
    }

    public String getOverviewLink() {
        return mOverviewLink;
    }

    public void setOverviewLink(String overviewLink) {
        mOverviewLink = overviewLink;
    }

    public String getParentRating() {
        return mParentRating;
    }

    public void setParentRating(String parentRating) {
        mParentRating = parentRating;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getRatingsLink() {
        return mRatingsLink;
    }

    public void setRatingsLink(String ratingsLink) {
        mRatingsLink = ratingsLink;
    }

    public String getReviewsLink() {
        return mReviewsLink;
    }

    public void setReviewsLink(String reviewsLink) {
        mReviewsLink = reviewsLink;
    }

    public String getSchoolStatsLink() {
        return mSchoolStatsLink;
    }

    public void setSchoolStatsLink(String schoolStatsLink) {
        mSchoolStatsLink = schoolStatsLink;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public void setWebsite(String website) {
        mWebsite = website;
    }

}
