
package com.draftcard.dashboard.model;


import com.google.gson.annotations.SerializedName;

public class Position {

    @SerializedName("id")
    private Long mId;
    @SerializedName("is_active")
    private String mIsActive;
    @SerializedName("name")
    private String mName;
    @SerializedName("order_by")
    private String mOrderBy;
    @SerializedName("sport_id")
    private String mSportId;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIsActive() {
        return mIsActive;
    }

    public void setIsActive(String is_active) {
        mIsActive = is_active;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOrderBy() {
        return mOrderBy;
    }

    public void setOrderBy(String order_by) {
        mOrderBy = order_by;
    }

    public String getSportId() {
        return mSportId;
    }

    public void setSportId(String sport_id) {
        mSportId = sport_id;
    }

}
