
package com.draftcard.dashboard.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class Stat {

    @SerializedName("id")
    private Long mId;
    @SerializedName("is_active")
    private String mIsActive;
    @SerializedName("sport_id")
    private String mSportId;
    @SerializedName("stats")
    private String mStats;
    @SerializedName("units")
    private List<Unit> mUnits;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIsActive() {
        return mIsActive;
    }

    public void setIsActive(String is_active) {
        mIsActive = is_active;
    }

    public String getSportId() {
        return mSportId;
    }

    public void setSportId(String sport_id) {
        mSportId = sport_id;
    }

    public String getStats() {
        return mStats;
    }

    public void setStats(String stats) {
        mStats = stats;
    }

    public List<Unit> getUnits() {
        return mUnits;
    }

    public void setUnits(List<Unit> units) {
        mUnits = units;
    }

}
