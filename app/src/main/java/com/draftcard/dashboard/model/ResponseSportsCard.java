package com.draftcard.dashboard.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS087 on 15-Mar-17.
 */

public class ResponseSportsCard {


    @SerializedName("data")
    private Data mData;
    @SerializedName("errors")
    private Errors mErrors;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("success")
    private Boolean mSuccess;


    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public Errors getErrors() {
        return mErrors;
    }

    public void setErrors(Errors errors) {
        mErrors = errors;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getSuccess() {
        return mSuccess;
    }

    public void setSuccess(Boolean success) {
        mSuccess = success;
    }

    public class Data {


        @SerializedName("age")
        private String mAge;
        @SerializedName("card_bg")
        private String mCardBg;
        @SerializedName("card_type")
        private String mCardType;
        @SerializedName("grade")
        private String mGrade;
        @SerializedName("id")
        private Long mId;
        @SerializedName("level")
        private String mLevel;
        @SerializedName("location")
        private String mLocation;
        @SerializedName("name")
        private String mName;
        @SerializedName("number")
        private String mNumber;
        @SerializedName("position")
        private String mPosition;
        @SerializedName("sport_id")
        private String mSportId;
        @SerializedName("team")
        private String mTeam;
        @SerializedName("user_id")
        private String mUserId;
        @SerializedName("video_thumb")
        private String mVideoThumb;

        public String getAge() {
            return mAge;
        }

        public void setAge(String age) {
            mAge = age;
        }

        public String getCardBg() {
            return mCardBg;
        }

        public void setCardBg(String card_bg) {
            mCardBg = card_bg;
        }

        public String getCardType() {
            return mCardType;
        }

        public void setCardType(String card_type) {
            mCardType = card_type;
        }

        public String getGrade() {
            return mGrade;
        }

        public void setGrade(String grade) {
            mGrade = grade;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getLevel() {
            return mLevel;
        }

        public void setLevel(String level) {
            mLevel = level;
        }

        public String getLocation() {
            return mLocation;
        }

        public void setLocation(String location) {
            mLocation = location;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getNumber() {
            return mNumber;
        }

        public void setNumber(String number) {
            mNumber = number;
        }

        public String getPosition() {
            return mPosition;
        }

        public void setPosition(String position) {
            mPosition = position;
        }

        public String getSportId() {
            return mSportId;
        }

        public void setSportId(String sport_id) {
            mSportId = sport_id;
        }

        public String getTeam() {
            return mTeam;
        }

        public void setTeam(String team) {
            mTeam = team;
        }

        public String getUserId() {
            return mUserId;
        }

        public void setUserId(String user_id) {
            mUserId = user_id;
        }

        public String getVideoThumb() {
            return mVideoThumb;
        }

        public void setVideoThumb(String video_thumb) {
            mVideoThumb = video_thumb;
        }


    }
}
