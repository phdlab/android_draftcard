package com.draftcard.dashboard.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS087 on 15-Mar-17.
 */

public class CardModel {

    @SerializedName("height")
    private String mHeight = "";
    @SerializedName("weight")
    private String mWeight = "";
    @SerializedName("gpa")
    private String mGPA = "";

    @SerializedName("age")
    private String mAge = "";
    @SerializedName("card_bg")
    private String mCardBg = "";
    @SerializedName("card_type")
    private String mCardType = "";
    @SerializedName("grade")
    private String mGrade = "";
    @SerializedName("id")
    private Long mId;
    @SerializedName("level")
    private String mLevel = "";
    @SerializedName("location")
    private String mLocation = "";
    @SerializedName("name")
    private String mName = "";
    @SerializedName("number")
    private String mNumber = "";
    @SerializedName("position")
    private String mPosition = "";
    @SerializedName("sport_id")
    private String mSportId = "";
    @SerializedName("team")
    private String mTeam = "";
    @SerializedName("user_id")
    private String mUserId = "";
    @SerializedName("video_thumb")
    private String mVideoThumb = "";
    private int mHeightInInches;

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public String getCardBg() {
        return mCardBg;
    }

    public void setCardBg(String cardBg) {
        mCardBg = cardBg;
    }

    public String getCardType() {
        return mCardType;
    }

    public void setCardType(String cardType) {
        mCardType = cardType;
    }

    public String getGrade() {
        return mGrade;
    }

    public void setGrade(String grade) {
        mGrade = grade;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLevel() {
        return mLevel;
    }

    public void setLevel(String level) {
        mLevel = level;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

    public String getPosition() {
        return mPosition;
    }

    public void setPosition(String position) {
        mPosition = position;
    }

    public String getSportId() {
        return mSportId;
    }

    public void setSportId(String sportId) {
        mSportId = sportId;
    }

    public String getTeam() {
        return mTeam;
    }

    public void setTeam(String team) {
        mTeam = team;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getVideoThumb() {
        return mVideoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        mVideoThumb = videoThumb;
    }

    public String getHeight() {
        return mHeight;
    }

    public void setHeight(String height) {
        mHeight = height;
    }

    public String getWeight() {
        return mWeight;
    }

    public void setWeight(String weight) {
        mWeight = weight;
    }

    public String getGPA() {
        return mGPA;
    }

    public void setGPA(String GPA) {
        mGPA = GPA;
    }

    public void setHeightInInches(int heightInInches) {
        mHeightInInches = heightInInches;
    }

    public int getHeightInInches() {
        return mHeightInInches;
    }
}
