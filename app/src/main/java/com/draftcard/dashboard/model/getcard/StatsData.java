
package com.draftcard.dashboard.model.getcard;

import com.google.gson.annotations.SerializedName;

public class StatsData {

    @SerializedName("40 yard dash")
    private String m40_yard_dash;
    @SerializedName("60 yard dash")
    private String m60_yard_dash;
    @SerializedName("Assists Per Game")
    private String mAssists_Per_Game;
    @SerializedName("Batting Average")
    private String mBatting_Average;
    @SerializedName("Bench Press Weight")
    private String mBench_Press_Weight;
    @SerializedName("On-base %")
    private String mOn_base;
    @SerializedName("Pitching Speed")
    private String mPitching_Speed;
    @SerializedName("Points Per Game")
    private String mPoints_Per_Game;
    @SerializedName("Rebounds Per Game")
    private String mRebounds_Per_Game;
    @SerializedName("Score")
    private String mScore;
    @SerializedName("Squat Weight")
    private String mSquat_Weight;


    public String getM40_yard_dash() {
        return m40_yard_dash;
    }

    public void setM40_yard_dash(String m40_yard_dash) {
        this.m40_yard_dash = m40_yard_dash;
    }

    public String getM60_yard_dash() {
        return m60_yard_dash;
    }

    public void setM60_yard_dash(String m60_yard_dash) {
        this.m60_yard_dash = m60_yard_dash;
    }

    public String getAssists_Per_Game() {
        return mAssists_Per_Game;
    }

    public void setAssists_Per_Game(String assists_Per_Game) {
        mAssists_Per_Game = assists_Per_Game;
    }

    public String getBatting_Average() {
        return mBatting_Average;
    }

    public void setBatting_Average(String batting_Average) {
        mBatting_Average = batting_Average;
    }

    public String getBench_Press_Weight() {
        return mBench_Press_Weight;
    }

    public void setBench_Press_Weight(String bench_Press_Weight) {
        mBench_Press_Weight = bench_Press_Weight;
    }

    public String getOn_base() {
        return mOn_base;
    }

    public void setOn_base(String on_base) {
        mOn_base = on_base;
    }

    public String getPitching_Speed() {
        return mPitching_Speed;
    }

    public void setPitching_Speed(String pitching_Speed) {
        mPitching_Speed = pitching_Speed;
    }

    public String getPoints_Per_Game() {
        return mPoints_Per_Game;
    }

    public void setPoints_Per_Game(String points_Per_Game) {
        mPoints_Per_Game = points_Per_Game;
    }

    public String getRebounds_Per_Game() {
        return mRebounds_Per_Game;
    }

    public void setRebounds_Per_Game(String rebounds_Per_Game) {
        mRebounds_Per_Game = rebounds_Per_Game;
    }

    public String getScore() {
        return mScore;
    }

    public void setScore(String score) {
        mScore = score;
    }

    public String getSquat_Weight() {
        return mSquat_Weight;
    }

    public void setSquat_Weight(String squat_Weight) {
        mSquat_Weight = squat_Weight;
    }
}
