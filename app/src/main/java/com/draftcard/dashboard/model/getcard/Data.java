
package com.draftcard.dashboard.model.getcard;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("card_bg_path")
    private String mCardBgPath;
    @SerializedName("card_video_thumb_path")
    private String mCardVideoThumbPath;
    @SerializedName("cards")
    private List<Card> mCards;

    public String getCardBgPath() {
        return mCardBgPath;
    }

    public void setCardBgPath(String card_bg_path) {
        mCardBgPath = card_bg_path;
    }

    public String getCardVideoThumbPath() {
        return mCardVideoThumbPath;
    }

    public void setCardVideoThumbPath(String card_video_thumb_path) {
        mCardVideoThumbPath = card_video_thumb_path;
    }

    public List<Card> getCards() {
        return mCards;
    }

    public void setCards(List<Card> cards) {
        mCards = cards;
    }

}
